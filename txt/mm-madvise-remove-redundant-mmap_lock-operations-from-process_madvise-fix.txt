From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-madvise-remove-redundant-mmap_lock-operations-from-process_madvise-fix
Date: Thu Feb  6 04:34:29 PM PST 2025

update comment, per Lorenzo

Cc: David Hildenbrand <david@redhat.com>
Cc: Davidlohr Bueso <dave@stgolabs.net>
Cc: Liam R. Howlett <howlett@gmail.com>
Cc: Lorenzo Stoakes <lorenzo.stoakes@oracle.com>
Cc: SeongJae Park <sj@kernel.org>
Cc: Shakeel Butt <shakeel.butt@linux.dev>
Cc: Vlastimil Babka <vbabka@suse.cz>
