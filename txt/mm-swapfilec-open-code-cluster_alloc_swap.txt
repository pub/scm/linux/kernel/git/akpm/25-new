From: Baoquan He <bhe@redhat.com>
Subject: mm/swapfile.c: open code cluster_alloc_swap()
Date: Wed, 5 Feb 2025 17:27:21 +0800

It's only called in scan_swap_map_slots().

And also remove the stale code comment in scan_swap_map_slots() because
it's not fit for the current cluster allocation mechanism.

Link: https://lkml.kernel.org/r/20250205092721.9395-13-bhe@redhat.com
Signed-off-by: Baoquan He <bhe@redhat.com>
Cc: Chris Li <chrisl@kernel.org>
Cc: Kairui Song <ryncsn@gmail.com>
