From: Shakeel Butt <shakeel.butt@linux.dev>
Subject: memcg: add hierarchical effective limits for v2
Date: Wed, 5 Feb 2025 14:20:29 -0800

Memcg-v1 exposes hierarchical_[memory|memsw]_limit counters in its
memory.stat file which applications can use to get their effective limit
which is the minimum of limits of itself and all of its ancestors.  This
is pretty useful in environments where cgroup namespace is used and the
application does not have access to the full view of the cgroup hierarchy.
Let's expose effective limits for memcg v2 as well.

Link: https://lkml.kernel.org/r/20250205222029.2979048-1-shakeel.butt@linux.dev
Signed-off-by: Shakeel Butt <shakeel.butt@linux.dev>
Reviewed-by: Balbir Singh <balbirs@nvidia.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Michal Hocko <mhocko@kernel.org>
Cc: Michal Koutný <mkoutny@suse.com>
Cc: Muchun Song <muchun.song@linux.dev>
Cc: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Tejun Heo <tj@kernel.org>
Cc: "T.J. Mercier" <tjmercier@google.com>
