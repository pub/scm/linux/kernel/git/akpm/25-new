From: Baoquan He <bhe@redhat.com>
Subject: mm/swap: rename swap_swapcount() to swap_entry_swapped()
Date: Wed, 5 Feb 2025 17:27:19 +0800

The new function name can reflect the real behaviour of the function more
clearly and more accurately.  And the renaming avoids the confusion
between swap_swapcount() and swp_swapcount().

Link: https://lkml.kernel.org/r/20250205092721.9395-11-bhe@redhat.com
Signed-off-by: Baoquan He <bhe@redhat.com>
Cc: Chris Li <chrisl@kernel.org>
Cc: Kairui Song <ryncsn@gmail.com>
