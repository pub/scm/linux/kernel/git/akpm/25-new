From: Hao Zhang <zhanghao1@kylinos.cn>
Subject: mm/vmscan: extract calculated pressure balance as a function
Date: Wed, 15 Jan 2025 09:58:29 +0800

Extract pressure balance calculation into a function.This doesn't change
current behaviour.

Link: https://lkml.kernel.org/r/tencent_735DB36A2306C08B8568049E4C0B99716C07@qq.com
Signed-off-by: Hao Zhang <zhanghao1@kylinos.cn>
