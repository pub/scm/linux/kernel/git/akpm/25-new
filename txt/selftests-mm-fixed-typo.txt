From: Eric Salem <ericsalem@gmail.com>
Subject: selftests: mm: fix typo
Date: Sat, 8 Feb 2025 20:36:36 -0600

Fix misspelling.

Link: https://lkml.kernel.org/r/77e0e915-36c3-4c95-84b8-0b73aaa17951@gmail.com
Signed-off-by: Eric Salem <ericsalem@gmail.com>
