From: Seongjun Kim <bus710@gmail.com>
Subject: samples/damon: a typo in the kconfig - sameple
Date: Wed, 26 Feb 2025 10:42:04 -0800

There is a typo in the Kconfig file of the damon sample module.  Correct
it: s/sameple/sample/

Link: https://lkml.kernel.org/r/20250226184204.29370-1-sj@kernel.org
Signed-off-by: Seongjun Kim <bus710@gmail.com>
Signed-off-by: SeongJae Park <sj@kernel.org>
Reviewed-by: SeongJae Park <sj@kernel.org>
