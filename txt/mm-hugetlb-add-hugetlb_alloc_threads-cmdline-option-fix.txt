From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-hugetlb-add-hugetlb_alloc_threads-cmdline-option-fix
Date: Mon Mar  3 08:51:01 PM PST 2025

tidy up a comment

Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Muchun Song <muchun.song@linux.dev>
Cc: Thomas Prescher <thomas.prescher@cyberus-technology.de>
