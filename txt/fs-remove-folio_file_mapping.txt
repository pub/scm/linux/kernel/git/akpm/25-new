From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: fs: remove folio_file_mapping()
Date: Mon, 17 Feb 2025 19:20:08 +0000

No callers of this function remain as filesystems no longer see swapfile
pages through their normal read/write paths.

Link: https://lkml.kernel.org/r/20250217192009.437916-3-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
