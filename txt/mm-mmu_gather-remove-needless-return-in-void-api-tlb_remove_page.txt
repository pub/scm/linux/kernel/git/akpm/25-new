From: Zijun Hu <quic_zijuhu@quicinc.com>
Subject: mm/mmu_gather: remove needless return in void API tlb_remove_page()
Date: Fri, 21 Feb 2025 05:02:06 -0800

Remove needless 'return' in void API tlb_remove_page() since both the API
and tlb_remove_page_size() are void functions.

Link: https://lkml.kernel.org/r/20250221-rmv_return-v1-1-cc8dff275827@quicinc.com
Signed-off-by: Zijun Hu <quic_zijuhu@quicinc.com>
