From: I Hsin Cheng <richard120310@gmail.com>
Subject: maple_tree: correct comment for mas_start()
Date: Mon, 10 Feb 2025 02:10:23 +0800

There's no mas->status of "mas_start", what the function is checking is
whether mas->status equals to "ma_start".  Correct the comment for the
function.

Link: https://lkml.kernel.org/r/20250209181023.228856-1-richard120310@gmail.com
Signed-off-by: I Hsin Cheng <richard120310@gmail.com>
Reviewed-by: Liam R. Howlett <howlett@gmail.com>
