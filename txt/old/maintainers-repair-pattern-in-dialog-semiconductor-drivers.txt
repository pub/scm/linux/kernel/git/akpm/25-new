From: Lukas Bulwahn <lukas.bulwahn@gmail.com>
Subject: MAINTAINERS: repair pattern in DIALOG SEMICONDUCTOR DRIVERS
Date: Tue, 9 May 2023 09:48:34 +0200

Commit 361104b05684c ("dt-bindings: mfd: Convert da9063 to yaml") converts
da9063.txt to dlg,da9063.yaml and adds a new file pattern in MAINTAINERS. 
Unfortunately, the file pattern matches da90*.yaml, but the yaml file is
prefixed with dlg,da90.

Hence, ./scripts/get_maintainer.pl --self-test=patterns complains about a
broken file pattern.

Repair this file pattern in DIALOG SEMICONDUCTOR DRIVERS.

Link: https://lkml.kernel.org/r/20230509074834.21521-1-lukas.bulwahn@gmail.com
Fixes: 361104b05684c ("dt-bindings: mfd: Convert da9063 to yaml")
Signed-off-by: Lukas Bulwahn <lukas.bulwahn@gmail.com>
Acked-by: Conor Dooley <conor.dooley@microchip.com>
Cc: Lee Jones <lee@kernel.org>
