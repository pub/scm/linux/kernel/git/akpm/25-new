From: Yosry Ahmed <yosryahmed@google.com>
Subject: mm: vmpressure: don't count userspace-induced reclaim as memory pressure
Date: Thu, 23 Jun 2022 00:44:52 +0000

Commit e22c6ed90aa9 ("mm: memcontrol: don't count limit-setting reclaim as
memory pressure") made sure that memory reclaim that is induced by
userspace (limit-setting, proactive reclaim, ..) is not counted as memory
pressure for the purposes of psi.

Instead of counting psi inside try_to_free_mem_cgroup_pages(), callers
from try_charge() and reclaim_high() wrap the call to
try_to_free_mem_cgroup_pages() with psi handlers.

However, vmpressure is still counted in these cases where reclaim is
directly induced by userspace.  This patch makes sure vmpressure is not
counted in those operations, in the same way as psi.  Since vmpressure
calls need to happen deeper within the reclaim path, the same approach
could not be followed.  Hence, a new "controlled" flag is added to struct
scan_control to flag a reclaim operation that is controlled by userspace. 
This flag is set by limit-setting and proactive reclaim operations, and is
used to count vmpressure correctly.

To prevent future divergence of psi and vmpressure, commit e22c6ed90aa9
("mm: memcontrol: don't count limit-setting reclaim as memory pressure")
is effectively reverted and the same flag is used to control psi as well.

Link: https://lkml.kernel.org/r/20220623004452.1217326-1-yosryahmed@google.com
Signed-off-by: Yosry Ahmed <yosryahmed@google.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Vlastimil Babka <vbabka@suse.cz>
Cc: David Hildenbrand <david@redhat.com>
Cc: Miaohe Lin <linmiaohe@huawei.com>
Cc: NeilBrown <neilb@suse.de>
Cc: Alistair Popple <apopple@nvidia.com>
Cc: Suren Baghdasaryan <surenb@google.com>
Cc: Peter Xu <peterx@redhat.com>
