From: Changbin Du <changbin.du@huawei.com>
Subject: modules-wait-do_free_init-correctly-fix
Date: Mon, 25 Dec 2023 14:15:13 +0800

fix compilation issue for no CONFIG_MODULES found by 0-DAY

Link: https://lkml.kernel.org/r/20231225061513.2984575-1-changbin.du@huawei.com
Fixes: 1a7b7d922081 ("modules: Use vmalloc special flag")
Signed-off-by: Changbin Du <changbin.du@huawei.com>
Cc: Xiaoyi Su <suxiaoyi@huawei.com>
