From: Feng Tang <feng.tang@intel.com>
Subject: panic: add option to dump blocked tasks in panic_print
Date: Fri, 2 Feb 2024 21:20:42 +0800

For debugging kernel panics and other bugs, there is already an option of
panic_print to dump all tasks' call stacks.  On today's large servers
running many containers, there could be thousands of tasks or more, and
this will print out huge amount of call stacks, taking a lot of time (for
serial console which is main target user case of panic_print).

And in many cases, only those several tasks being blocked are key for the
panic, so add an option to only dump blocked tasks' call stacks.

[akpm@linux-foundation.org: clarify documentation a little]
Link: https://lkml.kernel.org/r/20240202132042.3609657-1-feng.tang@intel.com
Signed-off-by: Feng Tang <feng.tang@intel.com>
Tested-by: Guilherme G. Piccoli <gpiccoli@igalia.com>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Josh Poimboeuf <jpoimboe@kernel.org>
Cc: Peter Zijlstra (Intel) <peterz@infradead.org>
Cc: Randy Dunlap <rdunlap@infradead.org>
