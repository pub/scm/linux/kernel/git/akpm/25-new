From: Kees Cook <keescook@chromium.org>
Subject: mempool: use kmalloc_size_roundup() to match ksize() usage
Date: Tue, 18 Oct 2022 02:03:29 -0700

Round up allocations with kmalloc_size_roundup() so that mempool's use of
ksize() is always accurate and no special handling of the memory is needed
by KASAN, UBSAN_BOUNDS, nor FORTIFY_SOURCE.

Link: https://lkml.kernel.org/r/20221018090323.never.897-kees@kernel.org
Signed-off-by: Kees Cook <keescook@chromium.org>
