From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: nilfs2: remove page_address() from nilfs_add_link
Date: Mon, 27 Nov 2023 23:30:23 +0900

In preparation for removing kmap from directory handling, use
offset_in_page() to calculate 'from'.  Matches ext2.

Link: https://lkml.kernel.org/r/20231127143036.2425-5-konishi.ryusuke@gmail.com
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Signed-off-by: Ryusuke Konishi <konishi.ryusuke@gmail.com>
