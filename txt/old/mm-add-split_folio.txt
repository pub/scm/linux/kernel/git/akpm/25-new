From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: add split_folio()
Date: Fri, 2 Sep 2022 20:46:00 +0100

This wrapper removes a need to use split_huge_page(&folio->page).  Convert
two callers.

Link: https://lkml.kernel.org/r/20220902194653.1739778-5-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
