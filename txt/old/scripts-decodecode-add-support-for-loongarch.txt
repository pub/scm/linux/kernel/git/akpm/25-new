From: Youling Tang <tangyouling@kylinos.cn>
Subject: scripts/decodecode: add support for LoongArch
Date: Sat, 16 Dec 2023 14:51:28 +0800

An example how to invoke decodecode for loongarch64:
$ echo 'Code: 380839f6  380831f9  28412bae  <24000ca6>
  004081ad  0014cb50  004083e8  02bff34c  58008e91' | \
  ARCH=loongarch CROSS_COMPILE=loongarch64-linux-gnu- \
  ./scripts/decodecode
Code: 380839f6 380831f9 28412bae <24000ca6> 004081ad 0014cb50 004083e8 02bff34c 58008e91
All code
========
   0:	380839f6 	ldx.w       	$fp, $t3, $t2
   4:	380831f9 	ldx.w       	$s2, $t3, $t0
   8:	28412bae 	ld.h        	$t2, $s6, 74(0x4a)
   c:*	24000ca6 	ldptr.w     	$a2, $a1, 12(0xc)		<-- trapping instruction
  10:	004081ad 	slli.w      	$t1, $t1, 0x0
  14:	0014cb50 	and         	$t4, $s3, $t6
  18:	004083e8 	slli.w      	$a4, $s8, 0x0
  1c:	02bff34c 	addi.w      	$t0, $s3, -4(0xffc)
  20:	58008e91 	beq         	$t8, $t5, 140(0x8c)	# 0xac

Code starting with the faulting instruction
===========================================
   0:	24000ca6 	ldptr.w     	$a2, $a1, 12(0xc)
   4:	004081ad 	slli.w      	$t1, $t1, 0x0
   8:	0014cb50 	and         	$t4, $s3, $t6
   c:	004083e8 	slli.w      	$a4, $s8, 0x0
  10:	02bff34c 	addi.w      	$t0, $s3, -4(0xffc)
  14:	58008e91 	beq         	$t8, $t5, 140(0x8c)	# 0xa0

Link: https://lkml.kernel.org/r/MW4PR84MB3145B99B9677BB7887BB26CD8192A@MW4PR84MB3145.NAMPRD84.PROD.OUTLOOK.COM
Signed-off-by: Youling Tang <tangyouling@kylinos.cn>
Acked-by: Huacai Chen <chenhuacai@loongson.cn>
