From: Qi Zheng <zhengqi.arch@bytedance.com>
Subject: mm-memcontrol-add-pgscanpgsteal_kswapddirect-items-in-memorystat-of-cgroup-v2-v3
Date: Mon, 6 Jun 2022 23:40:28 +0800

add comment for memcg_vm_event_stat (suggested by Michal)

Link: https://lkml.kernel.org/r/20220606154028.55030-1-zhengqi.arch@bytedance.com
Signed-off-by: Qi Zheng <zhengqi.arch@bytedance.com>
Acked-by: Johannes Weiner <hannes@cmpxchg.org>
Acked-by: Roman Gushchin <roman.gushchin@linux.dev>
Acked-by: Muchun Song <songmuchun@bytedance.com>
Acked-by: Shakeel Butt <shakeelb@google.com>
Acked-by: Michal Hocko <mhocko@suse.com>
