From: Shivamurthy Shastri <shivamurthy.shastri@linutronix.de>
Subject: mm/vmstat: fix -Wenum-enum-conversion warning in vmstat.h
Date: Fri, 21 Jun 2024 13:16:04 +0200

A W=1 build with -Wenum-enum-conversion enabled, results in the following
build warning due to an arithmetic operation between different enumeration
types 'enum node_stat_item' and 'enum lru_list':

  include/linux/vmstat.h:514:36: warning: arithmetic between different enumeration types ('enum node_stat_item' and 'enum lru_list') [-Wenum-enum-conversion]
    514 |         return node_stat_name(NR_LRU_BASE + lru) + 3; // skip "nr_"
        |                               ~~~~~~~~~~~ ^ ~~~

Address this by casting lru to the proper type.

Link: https://lkml.kernel.org/r/20240621111604.25330-1-shivamurthy.shastri@linutronix.de
Signed-off-by: Shivamurthy Shastri <shivamurthy.shastri@linutronix.de>
Reviewed-by: Anna-Maria Behnsen <anna-maria@linutronix.de>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Thomas Gleixner <tglx@linutronix.de>
Cc: "Arnd Bergmann" <arnd@arndb.de>
