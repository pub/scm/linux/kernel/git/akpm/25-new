From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: fix clean_record_shared_mapping_range kernel-doc
Date: Fri, 18 Aug 2023 21:06:29 +0100

Turn the a), b) into an unordered ReST list and remove the unnecessary
'Note:' prefix.

Link: https://lkml.kernel.org/r/20230818200630.2719595-4-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: Randy Dunlap <rdunlap@infradead.org>
Acked-by: Mike Rapoport (IBM) <rppt@kernel.org>
