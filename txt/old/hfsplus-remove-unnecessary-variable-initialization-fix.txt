From: Andrew Morton <akpm@linux-foundation.org>
Subject: hfsplus-remove-unnecessary-variable-initialization-fix
Date: Wed Dec 21 01:32:43 PM PST 2022

give hfsplus_listxattr:key_len narrower scope

Cc: XU pengfei <xupengfei@nfschina.com>
Cc: Andrew Morton <akpm@linux-foudation.org>
Cc: Christian Brauner <brauner@kernel.org>
Cc: Kees Cook <keescook@chromium.org>
