From: Peng Zhang <zhangpeng.00@bytedance.com>
Subject: maple_tree: simplify and clean up mas_wr_node_store()
Date: Wed, 24 May 2023 11:12:46 +0800

Simplify and clean up mas_wr_node_store(), remove unnecessary code.

Link: https://lkml.kernel.org/r/20230524031247.65949-10-zhangpeng.00@bytedance.com
Signed-off-by: Peng Zhang <zhangpeng.00@bytedance.com>
Reviewed-by: Liam R. Howlett <Liam.Howlett@oracle.com>
