From: Kefeng Wang <wangkefeng.wang@huawei.com>
Subject: perf/core: use vma_is_stack() and vma_is_heap()
Date: Wed, 12 Jul 2023 22:38:31 +0800

Use the helpers to simplify code, also kill unneeded goto cpy_name.

Link: https://lkml.kernel.org/r/20230712143831.120701-6-wangkefeng.wang@huawei.com
Signed-off-by: Kefeng Wang <wangkefeng.wang@huawei.com>
Cc: Peter Zijlstra <peterz@infradead.org>
Cc: Arnaldo Carvalho de Melo <acme@kernel.org>
