From: Muhammad Usama Anjum <usama.anjum@collabora.com>
Subject: selftests/mm: hugepage-vmemmap: fix type mismatch warnings
Date: Thu, 9 Jan 2025 22:38:36 +0500

Fix type mismatch warnings.

Link: https://lkml.kernel.org/r/20250109173842.1142376-11-usama.anjum@collabora.com
Signed-off-by: Muhammad Usama Anjum <usama.anjum@collabora.com>
Cc: Andy Lutomirski <luto@amacapital.net>
Cc: Jérôme Glisse <jglisse@redhat.com>
Cc: Kees Cook <kees@kernel.org>
Cc: Shuah Khan <shuah@kernel.org>
Cc: Will Drewry <wad@chromium.org>
