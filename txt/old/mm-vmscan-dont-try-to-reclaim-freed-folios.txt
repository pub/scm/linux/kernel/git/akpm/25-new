From: Miaohe Lin <linmiaohe@huawei.com>
Subject: mm/vmscan: don't try to reclaim freed folios
Date: Wed, 8 Jun 2022 22:14:32 +0800

If folios were freed from under us, there's no need to reclaim them.  Skip
these folios to save lots of cpu cycles and avoid possible unnecessary
disk I/O.

Link: https://lkml.kernel.org/r/20220608141432.23258-1-linmiaohe@huawei.com
Signed-off-by: Miaohe Lin <linmiaohe@huawei.com>
Cc: Matthew Wilcox <willy@infradead.org>
