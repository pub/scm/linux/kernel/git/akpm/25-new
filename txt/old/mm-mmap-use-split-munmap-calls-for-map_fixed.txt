From: "Liam R. Howlett" <Liam.Howlett@oracle.com>
Subject: mm/mmap: use split munmap calls for MAP_FIXED
Date: Tue, 11 Jun 2024 14:02:00 -0400

Use vms_gather_munmap_vmas() and vms_complete_munmap_vmas() to prepare and
execute the unmapping after the new area is written to the maple tree. 
Delaying the unmapping avoids RCU readers seeing a gap in the vmas, which
isn't supposed to exist logically.

Gathering the vmas that will be unmapped allows for the accounting work to
be calculated prior to checking if there is enough memory.  Using the
number calculated during vms_gather_munmap_vmas() allows code to be
reduced in mmap_region().  This removes the only caller to
count_vma_pages_range(), so the function has been dropped.

This does have the side effect of allowing vmas to be split for unmap and
fail may_expand_vm(), but the number of pages covered will not change.

The validate_mm() call in vma_expand() must be dropped as the MAP_FIXED
munmap may be in progress.  There is still a validate_mm() call at the end
of mmap_region(), which calls vma_expand().

Note that do_vmi_munmap() was previously used to munmap, which checked
alignment and overflow.  These checks were unnecessary as do_mmap()
already checks these cases, and arch/mips/kernel/vdso.c
arch_setup_additional_pages() uses predefined values that must already
pass these checks.

Link: https://lkml.kernel.org/r/20240611180200.711239-9-Liam.Howlett@oracle.com
Signed-off-by: Liam R. Howlett <Liam.Howlett@oracle.com>
Cc: Lorenzo Stoakes <lstoakes@gmail.com>
Cc: Suren Baghdasaryan <surenb@google.com>
Cc: Vlastimil Babka <vbabka@suse.cz>
