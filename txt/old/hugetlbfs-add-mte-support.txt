From: Yang Shi <yang@os.amperecomputing.com>
Subject: hugetlbfs: add MTE support
Date: Tue, 25 Jun 2024 16:37:17 -0700

MTE can be supported on ram based filesystem.  It is supported on tmpfs. 
There is use case to use MTE on hugetlbfs as well, adding MTE support.

Exercising this capability requires special arrangements.  This needs
hardware support and custom-patched QEMU.  We did in-house testing on
AmpereOne platform with patched QEMU 8.1.

Custom-patched QEMU is not required for a minimum test.  But a special
test program is definitely needed.  We used custom-patched QEMU to test
VM backed by hugetlbfs with MTE enabled.

Link: https://lkml.kernel.org/r/20240625233717.2769975-1-yang@os.amperecomputing.com
Signed-off-by: Yang Shi <yang@os.amperecomputing.com>
Cc: Catalin Marinas <catalin.marinas@arm.com>
Cc: Muchun Song <muchun.song@linux.dev>
Cc: Will Deacon <will@kernel.org>
