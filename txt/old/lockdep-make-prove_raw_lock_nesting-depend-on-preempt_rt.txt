From: Chen Jun <chenjun102@huawei.com>
Subject: lockdep: make PROVE_RAW_LOCK_NESTING depend on PREEMPT_RT
Date: Thu, 16 Jun 2022 02:29:11 +0000

CONFIG_PROVE_RAW_LOCK_NESTING is used to check two things.
1. calling spin_lock in hardirq.
2. raw_spinlock - spinlock nesting.
The error messages like below:
[   13.485711] [ BUG: Invalid wait context ]

However there is no problem on non-PREEMPT_RT kernel. The messages may be
confusing and disturbing.

Therefore, do not check raw_spinlock - spinlock nesting
on non-PREEMPT_RT kernel.

Link: https://lkml.kernel.org/r/20220616022911.35826-1-chenjun102@huawei.com
Signed-off-by: Chen Jun <chenjun102@huawei.com>
Cc: Peter Zijlstra <peterz@infradead.org>
Cc: Sebastian Andrzej Siewior <bigeasy@linutronix.de>
