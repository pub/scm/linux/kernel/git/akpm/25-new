From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: migrate: expand the use of folio in __migrate_device_pages()
Date: Tue, 23 Apr 2024 23:55:35 +0100

Removes a few calls to compound_head() and a call to page_mapping().

Link: https://lkml.kernel.org/r/20240423225552.4113447-5-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: David Hildenbrand <david@redhat.com>
Cc: Eric Biggers <ebiggers@google.com>
Cc: Sidhartha Kumar <sidhartha.kumar@oracle.com>
