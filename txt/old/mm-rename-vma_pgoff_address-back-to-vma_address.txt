From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: rename vma_pgoff_address back to vma_address
Date: Thu, 28 Mar 2024 22:58:29 +0000

With all callers converted, we can use the nice shorter name.  Take this
opportunity to reorder the arguments to the logical order (larger object
first).

Link: https://lkml.kernel.org/r/20240328225831.1765286-4-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
