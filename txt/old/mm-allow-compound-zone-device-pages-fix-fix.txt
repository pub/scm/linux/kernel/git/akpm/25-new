From: Balbir Singh <balbirs@nvidia.com>
Subject: mm-allow-compound-zone-device-pages-fix-fix
Date: Fri, 21 Feb 2025 12:27:49 +1100

 - Add {} around the if statement as per coding style
 - Add Reviewed-by/Acked-by tags

Link: https://lkml.kernel.org/r/20250219231337.364133-1-balbirs@nvidia.com
Link: https://lkml.kernel.org/r/20250221012749.506513-1-balbirs@nvidia.com
Signed-off-by: Balbir Singh <balbirs@nvidia.com>
Reviewed-by: Alistair Popple <apopple@nvidia.com>
Acked-by: David Hildenbrand <david@redhat.com>
Cc: Jason Gunthorpe <jgg@ziepe.ca>
Cc: Dan Williams <dan.j.williams@intel.com>
