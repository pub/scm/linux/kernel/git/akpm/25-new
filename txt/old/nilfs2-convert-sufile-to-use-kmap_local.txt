From: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Subject: nilfs2: convert sufile to use kmap_local
Date: Mon, 22 Jan 2024 23:01:52 +0900

Concerning the code of the metadata file sufile for segment management,
convert all parts that uses the deprecated kmap_atomic() to use
kmap_local.  All transformations are directly possible here.

Link: https://lkml.kernel.org/r/20240122140202.6950-6-konishi.ryusuke@gmail.com
Signed-off-by: Ryusuke Konishi <konishi.ryusuke@gmail.com>
