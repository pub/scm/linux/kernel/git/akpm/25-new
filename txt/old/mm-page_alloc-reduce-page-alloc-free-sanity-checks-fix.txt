From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-page_alloc-reduce-page-alloc-free-sanity-checks-fix
Date: Sat Feb 25 01:00:30 PM PST 2023

make check_pages_enabled static

Reported-by: kernel test robot <lkp@intel.com>
  Link: https://lkml.kernel.org/r/63fa411f.ZvVOisJt5OlLzGYF%25lkp@intel.com
Cc: Alexander Halbuer <halbuer@sra.uni-hannover.de>
Cc: Kees Cook <keescook@chromium.org>
Cc: Mel Gorman <mgorman@techsingularity.net>
Cc: Vlastimil Babka <vbabka@suse.cz>
