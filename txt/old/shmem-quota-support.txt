From: Carlos Maiolino <cem@kernel.org>
Subject: shmem: quota support
Date: Thu, 13 Jul 2023 15:48:47 +0200

Now the basic infra-structure is in place, enable quota support for tmpfs.

This offers user and group quotas to tmpfs (project quotas will be added
later).  Also, as other filesystems, the tmpfs quota is not supported
within user namespaces yet, so idmapping is not translated.

Link: https://lkml.kernel.org/r/20230713134848.249779-6-cem@kernel.org
Signed-off-by: Lukas Czerner <lczerner@redhat.com>
Signed-off-by: Carlos Maiolino <cmaiolino@redhat.com>
Reviewed-by: Jan Kara <jack@suse.cz>
Cc: Al Viro <viro@zeniv.linux.org.uk>
Cc: Christian Brauner <brauner@kernel.org>
Cc: Darrick J. Wong <djwong@kernel.org>
Cc: Hugh Dickins <hughd@google.com>
