From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: zram: add zlib compression backend support
Date: Mon, 2 Sep 2024 19:55:58 +0900

Add s/w zlib (inflate/deflate) compression.

Link: https://lkml.kernel.org/r/20240902105656.1383858-11-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Nick Terrell <terrelln@fb.com>
