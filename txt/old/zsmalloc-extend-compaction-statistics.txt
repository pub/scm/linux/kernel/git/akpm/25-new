From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: zsmalloc: extend compaction statistics
Date: Thu, 23 Feb 2023 12:04:50 +0900

Extend zsmalloc zs_pool_stats with a new member that holds the number of
objects pool compaction moved between pool pages.

Link: https://lkml.kernel.org/r/20230223030451.543162-6-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Yosry Ahmed <yosryahmed@google.com>
