From: Colin Ian King <colin.i.king@gmail.com>
Subject: fs: hfsplus: make extend error rate limited
Date: Wed, 19 Jul 2023 13:17:35 +0100

Extending a file where there is not enough free space can trigger frequent
extend alloc file error messages and this can easily spam the kernel log. 
Make the error message rate limited.

Link: https://lkml.kernel.org/r/20230719121735.2831164-1-colin.i.king@gmail.com
Signed-off-by: Colin Ian King <colin.i.king@gmail.com>
Cc: Christian Brauner <brauner@kernel.org>
