From: Yang Yingliang <yangyingliang@huawei.com>
Subject: fail_function: switch to memdup_user_nul() helper
Date: Fri, 26 Aug 2022 15:33:35 +0800

Use memdup_user_nul() helper instead of open-coding to simplify the code.

Link: https://lkml.kernel.org/r/20220826073337.2085798-1-yangyingliang@huawei.com
Signed-off-by: Yang Yingliang <yangyingliang@huawei.com>
Reviewed-by: Andrew Morton <akpm@linux-foundation.org>
