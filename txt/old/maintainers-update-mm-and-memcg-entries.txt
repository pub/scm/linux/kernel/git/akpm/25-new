From: "Mike Rapoport (IBM)" <rppt@kernel.org>
Subject: MAINTAINERS: update mm and memcg entries
Date: Thu, 8 Feb 2024 07:57:27 +0200

Add F: lines for memory management and memory cgroup include files.

Link: https://lkml.kernel.org/r/20240208055727.142387-1-rppt@kernel.org
Signed-off-by: Mike Rapoport (IBM) <rppt@kernel.org>
