From: Arnd Bergmann <arnd@arndb.de>
Subject: mips: move jump_label_apply_nops() declaration to header
Date: Mon, 4 Dec 2023 12:56:58 +0100

Instead of an extern declaration in the C file with the caller, move it to
an appropriate header, avoiding

arch/mips/kernel/jump_label.c:93:6: error: no previous prototype for 'jump_label_apply_nops' [-Werror=missing-prototypes]

Link: https://lkml.kernel.org/r/20231204115710.2247097-9-arnd@kernel.org
Signed-off-by: Arnd Bergmann <arnd@arndb.de>
Cc: Stephen Rothwell <sfr@rothwell.id.au>
Cc: Thomas Bogendoerfer <tsbogend@alpha.franken.de>
