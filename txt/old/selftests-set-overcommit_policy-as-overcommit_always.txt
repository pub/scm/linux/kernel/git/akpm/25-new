From: Chaitanya S Prakash <chaitanyas.prakash@arm.com>
Subject: selftests: set overcommit_policy as OVERCOMMIT_ALWAYS
Date: Tue, 14 Mar 2023 09:53:51 +0530

The kernel's default behaviour is to obstruct the allocation of high
virtual address as it handles memory overcommit in a heuristic manner. 
Setting the parameter as OVERCOMMIT_ALWAYS, ensures kernel isn't
susceptible to the availability of a platform's physical memory when
denying a memory allocation request.

Link: https://lkml.kernel.org/r/20230314042351.13134-4-chaitanyas.prakash@arm.com
Signed-off-by: Chaitanya S Prakash <chaitanyas.prakash@arm.com>
Cc: Anshuman Khandual <khandual@linux.vnet.ibm.com>
Cc: Shuah Khan <shuah@kernel.org>
