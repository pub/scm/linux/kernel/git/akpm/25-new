From: Miaohe Lin <linmiaohe@huawei.com>
Subject: mm, hwpoison: fix possible use-after-free in mf_dax_kill_procs()
Date: Thu, 18 Aug 2022 21:00:14 +0800

After kill_procs(), tk will be freed without being removed from the
to_kill list.  In the next iteration, the freed list entry in the to_kill
list will be accessed, thus leading to use-after-free issue.  Fix it by
reinitializing the to_kill list after unmap_and_kill().

Link: https://lkml.kernel.org/r/20220818130016.45313-5-linmiaohe@huawei.com
Fixes: c36e20249571 ("mm: introduce mf_dax_kill_procs() for fsdax case")
Signed-off-by: Miaohe Lin <linmiaohe@huawei.com>
Cc: Naoya Horiguchi <naoya.horiguchi@nec.com>
