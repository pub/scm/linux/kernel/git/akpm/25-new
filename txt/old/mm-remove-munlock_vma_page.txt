From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: remove munlock_vma_page()
Date: Mon, 16 Jan 2023 19:28:26 +0000

All callers now have a folio and can call munlock_vma_folio().  Update the
documentation to refer to munlock_vma_folio().

Link: https://lkml.kernel.org/r/20230116192827.2146732-4-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
