From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: squashfs: pass a folio to squashfs_readpage_fragment()
Date: Fri, 20 Dec 2024 22:46:25 +0000

Remove an access to page->mapping.

Link: https://lkml.kernel.org/r/20241220224634.723899-2-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: Phillip Lougher <phillip@squashfs.org.uk>
Tested-by: Phillip Lougher <phillip@squashfs.org.uk>
