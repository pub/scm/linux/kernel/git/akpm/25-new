From: SeongJae Park <sj@kernel.org>
Subject: Docs/admin-guide/mm/damon/usage: omit DAMOS filter details in favor of design doc
Date: Thu, 9 Jan 2025 09:51:25 -0800

DAMON usage document is describing some details about DAMOS filters, which
are also documented on the design doc.  Deduplicate the details in favor
of the design doc.

Link: https://lkml.kernel.org/r/20250109175126.57878-10-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
Cc: Jonathan Corbet <corbet@lwn.net>
