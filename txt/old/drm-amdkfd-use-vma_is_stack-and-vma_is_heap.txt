From: Kefeng Wang <wangkefeng.wang@huawei.com>
Subject: drm/amdkfd: use vma_is_stack() and vma_is_heap()
Date: Wed, 12 Jul 2023 22:38:29 +0800

Use the helpers to simplify code.

Link: https://lkml.kernel.org/r/20230712143831.120701-4-wangkefeng.wang@huawei.com
Signed-off-by: Kefeng Wang <wangkefeng.wang@huawei.com>
Cc: Felix Kuehling <Felix.Kuehling@amd.com>
Cc: Alex Deucher <alexander.deucher@amd.com>
Cc: "Christian König" <christian.koenig@amd.com>
Cc: "Pan, Xinhui" <Xinhui.Pan@amd.com>
Cc: David Airlie <airlied@gmail.com>
Cc: Daniel Vetter <daniel@ffwll.ch>
