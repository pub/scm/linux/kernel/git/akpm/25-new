From: Luca Ceresoli <luca.ceresoli@bootlin.com>
Subject: scripts/decode_stacktrace.sh: add '-h' flag
Date: Fri, 23 Aug 2024 10:27:44 +0200

When no parameters are passed, the usage instructions are presented only
when debuginfod-find is not found.  This makes sense because with
debuginfod none of the positional parameters are needed.  However it means
that users having debuginfod-find installed will have no chance of reading
the usage text without opening the file.

Many programs have a '-h' flag to get the usage, so add such a flag. 
Invoking 'scripts/decode_stacktrace.sh -h' will now show the usage text
and exit.

Link: https://lkml.kernel.org/r/20240823-decode_stacktrace-find_module-improvements-v2-3-d7a57d35558b@bootlin.com
Signed-off-by: Luca Ceresoli <luca.ceresoli@bootlin.com>
Reviewed-by: Stephen Boyd <swboyd@chromium.org>
Cc: Alexis Lothoré (eBPF Foundation) <alexis.lothore@bootlin.com>
Cc: Konstantin Khlebnikov <koct9i@gmail.com>
Cc: Sasha Levin <sashal@kernel.org>
Cc: Thomas Petazzoni <thomas.petazzoni@bootlin.com>
