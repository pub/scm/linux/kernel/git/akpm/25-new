From: Sidhartha Kumar <sidhartha.kumar@oracle.com>
Subject: fs/hugetlbfs/inode.c, mm/memory-failure.c: fix hugetlbfs hwpoison handling
Date: Thu, 11 Jan 2024 11:16:55 -0800

has_extra_refcount() makes the assumption that a ref count of 1 means the
page is not referenced by other users.  Commit a08c7193e4f1 (mm/filemap:
remove hugetlb special casing in filemap.c) modifies __filemap_add_folio()
by calling folio_ref_add(folio, nr); for all cases (including hugtetlb)
where nr is the number of pages in the folio.  We should check if the page
is not referenced by other users by checking the page count against the
number of pages rather than 1.

In hugetlbfs_read_iter(), folio_test_has_hwpoisoned() is testing the wrong
flag as, in the hugetlb case, memory-failure code calls
folio_test_set_hwpoison() to indicate poison.  folio_test_hwpoison() is
the correct function to test for that flag.

After these fixes, the hugetlb hwpoison read selftest passes all cases.

Link: https://lkml.kernel.org/r/20240111191655.295530-1-sidhartha.kumar@oracle.com
Signed-off-by: Sidhartha Kumar <sidhartha.kumar@oracle.com>
Fixes: a08c7193e4f1 ("mm/filemap: remove hugetlb special casing in filemap.c")
Closes: https://lore.kernel.org/linux-mm/20230713001833.3778937-1-jiaqiyan@google.com/T/#m8e1469119e5b831bbd05d495f96b842e4a1c5519
Reported-by: Muhammad Usama Anjum <usama.anjum@collabora.com>
Tested-by: Muhammad Usama Anjum <usama.anjum@collabora.com>
Cc: James Houghton <jthoughton@google.com>
Cc: Jiaqi Yan <jiaqiyan@google.com>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Miaohe Lin <linmiaohe@huawei.com>
Cc: Muchun Song <muchun.song@linux.dev>
Cc: Naoya Horiguchi <naoya.horiguchi@nec.com>
Cc: Yang Shi <shy828301@gmail.com>
Cc: <stable@vger.kernel.org>	[6.7+]
