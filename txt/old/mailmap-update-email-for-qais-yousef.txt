From: Qais Yousef <qyousef@layalina.io>
Subject: mailmap: update email for Qais Yousef
Date: Fri, 14 Oct 2022 15:10:16 +0100

Update my email address for old entry and add a new entry for my
contribution while working with arm to continue support that work.

Link: https://lkml.kernel.org/r/20221014141016.539625-1-qyousef@layalina.io
Signed-off-by: Qais Yousef <qyousef@layalina.io>
Acked-by: Qais Yousef <qais.yousef@arm.com>
Acked-by: Qais Yousef <qsyousef@gmail.com>
