From: Jann Horn <jannh@google.com>
Subject: slub: Introduce CONFIG_SLUB_RCU_DEBUG
Date: Fri, 09 Aug 2024 17:36:56 +0200

  - move rcu_barrier() out of locked region (vbabka)
  - rearrange code in slab_free_after_rcu_debug (vbabka)

Link: https://lkml.kernel.org/r/20240809-kasan-tsbrcu-v8-2-aef4593f9532@google.com
Tested-by: syzbot+263726e59eab6b442723@syzkaller.appspotmail.com
Reviewed-by: Andrey Konovalov <andreyknvl@gmail.com>
Acked-by: Marco Elver <elver@google.com>
Acked-by: Vlastimil Babka <vbabka@suse.cz> (slab)
Signed-off-by: Jann Horn <jannh@google.com>
Cc: Alexander Potapenko <glider@google.com>
Cc: Andrey Ryabinin <ryabinin.a.a@gmail.com>
Cc: Christoph Lameter <cl@linux.com>
Cc: David Rientjes <rientjes@google.com>
Cc: David Sterba <dsterba@suse.cz>
Cc: Dmitry Vyukov <dvyukov@google.com>
Cc: Hyeonggon Yoo <42.hyeyoo@gmail.com>
Cc: Joonsoo Kim <iamjoonsoo.kim@lge.com>
Cc: Pekka Enberg <penberg@kernel.org>
Cc: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Vincenzo Frascino <vincenzo.frascino@arm.com>
