From: Mikhail Zaslonko <zaslonko@linux.ibm.com>
Subject: lib/zlib: DFLTCC always switch to software inflate for Z_PACKET_FLUSH option
Date: Thu, 26 Jan 2023 14:14:28 +0100

Since hardware inflate does not support Z_PACKET_FLUSH option (used
exclusively by kernel PPP driver), always switch to software like we
already do for Z_BLOCK flush option.  Without this patch, PPP might get
Z_DATA_ERROR return code from zlib_inflate() and disable zlib compression
for the packets.

Link: https://lkml.kernel.org/r/20230126131428.1222214-9-zaslonko@linux.ibm.com
Signed-off-by: Mikhail Zaslonko <zaslonko@linux.ibm.com>
Acked-by: Ilya Leoshkevich <iii@linux.ibm.com>
Cc: Heiko Carstens <hca@linux.ibm.com>
Cc: Vasily Gorbik <gor@linux.ibm.com>
