From: Andrii Nakryiko <andrii@kernel.org>
Subject: fs/procfs: remove build ID-related code duplication in PROCMAP_QUERY
Date: Mon, 29 Jul 2024 10:40:44 -0700

A piece of build ID handling code in PROCMAP_QUERY ioctl() was
accidentally duplicated.  It wasn't meant to be part of ed5d583a88a9
("fs/procfs: implement efficient VMA querying API for /proc/<pid>/maps")
commit, which is what introduced duplication.

It has no correctness implications, but we unnecessarily perform the same
work twice, if build ID parsing is requested.  Drop the duplication.

Link: https://lkml.kernel.org/r/20240729174044.4008399-1-andrii@kernel.org
Fixes: ed5d583a88a9 ("fs/procfs: implement efficient VMA querying API for /proc/<pid>/maps")
Signed-off-by: Andrii Nakryiko <andrii@kernel.org>
Reported-by: Jann Horn <jannh@google.com>
Cc: Alexey Dobriyan <adobriyan@gmail.com>
