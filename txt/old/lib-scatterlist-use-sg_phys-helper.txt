From: Sui Jingfeng <sui.jingfeng@linux.dev>
Subject: lib/scatterlist: use sg_phys() helper
Date: Tue, 29 Oct 2024 02:29:20 +0800

This shorten the length of code in horizential direction, therefore is
easier to read.

Link: https://lkml.kernel.org/r/20241028182920.1025819-1-sui.jingfeng@linux.dev
Signed-off-by: Sui Jingfeng <sui.jingfeng@linux.dev>
