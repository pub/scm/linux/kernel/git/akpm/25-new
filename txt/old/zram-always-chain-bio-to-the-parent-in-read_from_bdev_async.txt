From: Pankaj Raghav <p.raghav@samsung.com>
Subject: zram: always chain bio to the parent in read_from_bdev_async
Date: Mon, 3 Apr 2023 15:22:17 +0200

Patch series "remove page_endio()", v2.

It was decided to remove page_endio() as per the previous RFC
discussion[1] of this series and move that functionality into the caller
itself.  One of the side benefit of doing that is the callers have been
modified to directly work on folios as page_endio() already worked on
folios.

mpage changes were tested with a simple boot testing.  orangefs was tested
by Mike Marshall (No code changes since he tested).  Basic testing was
performed for the zram changes with fio and writeback to a backing device.


This patch (of 5):

zram_bvec_read() is called with the bio set to NULL only in
writeback_store() function.  When a writeback is triggered,
zram_bvec_read() is called only if ZRAM_WB flag is not set.  That will
result only calling zram_read_from_zspool() in __zram_bvec_read().

rw_page callback used to call read_from_bdev_async with a NULL parent bio
but that has been removed since commit 3222d8c2a7f8 ("block: remove
->rw_page").

We can now safely always call bio_chain() as read_from_bdev_async() will
be called with a parent bio set.  A WARN_ON_ONCE is added if this function
is called with parent set to NULL.

Link: https://lore.kernel.org/linux-mm/ZBHcl8Pz2ULb4RGD@infradead.org/ [1]
Link: https://lore.kernel.org/linux-mm/20230322135013.197076-1-p.raghav@samsung.com/ [2]
Link: https://lore.kernel.org/linux-mm/8adb0770-6124-e11f-2551-6582db27ed32@samsung.com/ [3]
Link: https://lkml.kernel.org/r/20230403132221.94921-2-p.raghav@samsung.com
Signed-off-by: Pankaj Raghav <p.raghav@samsung.com>
Acked-by: Minchan Kim <minchan@kernel.org>
Tested-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Christian Brauner <brauner@kernel.org>
Cc: Christoph Hellwig <hch@lst.de>
Cc: Jens Axboe <axboe@kernel.dk>
Cc: Luis Chamberlain <mcgrof@kernel.org>
Cc: Martin Brandenburg <martin@omnibond.com>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Mike Marshall <hubcap@omnibond.com>
