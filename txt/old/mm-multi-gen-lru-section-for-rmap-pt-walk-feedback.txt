From: "T.J. Alumbaugh" <talumbau@google.com>
Subject: mm: multi-gen LRU: section for rmap/PT walk feedback
Date: Wed, 18 Jan 2023 00:18:22 +0000

Add a section for lru_gen_look_around() in the code and the design doc.

Link: https://lkml.kernel.org/r/20230118001827.1040870-3-talumbau@google.com
Signed-off-by: T.J. Alumbaugh <talumbau@google.com>
Cc: Yu Zhao <yuzhao@google.com>
