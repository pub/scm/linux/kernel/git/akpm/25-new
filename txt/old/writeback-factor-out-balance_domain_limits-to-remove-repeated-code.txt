From: Kemeng Shi <shikemeng@huaweicloud.com>
Subject: writeback: factor out balance_domain_limits to remove repeated code
Date: Tue, 14 May 2024 20:52:52 +0800

Factor out balance_domain_limits to remove repeated code.

Link: https://lkml.kernel.org/r/20240514125254.142203-7-shikemeng@huaweicloud.com
Signed-off-by: Kemeng Shi <shikemeng@huaweicloud.com>
Acked-by: Tejun Heo <tj@kernel.org>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
