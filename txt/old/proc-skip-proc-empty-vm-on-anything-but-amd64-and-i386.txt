From: Alexey Dobriyan <adobriyan@gmail.com>
Subject: proc: skip proc-empty-vm on anything but amd64 and i386
Date: Fri, 30 Jun 2023 21:34:34 +0300

This test is arch specific, requires "munmap everything" primitive.

Link: https://lkml.kernel.org/r/20230630183434.17434-2-adobriyan@gmail.com
Signed-off-by: Alexey Dobriyan <adobriyan@gmail.com>
Cc: Björn Töpel <bjorn@kernel.org>
