From: Julia Lawall <Julia.Lawall@inria.fr>
Subject: selftests/vm/pkeys: fix typo in comment
Date: Sat, 21 May 2022 13:11:30 +0200

Spelling mistake (triple letters) in comment.  Detected with the help of
Coccinelle.

Link: https://lkml.kernel.org/r/20220521111145.81697-80-Julia.Lawall@inria.fr
Signed-off-by: Julia Lawall <Julia.Lawall@inria.fr>
Reviewed-by: Muchun Song <songmuchun@bytedance.com>
