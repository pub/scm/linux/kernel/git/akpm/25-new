From: Itaru Kitayama <itaru.kitayama@linux.dev>
Subject: tools/testing/selftests/mm/run_vmtests.sh: lower the ptrace permissions
Date: Mon, 30 Oct 2023 17:54:45 +0900

On Ubuntu and probably other distros, ptrace permissions are tightend a
bit by default; i.e., /proc/sys/kernel/yama/ptrace_score is set to 1. 
This cases memfd_secret's ptrace attach test fails with a permission
error.  Set it to 0 piror to running the program.  

Link: https://lkml.kernel.org/r/20231030-selftest-v1-1-743df68bb996@linux.dev
Signed-off-by: Itaru Kitayama <itaru.kitayama@linux.dev>
Cc: Shuah Khan <shuah@kernel.org>
