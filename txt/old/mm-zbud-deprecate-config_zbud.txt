From: Yosry Ahmed <yosry.ahmed@linux.dev>
Subject: mm: zbud: deprecate CONFIG_ZBUD
Date: Mon, 27 Jan 2025 23:58:21 +0000

Patch series "mm: zswap: deprecate zbud and remove z3fold".

After 2 cycles of deprecating z3fold, remove it and mark zbud as
deprecated for removal next.  It was initially thought that zbud cannot be
removed because it has no dependency on CONFIG_MMU while zsmalloc does,
but CONFIG_SWAP depends on CONFIG_MMU anyway.


This patch (of 2):

The zbud compressed pages allocator is rarely used, most users use
zsmalloc.  zbud consumes much more memory (only stores 1 or 2 compressed
pages per physical page).  The only advantage of zbud is a marginal
performance improvement that by no means justify the memory overhead.

Historically, zsmalloc had significantly worse latency than zbud and
z3fold but offered better memory savings.  This is no longer the case as
shown by a simple recent analysis [1].  In a kernel build test on tmpfs in
a limited cgroup, zbud 2-3% less time than zsmalloc, but at the cost of
using ~32% more memory (1.5G vs 1.13G).  The tradeoff does not make sense
for zbud in any practical scenario.

The only alleged advantage of zbud is not having the dependency on
CONFIG_MMU, but CONFIG_SWAP already depends on CONFIG_MMU anyway, and zbud
is only used by zswap.

Following in the footsteps of [2], which deprecated z3fold, deprecated
zbud as planned and remove it in a few cycles if no objections are raised
from active users.

Rename the user-visible config options so that users with CONFIG_ZBUD=y
get a new prompt with explanation during make oldconfig.  Also, remove
CONFIG_ZBUD from defconfig.

[1]https://lore.kernel.org/lkml/CAJD7tkbRF6od-2x_L8-A1QL3=2Ww13sCj4S3i4bNndqF+3+_Vg@mail.gmail.com/
[2]https://lore.kernel.org/lkml/20240904233343.933462-1-yosryahmed@google.com/

Link: https://lkml.kernel.org/r/Z5gdlO5pOu9XeGce@google.com
Link: https://lkml.kernel.org/r/Z5gdnSX5Lv-nfjQL@google.com
Signed-off-by: Yosry Ahmed <yosry.ahmed@linux.dev>
Acked-by: Nhat Pham <nphamcs@gmail.com>
Reviewed-by: Shakeel Butt <shakeel.butt@linux.dev>
Cc: Chengming Zhou <chengming.zhou@linux.dev>
Cc: Huacai Chen <chenhuacai@kernel.org>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: linmiaohe <linmiaohe@huawei.com>
Cc: Vitaly Wool <vitaly.wool@konsulko.com>
Cc: WANG Xuerui <kernel@xen0n.name>
