From: Nathan Chancellor <nathan@kernel.org>
Subject: mm-move-follow_phys-to-arch-x86-mm-pat-memtypec-fix-2
Date: Thu, 28 Mar 2024 08:25:58 -0700

memtype.c needs highmem.h

Link: https://lkml.kernel.org/r/20240328152558.GA2652500@dev-arch.thelio-3990X
Signed-off-by: Nathan Chancellor <nathan@kernel.org>
Cc: Andy Lutomirski <luto@kernel.org>
Cc: Christoph Hellwig <hch@lst.de>
Cc: Dave Hansen <dave.hansen@linux.intel.com>
Cc: David Hildenbrand <david@redhat.com>
Cc: Fei Li <fei1.li@intel.com>
Cc: Ingo Molnar <mingo@kernel.org>
Cc: Peter Zijlstra <peterz@infradead.org>
