From: Nathan Chancellor <nathan@kernel.org>
Subject: hexagon: traps: add internal prototypes for functions only called from asm
Date: Thu, 30 Nov 2023 15:58:32 -0700

Clang warns:

  arch/hexagon/kernel/traps.c:284:6: warning: no previous prototype for function 'do_genex' [-Wmissing-prototypes]
    284 | void do_genex(struct pt_regs *regs)
        |      ^
  arch/hexagon/kernel/traps.c:284:1: note: declare 'static' if the function is not intended to be used outside of this translation unit
    284 | void do_genex(struct pt_regs *regs)
        | ^
        | static
  arch/hexagon/kernel/traps.c:341:6: warning: no previous prototype for function 'do_trap0' [-Wmissing-prototypes]
    341 | void do_trap0(struct pt_regs *regs)
        |      ^
  arch/hexagon/kernel/traps.c:341:1: note: declare 'static' if the function is not intended to be used outside of this translation unit
    341 | void do_trap0(struct pt_regs *regs)
        | ^
        | static
  arch/hexagon/kernel/traps.c:418:6: warning: no previous prototype for function 'do_machcheck' [-Wmissing-prototypes]
    418 | void do_machcheck(struct pt_regs *regs)
        |      ^
  arch/hexagon/kernel/traps.c:418:1: note: declare 'static' if the function is not intended to be used outside of this translation unit
    418 | void do_machcheck(struct pt_regs *regs)
        | ^
        | static
  arch/hexagon/kernel/traps.c:428:6: warning: no previous prototype for function 'do_debug_exception' [-Wmissing-prototypes]
    428 | void do_debug_exception(struct pt_regs *regs)
        |      ^
  arch/hexagon/kernel/traps.c:428:1: note: declare 'static' if the function is not intended to be used outside of this translation unit
    428 | void do_debug_exception(struct pt_regs *regs)
        | ^
        | static

These functions are only called from assembly or this translation unit,
so just add prototypes right above the definitions to silence the
warnings.

Link: https://lkml.kernel.org/r/20231130-hexagon-missing-prototypes-v1-19-5c34714afe9e@kernel.org
Signed-off-by: Nathan Chancellor <nathan@kernel.org>
Cc: Arnd Bergmann <arnd@arndb.de>
Cc: Brian Cain <bcain@quicinc.com>
