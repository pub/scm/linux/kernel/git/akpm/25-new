From: Sidhartha Kumar <sidhartha.kumar@oracle.com>
Subject: mm/hugetlb: set head flag before setting compound_order in __prep_compound_gigantic_folio
Date: Mon, 12 Dec 2022 14:55:29 -0800

folio_set_compound_order() checks if the passed in folio is a large folio.
A large folio is indicated by the PG_head flag.  Call __folio_set_head()
before setting the order.

Link: https://lkml.kernel.org/r/20221212225529.22493-1-sidhartha.kumar@oracle.com
Fixes: d1c6095572d0 ("mm/hugetlb: convert hugetlb prep functions to folios")
Signed-off-by: Sidhartha Kumar <sidhartha.kumar@oracle.com>
Reported-by: David Hildenbrand <david@redhat.com>
