From: Andrew Morton <akpm@linux-foundation.org>
Subject: init-add-bdev-fs-printk-if-mount_block_root-failed-fix
Date: Thu May 18 12:58:30 PM PDT 2023

fix spelling in printk message

Cc: Angus Chen <angus.chen@jaguarmicro.com>
