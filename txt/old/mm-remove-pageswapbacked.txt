From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: remove PageSwapBacked
Date: Wed, 21 Aug 2024 20:34:35 +0100

This flag is now only used on folios, so we can remove all the page
accessors.

Link: https://lkml.kernel.org/r/20240821193445.2294269-3-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
