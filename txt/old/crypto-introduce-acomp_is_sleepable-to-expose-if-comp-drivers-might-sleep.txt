From: Barry Song <v-songbaohua@oppo.com>
Subject: crypto: introduce acomp_is_sleepable to expose if comp drivers might sleep
Date: Sat, 17 Feb 2024 17:51:00 +1300

Patch series "mm/zswap & crypto/compress: remove a couple of memcpy", v3.

The patchset removes a couple of memcpy in zswap and crypto to improve
zswap's performance.

Thanks for Chengming Zhou's test and perf data.
Quote from Chengming,
 I just tested these three patches on my server, found improvement in the
 kernel build testcase on a tmpfs with zswap (lz4 + zsmalloc) enabled.
 
         mm-stable 501a06fe8e4c  patched
 real    1m38.028s               1m32.317s
 user    19m11.482s              18m39.439s
 sys     19m26.445s              17m5.646s


This patch (of 3):

Almost all CPU-based compressors/decompressors are actually synchronous
though they support acomp APIs.  While some hardware has hardware-based
accelerators to offload CPU's work such as hisilicon and intel/qat/, their
drivers are working in async mode.  Letting acomp's users know exactly if
the acomp is really async will help users know if the compression and
decompression procedure can sleep.

Generally speaking, async and sleepable are semantically similar but not
equal.  But for compress drivers, they are actually equal at least due to
the below facts.

Firstly, scompress drivers - crypto/deflate.c, lz4.c, zstd.c, lzo.c etc
have no sleep.  Secondly, zRAM has been using these scompress drivers for
years in atomic contexts, and never worried those drivers going to sleep.

Link: https://lkml.kernel.org/r/20240217045102.55339-1-21cnbao@gmail.com
Link: https://lkml.kernel.org/r/20240217045102.55339-2-21cnbao@gmail.com
Signed-off-by: Barry Song <v-songbaohua@oppo.com>
Tested-by: Chengming Zhou <zhouchengming@bytedance.com>
Cc: Barry Song <v-songbaohua@oppo.com>
Cc: Chris Li <chrisl@kernel.org>
Cc: Dan Streetman <ddstreet@ieee.org>
Cc: David S. Miller <davem@davemloft.net>
Cc: Herbert Xu <herbert@gondor.apana.org.au>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Nhat Pham <nphamcs@gmail.com>
Cc: Seth Jennings <sjenning@redhat.com>
Cc: Vitaly Wool <vitaly.wool@konsulko.com>
Cc: Yosry Ahmed <yosryahmed@google.com>
