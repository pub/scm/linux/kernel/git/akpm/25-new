From: SeongJae Park <sj@kernel.org>
Subject: mm/damon/sysfs-schemes: support PSI-based quota auto-tune
Date: Mon, 19 Feb 2024 11:44:25 -0800

Extend DAMON sysfs interface to support the PSI-based quota auto-tuning by
adding a new file, 'target_metric' under the quota goal directory.  Old
users don't get any behavioral changes since the default value of the
metric is 'user input'.

Link: https://lkml.kernel.org/r/20240219194431.159606-15-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
