From: Geert Uytterhoeven <geert+renesas@glider.be>
Subject: kbuild: drop support for include/asm-<arch> in headers_check.pl
Date: Thu, 5 Dec 2024 14:20:43 +0100

"include/asm-<arch>" was replaced by "arch/<arch>/include/asm" a long time
ago.  All assembler header files are now included using "#include
<asm/*>", so there is no longer a need to rewrite paths.

Link: https://lkml.kernel.org/r/19fb5b49396239d28020015ba2640d77dacdb6c2.1733404444.git.geert+renesas@glider.be
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
Cc: Andy Whitcroft <apw@canonical.com>
Cc: Arnd Bergmann <arnd@arndb.de>
Cc: Dwaipayan Ray <dwaipayanray1@gmail.com>
Cc: Joe Perches <joe@perches.com>
Cc: Lukas Bulwahn <lukas.bulwahn@gmail.com>
Cc: Masahiro Yamada <masahiroy@kernel.org>
Cc: Nathan Chancellor <nathan@kernel.org>
Cc: Nicolas Schier <nicolas@fjasle.eu>
Cc: Oleg Nesterov <oleg@redhat.com>
Cc: Rasmus Villemoes <linux@rasmusvillemoes.dk>
Cc: Yury Norov <yury.norov@gmail.com>
