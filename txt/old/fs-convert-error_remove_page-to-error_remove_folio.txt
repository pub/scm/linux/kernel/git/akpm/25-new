From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: fs: convert error_remove_page to error_remove_folio
Date: Fri, 17 Nov 2023 16:14:47 +0000

There were already assertions that we were not passing a tail page to
error_remove_page(), so make the compiler enforce that by converting
everything to pass and use a folio.

Link: https://lkml.kernel.org/r/20231117161447.2461643-7-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Naoya Horiguchi <naoya.horiguchi@nec.com>
