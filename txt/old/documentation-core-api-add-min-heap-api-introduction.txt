From: Kuan-Wei Chiu <visitorckw@gmail.com>
Subject: Documentation/core-api: add min heap API introduction
Date: Sun, 20 Oct 2024 12:02:00 +0800

Introduce an overview of the min heap API, detailing its usage and
functionality.  The documentation aims to provide developers with a clear
understanding of how to implement and utilize min heaps within the Linux
kernel, enhancing the overall accessibility of this data structure.

Link: https://lkml.kernel.org/r/20241020040200.939973-11-visitorckw@gmail.com
Signed-off-by: Kuan-Wei Chiu <visitorckw@gmail.com>
Reviewed-by: Bagas Sanjaya <bagasdotme@gmail.com>
Cc: Adrian Hunter <adrian.hunter@intel.com>
Cc: Arnaldo Carvalho de Melo <acme@kernel.org>
Cc: Ching-Chun (Jim) Huang <jserv@ccns.ncku.edu.tw>
Cc: Coly Li <colyli@suse.de>
Cc: Ian Rogers <irogers@google.com>
Cc: Ingo Molnar <mingo@redhat.com>
Cc: Jiri Olsa <jolsa@kernel.org>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Kent Overstreet <kent.overstreet@linux.dev>
Cc: "Liang, Kan" <kan.liang@linux.intel.com>
Cc: Mark Rutland <mark.rutland@arm.com>
Cc: Matthew Sakai <msakai@redhat.com>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Namhyung Kim <namhyung@kernel.org>
Cc: Peter Zijlstra <peterz@infradead.org>
