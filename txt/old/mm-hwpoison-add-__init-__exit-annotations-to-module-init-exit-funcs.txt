From: Xiu Jianfeng <xiujianfeng@huawei.com>
Subject: mm/hwpoison: add __init/__exit annotations to module init/exit funcs
Date: Tue, 6 Sep 2022 17:35:30 +0800

Add missing __init/__exit annotations to module init/exit funcs.

Link: https://lkml.kernel.org/r/20220906093530.243262-1-xiujianfeng@huawei.com
Signed-off-by: Xiu Jianfeng <xiujianfeng@huawei.com>
Reviewed-by: Miaohe Lin <linmiaohe@huawei.com>
Acked-by: Naoya Horiguchi <naoya.horiguchi@nec.com>
