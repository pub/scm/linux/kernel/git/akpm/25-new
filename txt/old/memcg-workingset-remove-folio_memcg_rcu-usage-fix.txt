From: Andrew Morton <akpm@linux-foundation.org>
Subject: memcg-workingset-remove-folio_memcg_rcu-usage-fix
Date: Sun Oct 27 05:59:41 PM PDT 2024

fix build: provide folio_memcg_charged stub for CONFIG_MEMCG=n

Reported-by: Stephen Rothwell <sfr@canb.auug.org.au>
Cc: Hugh Dickins <hughd@google.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Michal Hocko <mhocko@suse.com>
Cc: Muchun Song <songmuchun@bytedance.com>
Cc: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Shakeel Butt <shakeel.butt@linux.dev>
Cc: Yosry Ahmed <yosryahmed@google.com>
Cc: Yu Zhao <yuzhao@google.com>
