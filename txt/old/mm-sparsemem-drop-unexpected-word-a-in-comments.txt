From: "XueBing Chen" <chenxuebing@jari.cn>
Subject: mm: sparsemem: drop unexpected word 'a' in comments
Date: Sat, 25 Jun 2022 16:51:35 +0800 (GMT+08:00)

there is an unexpected word 'a' in the comments that need to be dropped

Link: https://lkml.kernel.org/r/24fbdae3.c86.1819a0f31b9.Coremail.chenxuebing@jari.cn
Signed-off-by: XueBing Chen <chenxuebing@jari.cn>
