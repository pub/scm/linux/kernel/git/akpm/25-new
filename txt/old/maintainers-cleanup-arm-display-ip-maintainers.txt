From: Liviu Dudau <liviu.dudau@arm.com>
Subject: MAINTAINERS: Cleanup Arm Display IP maintainers
Date: Wed, 10 May 2023 13:28:11 +0100

Some people have moved to different roles and are no longer involved in
the upstream development.  As there is only one person left, remove the
mailing list as well as it serves no purpose.

Link: https://lkml.kernel.org/r/20230510122811.1872358-1-liviu.dudau@arm.com
Signed-off-by: Liviu Dudau <liviu.dudau@arm.com>
Acked-by: Brian Starkey <brian.starkey@arm.com>
Cc: Mihail Atanassov <mihail.atanassov@arm.com>
Cc: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Cc: Joe Perches <joe@perches.com> # "Please use --order"
Cc: Mihail Atanassov <mihail.atanassov@arm.com>
Cc: Uwe Kleine-König <u.kleine-koenig@pengutronix.de>
