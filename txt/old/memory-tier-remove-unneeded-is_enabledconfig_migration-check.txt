From: Miaohe Lin <linmiaohe@huawei.com>
Subject: memory tier: remove unneeded !IS_ENABLED(CONFIG_MIGRATION) check
Date: Sat, 10 Jun 2023 11:41:14 +0800

establish_demotion_targets() is defined while CONFIG_MIGRATION is
enabled. There's no need to check it again.

Link: https://lkml.kernel.org/r/20230610034114.981861-1-linmiaohe@huawei.com
Signed-off-by: Miaohe Lin <linmiaohe@huawei.com>
Reviewed-by: David Hildenbrand <david@redhat.com>
Reviewed-by: Yang Shi <shy828301@gmail.com>
