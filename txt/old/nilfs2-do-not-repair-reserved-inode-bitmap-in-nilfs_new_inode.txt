From: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Subject: nilfs2: do not repair reserved inode bitmap in nilfs_new_inode()
Date: Tue, 27 Aug 2024 02:41:13 +0900

After commit 93aef9eda1ce ("nilfs2: fix incorrect inode allocation from
reserved inodes") is applied, the inode number returned by
nilfs_ifile_create_inode() is guaranteed to always be greater than or
equal to NILFS_USER_INO, so if the inode number is a reserved inode number
(less than NILFS_USER_INO), the code to repair the bitmap immediately
following it is no longer executed.  So, delete it.

Link: https://lkml.kernel.org/r/20240826174116.5008-6-konishi.ryusuke@gmail.com
Signed-off-by: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Cc: Huang Xiaojia <huangxiaojia2@huawei.com>
