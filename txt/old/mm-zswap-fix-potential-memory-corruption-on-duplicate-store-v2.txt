From: Domenico Cerasuolo <cerasuolodomenico@gmail.com>
Subject: mm-zswap-fix-potential-memory-corruption-on-duplicate-store-v2
Date: Mon, 25 Sep 2023 15:00:02 +0200

add a warning and a comment to the second duplicates check in
zswap_store() (Johannes).

Link: https://lkml.kernel.org/r/20230925130002.1929369-1-cerasuolodomenico@gmail.com
Fixes: 42c06a0e8ebe ("mm: kill frontswap")
Signed-off-by: Domenico Cerasuolo <cerasuolodomenico@gmail.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Nhat Pham <nphamcs@gmail.com>
Cc: Dan Streetman <ddstreet@ieee.org>
Cc: Domenico Cerasuolo <cerasuolodomenico@gmail.com>
Cc: Seth Jennings <sjenning@redhat.com>
Cc: Vitaly Wool <vitaly.wool@konsulko.com>
