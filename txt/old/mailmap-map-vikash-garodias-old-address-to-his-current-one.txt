From: Konrad Dybcio <konrad.dybcio@linaro.org>
Subject: mailmap: map Vikash Garodia's old address to his current one
Date: Tue, 28 Feb 2023 16:33:35 +0100

Vikash's old email is still picked up by the likes of get_maintainer.pl
and keeps bouncing.  Map it to his current one.

Link: https://lkml.kernel.org/r/20230228153335.907164-3-konrad.dybcio@linaro.org
Signed-off-by: Konrad Dybcio <konrad.dybcio@linaro.org>
Cc: Vikash Garodia <quic_vgarodia@quicinc.com>
