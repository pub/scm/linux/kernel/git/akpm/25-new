From: Suren Baghdasaryan <surenb@google.com>
Subject: mm: mark VMA as being written when changing vm_flags
Date: Mon, 27 Feb 2023 09:36:12 -0800

Updates to vm_flags have to be done with VMA marked as being written for
preventing concurrent page faults or other modifications.

Link: https://lkml.kernel.org/r/20230227173632.3292573-14-surenb@google.com
Signed-off-by: Suren Baghdasaryan <surenb@google.com>
