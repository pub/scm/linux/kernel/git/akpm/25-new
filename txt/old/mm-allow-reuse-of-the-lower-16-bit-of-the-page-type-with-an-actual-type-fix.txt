From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-allow-reuse-of-the-lower-16-bit-of-the-page-type-with-an-actual-type-fix
Date: Wed May 29 12:04:52 PM PDT 2024

fix PG_hugetlb typo, per David

Cc: David Hildenbrand <david@redhat.com>
Cc: Hyeonggon Yoo <42.hyeyoo@gmail.com>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Mike Rapoport (IBM) <rppt@kernel.org>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Sergey Senozhatsky <senozhatsky@chromium.org>
