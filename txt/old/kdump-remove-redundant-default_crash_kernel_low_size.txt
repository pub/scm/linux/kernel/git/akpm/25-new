From: Youling Tang <tangyouling@kylinos.cn>
Subject: kdump: remove redundant DEFAULT_CRASH_KERNEL_LOW_SIZE
Date: Wed, 27 Dec 2023 07:46:25 +0800

Remove duplicate definitions, no functional changes.

Link: https://lkml.kernel.org/r/MW4PR84MB3145459ADC7EB38BBB36955B8198A@MW4PR84MB3145.NAMPRD84.PROD.OUTLOOK.COM
Signed-off-by: Youling Tang <tangyouling@kylinos.cn>
Reported-by: Huacai Chen <chenhuacai@loongson.cn>
Acked-by: Baoquan He <bhe@redhat.com>
