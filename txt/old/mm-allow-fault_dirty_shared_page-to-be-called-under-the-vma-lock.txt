From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: allow fault_dirty_shared_page() to be called under the VMA lock
Date: Sat, 12 Aug 2023 01:20:33 +0100

By making maybe_unlock_mmap_for_io() handle the VMA lock correctly, we
make fault_dirty_shared_page() safe to be called without the mmap lock
held.

Link: https://lkml.kernel.org/r/20230812002033.1002367-1-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reported-by: David Hildenbrand <david@redhat.com>
Tested-by: Suren Baghdasaryan <surenb@google.com>
