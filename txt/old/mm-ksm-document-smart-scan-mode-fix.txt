From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-ksm-document-smart-scan-mode-fix
Date: Tue Sep 26 05:49:41 PM PDT 2023

fix typo

Cc: David Hildenbrand <david@redhat.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Rik van Riel <riel@surriel.com>
Cc: Stefan Roesch <shr@devkernel.io>
