From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: remove mlock_vma_page()
Date: Mon, 16 Jan 2023 19:28:25 +0000

All callers now have a folio and can call mlock_vma_folio().  Update the
documentation to refer to mlock_vma_folio().

Link: https://lkml.kernel.org/r/20230116192827.2146732-3-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
