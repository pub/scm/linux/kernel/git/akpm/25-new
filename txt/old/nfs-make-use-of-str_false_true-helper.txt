From: Hongbo Li <lihongbo22@huawei.com>
Subject: nfs make use of str_false_true helper
Date: Tue, 27 Aug 2024 10:45:17 +0800

The helper str_false_true() was introduced to return "false/true" string
literal.  We can simplify this format by str_false_true.

Link: https://lkml.kernel.org/r/20240827024517.914100-4-lihongbo22@huawei.com
Signed-off-by: Hongbo Li <lihongbo22@huawei.com>
Cc: Andy Shevchenko <andy@kernel.org>
Cc: Anna Schumaker <anna@kernel.org>
Cc: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Cc: Kees Cook <kees@kernel.org>
Cc: Trond Myklebust <trondmy@kernel.org>
Cc: Matthew Wilcox <willy@infradead.org>
