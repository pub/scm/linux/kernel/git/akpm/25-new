From: Vincent Whitchurch <vincent.whitchurch@axis.com>
Subject: squashfs: fix page update race
Date: Fri, 26 May 2023 15:25:45 +0200

We only put the page into the cache after we've read it, so the
PageUptodate() check should not be necessary.  In fact, it's actively
harmful since the check could fail (since we used find_get_page() and not
find_lock_page()) and we could end up submitting a page for I/O after it
has been read and while it's actively being used, which could lead to
corruption depending on what the block driver does with it.

Link: https://lkml.kernel.org/r/20230526-squashfs-cache-fixup-v1-1-d54a7fa23e7b@axis.com
Signed-off-by: Vincent Whitchurch <vincent.whitchurch@axis.com>
Reviewed-by: Christoph Hellwig <hch@lst.de>
Reviewed-by: Phillip Lougher <phillip@squashfs.org.uk>
