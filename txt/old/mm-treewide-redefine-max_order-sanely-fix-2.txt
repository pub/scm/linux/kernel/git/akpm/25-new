From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-treewide-redefine-max_order-sanely-fix-2
Date: Wed Mar 15 01:54:45 PM PDT 2023

fix another min_t warning

Cc: "Kirill A. Shutemov" <kirill@shutemov.name>
Cc: kernel test robot <lkp@intel.com>
Cc: "Kirill A. Shutemov" <kirill.shutemov@linux.intel.com>
Cc: Zi Yan <ziy@nvidia.com>
