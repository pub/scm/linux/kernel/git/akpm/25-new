From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-userfaultfd-support-wp-on-multiple-vmas-fix
Date: Thu Feb 23 03:13:06 PM PST 2023

s/VM_WARN_ON_ONCE/VM_WARN_ONCE/ to fix build

Cc: Muhammad Usama Anjum <usama.anjum@collabora.com>
Cc: Peter Xu <peterx@redhat.com>
Cc: David Hildenbrand <david@redhat.com>
Cc: Paul Gofman <pgofman@codeweavers.com>
