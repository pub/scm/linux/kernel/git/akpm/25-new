From: Koudai Iwahori <koudai@google.com>
Subject: scripts/gdb: fix lx-symbols command for arm64 LLVM
Date: Tue, 1 Aug 2023 05:10:52 -0700

lx-symbols assumes that module's .text sections is located at
`module->mem[MOD_TEXT].base` and passes it to add-symbol-file command. 
However, .text section follows after .plt section in modules built by LLVM
toolchain for arm64 target.  Symbol addresses are skewed in GDB.

Fix this issue by using the address of .text section stored in
`module->sect_attrs`.

Link: https://lkml.kernel.org/r/20230801121052.2475183-1-koudai@google.com
Signed-off-by: Koudai Iwahori <koudai@google.com>
Cc: Jan Kiszka <jan.kiszka@siemens.com>
Cc: Kieran Bingham <kbingham@kernel.org>
