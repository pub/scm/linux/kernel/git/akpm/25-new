From: "T.J. Alumbaugh" <talumbau@google.com>
Subject: mm: multi-gen LRU: improve design doc
Date: Tue, 14 Feb 2023 03:54:45 +0000

This patch improves the design doc. Specifically,
  1. add a section for the per-memcg mm_struct list, and
  2. add a section for the PID controller.

Link: https://lkml.kernel.org/r/20230214035445.1250139-2-talumbau@google.com
Signed-off-by: T.J. Alumbaugh <talumbau@google.com>
Cc: Yu Zhao <yuzhao@google.com>
