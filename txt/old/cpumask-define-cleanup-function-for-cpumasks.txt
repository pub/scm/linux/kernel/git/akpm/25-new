From: Yury Norov <yury.norov@gmail.com>
Subject: cpumask: define cleanup function for cpumasks
Date: Thu, 28 Dec 2023 12:09:34 -0800

Now we can simplify a code that allocates cpumasks for local needs.

Link: https://lkml.kernel.org/r/20231228200936.2475595-8-yury.norov@gmail.com
Signed-off-by: Yury Norov <yury.norov@gmail.com>
Cc: Andy Shevchenko <andriy.shevchenko@linux.intel.com>
Cc: Ming Lei <ming.lei@redhat.com>
Cc: Rasmus Villemoes <linux@rasmusvillemoes.dk>
Cc: Thomas Gleixner <tglx@linutronix.de>
