From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-fix-a-potential-infinite-loop-in-start_isolate_page_range-fix
Date: Wed May 25 10:34:12 AM PDT 2022

suppressmin() warning

Cc: Christophe Leroy <christophe.leroy@csgroup.eu>
Cc: David Hildenbrand <david@redhat.com>
Cc: Eric Ren <renzhengeek@gmail.com>
Cc: Mel Gorman <mgorman@techsingularity.net>
Cc: Mike Rapoport <rppt@linux.ibm.com>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Oscar Salvador <osalvador@suse.de>
Cc: Qian Cai <quic_qiancai@quicinc.com>
Cc: Vlastimil Babka <vbabka@suse.cz>
Cc: Zi Yan <ziy@nvidia.com>
Cc: Stephen Rothwell <sfr@canb.auug.org.au>
