From: Andrew Morton <akpm@linux-foundation.org>
Subject: kasan-stop-leaking-stack-trace-handles-fix
Date: Wed Dec 27 01:21:02 PM PST 2023

make release_free_meta() and release_alloc_meta)( static

Reported-by: kernel test robot <lkp@intel.com>
Closes: https://lore.kernel.org/oe-kbuild-all/202312280213.6j147JJb-lkp@intel.Cc: Alexander Potapenko <glider@google.com>
Cc: Andrey Konovalov <andreyknvl@google.com>
Cc: Andrey Ryabinin <ryabinin.a.a@gmail.com>
Cc: Dmitry Vyukov <dvyukov@google.com>
Cc: Marco Elver <elver@google.com>
