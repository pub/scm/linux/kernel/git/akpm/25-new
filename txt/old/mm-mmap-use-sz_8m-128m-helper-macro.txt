From: Yajun Deng <yajun.deng@linux.dev>
Subject: mm/mmap: use SZ_{8M, 128M} helper macro
Date: Fri, 26 Jan 2024 16:59:05 +0800

Use SZ_{8M, 128M} macro intead of the number in init_user_reserve and
reserve_mem_notifier.

Link: https://lkml.kernel.org/r/20240126085905.2835513-1-yajun.deng@linux.dev
Signed-off-by: Yajun Deng <yajun.deng@linux.dev>
