From: Mel Gorman <mgorman@techsingularity.net>
Subject: kernel-fork-convert-vma-assignment-to-a-memcpy-fix
Date: Thu, 26 Jan 2023 11:52:24 +0000

preserve data_race()

Link: https://lkml.kernel.org/r/20230126115224.3urhskf35eomk7xl@techsingularity.net
Signed-off-by: Mel Gorman <mgorman@techsingularity.net>
Cc: Michal Hocko <mhocko@suse.com>
Cc: Suren Baghdasaryan <surenb@google.com>
