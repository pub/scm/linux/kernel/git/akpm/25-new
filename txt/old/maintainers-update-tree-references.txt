From: Andrew Morton <akpm@linux-foundation.org>
Subject: MAINTAINERS: update MM tree references
Date: Wed Jun 15 02:22:44 PM PDT 2022

Describe the new kernel.org location of the MM trees.

Suggested-by: David Hildenbrand <david@redhat.com>
Cc: Muchun Song <songmuchun@bytedance.com>
Cc: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Cc: Miaohe Lin <linmiaohe@huawei.com>
