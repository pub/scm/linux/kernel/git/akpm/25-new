From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: shmem: add shmem_get_folio()
Date: Fri, 2 Sep 2022 20:46:20 +0100

With no remaining callers of shmem_getpage_gfp(), add shmem_get_folio()
and reimplement shmem_getpage() as a call to shmem_get_folio().

Link: https://lkml.kernel.org/r/20220902194653.1739778-25-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
