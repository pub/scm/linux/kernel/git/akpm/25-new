From: wuchi <wuchi.zero@gmail.com>
Subject: relay: use kvcalloc to alloc page array in relay_alloc_page_array
Date: Fri, 9 Sep 2022 18:10:25 +0800

kvcalloc() is safer because it will check the integer overflows, and using
it will simple the logic of allocation size.

Link: https://lkml.kernel.org/r/20220909101025.82955-1-wuchi.zero@gmail.com
Signed-off-by: wuchi <wuchi.zero@gmail.com>
Cc: Christoph Hellwig <hch@lst.de>
Cc: Jens Axboe <axboe@kernel.dk>
