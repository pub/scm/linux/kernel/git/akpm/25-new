From: Yang Yingliang <yangyingliang@huawei.com>
Subject: mm/damon: change damon_lru_sort_stub_pattern to static
Date: Sat, 17 Sep 2022 20:12:28 +0800

damon_lru_sort_stub_pattern is only used in lru_sort.c now, change it to
static.

Link: https://lkml.kernel.org/r/20220917121228.1889699-1-yangyingliang@huawei.com
Fixes: 27812ec2a990 ("mm/damon: simplify scheme create in lru_sort.c")
Signed-off-by: Yang Yingliang <yangyingliang@huawei.com>
Cc: SeongJae Park <sj@kernel.org>
Cc: Xin Hao <xhao@linux.alibaba.com>
