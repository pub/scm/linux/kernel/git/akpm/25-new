From: Liao Chang <liaochang1@huawei.com>
Subject: checkpatch: add check for array allocator family argument order
Date: Fri, 4 Nov 2022 15:05:23 +0800

These array allocator family are sometimes misused with the first and
second arguments switched.

Same issue with calloc, kvcalloc, kvmalloc_array etc.

Bleat if sizeof is the first argument.

Link: https://lore.kernel.org/lkml/5374345c-7973-6a3c-d559-73bf4ac15079@redhat.com/
Link: https://lkml.kernel.org/r/20221104070523.60296-1-liaochang1@huawei.com
Signed-off-by: Liao Chang <liaochang1@huawei.com>
Acked-by: Joe Perches <joe@perches.com>
Cc: Lukas Bulwahn <lukas.bulwahn@gmail.com>
Cc: Christophe JAILLET <christophe.jaillet@wanadoo.fr>
Cc: Andy Whitcroft <apw@canonical.com>
Cc: Bagas Sanjaya <bagasdotme@gmail.com>
Cc: Dwaipayan Ray <dwaipayanray1@gmail.com>
Cc: Paolo Bonzini <pbonzini@redhat.com>
