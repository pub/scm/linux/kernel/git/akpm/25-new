From: Frank van der Linden <fvdl@google.com>
Subject: mm/hugetlb: fix round-robin bootmem allocation
Date: Thu, 6 Feb 2025 18:50:45 +0000

Commit b5389086ad7b ("hugetlbfs: extend the definition of hugepages
parameter to support node allocation") changed the NUMA_NO_NODE
round-robin allocation behavior in case of a failure to allocate from one
NUMA node.  The code originally moved on to the next node to try again,
but now it immediately breaks out of the loop.

Restore the original behavior.

Link: https://lkml.kernel.org/r/20250206185109.1210657-6-fvdl@google.com
Fixes: b5389086ad7b ("hugetlbfs: extend the definition of hugepages parameter to support node allocation")
Signed-off-by: Frank van der Linden <fvdl@google.com>
Cc: Zhenguo Yao <yaozhenguo1@gmail.com>
Cc: Alexander Gordeev <agordeev@linux.ibm.com>
Cc: Andy Lutomirski <luto@kernel.org>
Cc: Dave Hansen <dave.hansen@linux.intel.com>
Cc: Heiko Carstens <hca@linux.ibm.com>
Cc: Joao Martins <joao.m.martins@oracle.com>
Cc: Madhavan Srinivasan <maddy@linux.ibm.com>
Cc: Michael Ellerman <mpe@ellerman.id.au>
Cc: Muchun Song <muchun.song@linux.dev>
Cc: Peter Zijlstra <peterz@infradead.org>
Cc: Roman Gushchin (Cruise) <roman.gushchin@linux.dev>
Cc: Usama Arif <usamaarif642@gmail.com>
Cc: Vasily Gorbik <gor@linux.ibm.com>
Cc: Yu Zhao <yuzhao@google.com>
Cc: Oscar Salvador <osalvador@suse.de>
