From: Eric DeVolder <eric.devolder@oracle.com>
Subject: x86/kexec: refactor for kernel/Kconfig.kexec
Date: Wed, 12 Jul 2023 12:15:33 -0400

The kexec and crash kernel options are provided in the common
kernel/Kconfig.kexec. Utilize the common options and provide
the ARCH_SUPPORTS_ and ARCH_SELECTS_ entries to recreate the
equivalent set of KEXEC and CRASH options.

Link: https://lkml.kernel.org/r/20230712161545.87870-3-eric.devolder@oracle.com
Signed-off-by: Eric DeVolder <eric.devolder@oracle.com>
