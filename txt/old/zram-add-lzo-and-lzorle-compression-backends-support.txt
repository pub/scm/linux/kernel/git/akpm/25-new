From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: zram: add lzo and lzorle compression backends support
Date: Mon, 2 Sep 2024 19:55:53 +0900

Add s/w lzo/lzorle compression support.

Link: https://lkml.kernel.org/r/20240902105656.1383858-6-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Nick Terrell <terrelln@fb.com>
