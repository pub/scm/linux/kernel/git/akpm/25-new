From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: proc: pass a folio to smaps_page_accumulate()
Date: Wed, 3 Apr 2024 18:14:54 +0100

Both callers already have a folio; pass it in instead of doing the
conversion each time.

Link: https://lkml.kernel.org/r/20240403171456.1445117-4-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Christian Brauner <brauner@kernel.org>
