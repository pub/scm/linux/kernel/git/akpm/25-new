From: Kefeng Wang <wangkefeng.wang@huawei.com>
Subject: mm: compaction: convert to folio_isolate_movable()
Date: Thu, 29 Aug 2024 22:54:55 +0800

Directly use folio_isolate_movable() in isolate_migratepages_block().

Link: https://lkml.kernel.org/r/20240829145456.2591719-5-wangkefeng.wang@huawei.com
Signed-off-by: Kefeng Wang <wangkefeng.wang@huawei.com>
Reviewed-by: Vishal Moola (Oracle) <vishal.moola@gmail.com>
Cc: Baolin Wang <baolin.wang@linux.alibaba.com>
Cc: David Hildenbrand <david@redhat.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Zi Yan <ziy@nvidia.com>
