From: Tom Rix <trix@redhat.com>
Subject: watchdog: delete old declarations for watchdog_soft,hardlockup_user_enabled + make static
Date: Thu, 25 May 2023 16:28:32 -0700

smatch reports
kernel/watchdog.c:40:19: warning: symbol
  'watchdog_hardlockup_user_enabled' was not declared. Should it be static?
kernel/watchdog.c:41:19: warning: symbol
  'watchdog_softlockup_user_enabled' was not declared. Should it be static?

These variables are only used in their defining file, so they should
be static.

This problem showed up after the patch ("watchdog/hardlockup: rename some
"NMI watchdog" constants/function") because that rename missed the header
file.  That didn't cause any compile-time errors because, since commit
dd0693fdf054 ("watchdog: move watchdog sysctl interface to watchdog.c"),
nobody outside of "watchdog.c" was actually referring to them.  Thus, not
only should we make these variables static but we should remove the old
declarations in the header file that we missed renaming.

[dianders@chromium.org: updated subject + commit message; squashed in Petr's suggestion]
Link: https://lkml.kernel.org/r/20230525162822.1.I0fb41d138d158c9230573eaa37dc56afa2fb14ee@changeid
Fixes: 4b95b620dcd5 ("watchdog/hardlockup: rename some "NMI watchdog" constants/function")
Signed-off-by: Tom Rix <trix@redhat.com>
Signed-off-by: Douglas Anderson <dianders@chromium.org>
Reviewed-by: Tom Rix <trix@redhat.com>
Suggested-by: Petr Mladek <pmladek@suse.com>
Reviewed-by: Petr Mladek <pmladek@suse.com>
