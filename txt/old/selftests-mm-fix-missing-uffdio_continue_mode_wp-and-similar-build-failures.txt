From: John Hubbard <jhubbard@nvidia.com>
Subject: selftests/mm: fix missing UFFDIO_CONTINUE_MODE_WP and similar build failures
Date: Thu, 1 Jun 2023 18:33:57 -0700

UFFDIO_CONTINUE_MODE_WP, UFFD_FEATURE_WP_UNPOPULATED, USERFAULTFD_IOC, and
USERFAULTFD_IOC_NEW are needed lately, but they are not in my host (Arch
Linux) distro's userfaultfd.h yet.  So put them in here.

A better approach would be to include the uapi version of userfaultfd.h
from the kernel tree, but that currently fails with rather difficult
linker problems (__packed is defined multiple times, ugg), so defer that
to another day and just fix the build for now.

Link: https://lkml.kernel.org/r/20230602013358.900637-12-jhubbard@nvidia.com
Signed-off-by: John Hubbard <jhubbard@nvidia.com>
Acked-by: David Hildenbrand <david@redhat.com>
Cc: Mel Gorman <mgorman@techsingularity.net>
Cc: Mike Kravetz <mike.kravetz@oracle.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Peter Xu <peterx@redhat.com>
