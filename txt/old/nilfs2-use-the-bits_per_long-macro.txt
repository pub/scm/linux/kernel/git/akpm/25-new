From: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Subject: nilfs2: use the BITS_PER_LONG macro
Date: Tue, 27 Aug 2024 02:41:10 +0900

The macros NILFS_BMAP_KEY_BIT and NILFS_BMAP_NEW_PTR_INIT calculate,
within their definitions, the number of bits in an unsigned long variable.
Use the BITS_PER_LONG macro to make them simpler.

Link: https://lkml.kernel.org/r/20240826174116.5008-3-konishi.ryusuke@gmail.com
Signed-off-by: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Cc: Huang Xiaojia <huangxiaojia2@huawei.com>
