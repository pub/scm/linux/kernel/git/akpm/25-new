From: Andrew Morton <akpm@linux-foundation.org>
Subject: lib-add-version-into-proc-allocinfo-output-fix
Date: Tue May 14 01:19:12 PM PDT 2024

remove stray newline from struct allocinfo_private

Cc: Kees Cook <keescook@chromium.org>
Cc: Kent Overstreet <kent.overstreet@linux.dev>
Cc: Pasha Tatashin <pasha.tatashin@soleen.com>
Cc: Suren Baghdasaryan <surenb@google.com>
Cc: Vlastimil Babka <vbabka@suse.cz>
