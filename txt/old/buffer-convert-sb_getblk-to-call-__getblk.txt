From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: buffer: convert sb_getblk() to call __getblk()
Date: Thu, 14 Sep 2023 16:00:09 +0100

Now that __getblk() is in the right place in the file, it is trivial to
call it from sb_getblk().

Link: https://lkml.kernel.org/r/20230914150011.843330-7-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Hui Zhu <teawater@antgroup.com>
