From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: documentation: add recompression documentation
Date: Tue, 18 Oct 2022 13:55:29 +0900

Document user-space visible device attributes that are enabled by
ZRAM_MULTI_COMP.

Link: https://lkml.kernel.org/r/20221018045533.2396670-6-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Nitin Gupta <ngupta@vflare.org>
