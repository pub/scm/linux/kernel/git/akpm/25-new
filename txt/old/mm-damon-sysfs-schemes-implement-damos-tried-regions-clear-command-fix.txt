From: SeongJae Park <sj@kernel.org>
Subject: mm/damon/sysfs-schemes: skip schemes regions clearing if the scheme directory has removed
Date: Mon, 14 Nov 2022 18:29:54 +0000

A DAMON sysfs interface user can start DAMON with a scheme, remove the
sysfs directory for the scheme, and then ask clearing of the scheme's
tried regions.  Because the schemes tried regions clearing logic doesn't
aware of the situation, it results in an invalid memory access.  Fix the
bug by checking if the scheme sysfs directory exists.

Link: https://lkml.kernel.org/r/20221114182954.4745-3-sj@kernel.org
Fixes: ("mm/damon/sysfs-schemes: implement DAMOS-tried regions clear command")
Signed-off-by: SeongJae Park <sj@kernel.org>
