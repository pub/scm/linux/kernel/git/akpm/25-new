From: Matthew Wilcox <willy@infradead.org>
Subject: mm-convert-swap_cluster_readahead-and-swap_vma_readahead-to-return-a-folio-fix
Date: Wed, 20 Dec 2023 00:54:17 +0000

avoid NULL pointer deref

Link: https://lkml.kernel.org/r/ZYI7OcVlM1voKfBl@casper.infradead.org
Signed-off-by: Matthew Wilcox <willy@infradead.org>
Reported-by: Kairui Song <ryncsn@gmail.com>
