From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: convert split_huge_pages_pid() to use a folio
Date: Wed, 16 Aug 2023 16:12:01 +0100

Replaces five calls to compound_head with one.

Link: https://lkml.kernel.org/r/20230816151201.3655946-14-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: David Hildenbrand <david@redhat.com>
Cc: Jens Axboe <axboe@kernel.dk>
Cc: Sidhartha Kumar <sidhartha.kumar@oracle.com>
Cc: Yanteng Si <siyanteng@loongson.cn>
