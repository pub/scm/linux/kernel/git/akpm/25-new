From: Chunyan Zhang <zhangchunyan@iscas.ac.cn>
Subject: selftest/mm: fix typo in virtual_address_range
Date: Tue, 8 Oct 2024 17:41:40 +0800

The function name should be *hint* address, so correct it.

Link: https://lkml.kernel.org/r/20241008094141.549248-4-zhangchunyan@iscas.ac.cn
Signed-off-by: Chunyan Zhang <zhangchunyan@iscas.ac.cn>
Reviewed-by: Charlie Jenkins <charlie@rivosinc.com>
Acked-by: Palmer Dabbelt <palmer@rivosinc.com>
Cc: Alexandre Ghiti <alex@ghiti.fr>
Cc: Paul Walmsley <paul.walmsley@sifive.com>
Cc: Shuah Khan <shuah@kernel.org>
