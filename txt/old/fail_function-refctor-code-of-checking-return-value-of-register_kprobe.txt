From: Yang Yingliang <yangyingliang@huawei.com>
Subject: fail_function: refactor code of checking return value of register_kprobe()
Date: Fri, 26 Aug 2022 15:33:36 +0800

Refactor the error handling of register_kprobe() to improve readability. 
No functional change.

Link: https://lkml.kernel.org/r/20220826073337.2085798-2-yangyingliang@huawei.com
Signed-off-by: Yang Yingliang <yangyingliang@huawei.com>
Reviewed-by: Andrew Morton <akpm@linux-foundation.org>
