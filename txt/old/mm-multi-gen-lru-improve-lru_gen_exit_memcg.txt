From: "T.J. Alumbaugh" <talumbau@google.com>
Subject: mm: multi-gen LRU: improve lru_gen_exit_memcg()
Date: Wed, 18 Jan 2023 00:18:25 +0000

Add warnings and poison ->next.

Link: https://lkml.kernel.org/r/20230118001827.1040870-6-talumbau@google.com
Signed-off-by: T.J. Alumbaugh <talumbau@google.com>
Cc: Yu Zhao <yuzhao@google.com>
