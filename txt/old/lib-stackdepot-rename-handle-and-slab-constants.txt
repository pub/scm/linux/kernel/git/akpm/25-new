From: Andrey Konovalov <andreyknvl@google.com>
Subject: lib/stackdepot: rename handle and slab constants
Date: Mon, 30 Jan 2023 21:49:36 +0100

Change the "STACK_ALLOC_" prefix to "DEPOT_" for the constants that define
the number of bits in stack depot handles and the maximum number of slabs.

The old prefix is unclear and makes wonder about how these constants are
related to stack allocations.  The new prefix is also shorter.

Also simplify the comment for DEPOT_SLAB_ORDER.

No functional changes.

Link: https://lkml.kernel.org/r/d9c6d1fa0ae6e1e65577ee81444656c99eb598d8.1675111415.git.andreyknvl@google.com
Signed-off-by: Andrey Konovalov <andreyknvl@google.com>
Reviewed-by: Alexander Potapenko <glider@google.com>
Cc: Evgenii Stepanov <eugenis@google.com>
Cc: Marco Elver <elver@google.com>
Cc: Vlastimil Babka <vbabka@suse.cz>
