From: Konrad Dybcio <konrad.dybcio@linaro.org>
Subject: mailmap: map Sai Prakash Ranjan's old address to his current one
Date: Tue, 14 Mar 2023 13:56:03 +0100

Sai's old email is still picked up by the likes of get_maintainer.pl and
keeps bouncing like all other @codeaurora.org addresses.  Map it to his
current one.

Link: https://lkml.kernel.org/r/20230314125604.2734146-1-konrad.dybcio@linaro.org
Signed-off-by: Konrad Dybcio <konrad.dybcio@linaro.org>
Cc: Sai Prakash Ranjan <quic_saipraka@quicinc.com>
