From: Alexey Dobriyan <adobriyan@gmail.com>
Subject: ELF: document some de-facto PT_* ABI quirks
Date: Sun, 26 Mar 2023 19:49:49 +0300

Turns out rules about PT_INTERP, PT_GNU_STACK and PT_GNU_PROPERTY
program headers are slightly different.

Link: https://lkml.kernel.org/r/c4233c97-306c-4db8-9667-34fc31ec4aed@p183
Signed-off-by: Alexey Dobriyan <adobriyan@gmail.com>
Reviewed-by: Randy Dunlap <rdunlap@infradead.org>
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Christian Brauner <brauner@kernel.org>
Cc: Eric Biederman <ebiederm@xmission.com>
Cc: Kees Cook <keescook@chromium.org>
Cc: Bagas Sanjaya <bagasdotme@gmail.com>
Cc: Jonathan Corbet <corbet@lwn.net>
