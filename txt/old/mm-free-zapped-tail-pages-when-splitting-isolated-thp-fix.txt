From: Usama Arif <usamaarif642@gmail.com>
Subject: mm-free-zapped-tail-pages-when-splitting-isolated-thp-fix
Date: Thu, 15 Aug 2024 20:16:56 +0100

fix bug going from v1 page version to the folio version of the patch in v3

Signed-off-by: Usama Arif <usamaarif642@gmail.com>
Reported-by: Kairui Song <ryncsn@gmail.com>
Cc: Alexander Zhu <alexlzhu@fb.com>
Cc: Andi Kleen <ak@linux.intel.com>
Cc: Barry Song <baohua@kernel.org>
Cc: David Hildenbrand <david@redhat.com>
Cc: Domenico Cerasuolo <cerasuolodomenico@gmail.com>
Cc: Huan Yang <link@vivo.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Mike Rapoport <rppt@kernel.org>
Cc: Rik van Riel <riel@surriel.com>
Cc: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Ryan Roberts <ryan.roberts@arm.com>
Cc: Shakeel Butt <shakeel.butt@linux.dev>
Cc: Shuang Zhai <zhais@google.com>
Cc: Yu Zhao <yuzhao@google.com>
