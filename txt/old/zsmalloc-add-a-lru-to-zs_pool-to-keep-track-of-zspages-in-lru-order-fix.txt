From: Nhat Pham <nphamcs@gmail.com>
Subject: zsmalloc: add a LRU to zs_pool to keep track of zspages in LRU order (fix)
Date: Wed, 23 Nov 2022 11:17:02 -0800

Add a comment explaining the mapping check for LRU update.

Link: https://lkml.kernel.org/r/20221123191703.2902079-2-nphamcs@gmail.com
Signed-off-by: Nhat Pham <nphamcs@gmail.com>
Suggested-by: Johannes Weiner <hannes@cmpxchg.org>
Suggested-by: Sergey Senozhatsky <senozhatsky@chromium.org>
