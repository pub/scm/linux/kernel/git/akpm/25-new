From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: zram: remove two-staged handle allocation
Date: Fri, 14 Feb 2025 13:50:17 +0900

Previously zram write() was atomic which required us to pass
__GFP_KSWAPD_RECLAIM to zsmalloc handle allocation on a fast path and
attempt a slow path allocation (with recompression) when the fast path
failed.

Since it's not atomic anymore we can permit direct reclaim during
allocation, and remove fast allocation path and, also, drop the
recompression path (which should reduce CPU/battery usage).

Link: https://lkml.kernel.org/r/20250214045208.1388854-6-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Hillf Danton <hdanton@sina.com>
Cc: Kairui Song <ryncsn@gmail.com>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Sebastian Andrzej Siewior <bigeasy@linutronix.de>
Cc: Yosry Ahmed <yosry.ahmed@linux.dev>
