From: Shubhang Kaushik OS <Shubhang@os.amperecomputing.com>
Subject: vmalloc: modify the alloc_vmap_area() error message for better diagnostics
Date: Tue, 11 Jun 2024 19:38:44 +0000

replace the vmalloc range parameters in the overflow warning message to be
between vstart and vend

Link: https://lkml.kernel.org/r/CH2PR01MB5894B0182EA0B28DF2EFB916F5C72@CH2PR01MB5894.prod.exchangelabs.com
Signed-off-by: Shubhang Kaushik <shubhang@os.amperecomputing.com>
Reviewed-by: Christoph Lameter (Ampere) <cl@linux.com>
Cc: Guo Ren <guoren@kernel.org>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Uladzislau Rezki (Sony) <urezki@gmail.com>
Cc: Xiongwei Song <xiongwei.song@windriver.com>
