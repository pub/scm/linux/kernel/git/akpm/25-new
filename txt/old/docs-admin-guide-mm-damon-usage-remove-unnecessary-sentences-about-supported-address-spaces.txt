From: SeongJae Park <sj@kernel.org>
Subject: Docs/admin-guide/mm/damon/usage: remove unnecessary sentences about supported address spaces
Date: Fri, 16 Jun 2023 19:17:39 +0000

Brief explanation of DAMON user space tool and sysfs interface are
unnecessarily and repeatedly mentioning the list of address spaces that
DAMON is supporting.  Remove those.

Link: https://lkml.kernel.org/r/20230616191742.87531-5-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
Cc: Jonathan Corbet <corbet@lwn.net>
