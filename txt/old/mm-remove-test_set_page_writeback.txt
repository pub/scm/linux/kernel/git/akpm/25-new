From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: remove test_set_page_writeback()
Date: Wed, 8 Nov 2023 20:46:02 +0000

Patch series "Make folio_start_writeback return void".

Most of the folio flag-setting functions return void. 
folio_start_writeback is gratuitously different; the only two filesystems
that do anything with the return value emit debug messages if it's already
set, and we can (and should) do that internally without bothering the
filesystem to do it.


This patch (of 4):

There are no more callers of this wrapper.

Link: https://lkml.kernel.org/r/20231108204605.745109-1-willy@infradead.org
Link: https://lkml.kernel.org/r/20231108204605.745109-2-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: David Howells <dhowells@redhat.com>
Cc: Steve French <sfrench@samba.org>
