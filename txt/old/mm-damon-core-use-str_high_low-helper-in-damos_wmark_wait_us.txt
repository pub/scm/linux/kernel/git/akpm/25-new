From: Thorsten Blum <thorsten.blum@linux.dev>
Subject: mm/damon/core: use str_high_low() helper in damos_wmark_wait_us()
Date: Thu, 16 Jan 2025 21:42:16 +0100

Remove hard-coded strings by using the str_high_low() helper function.

Link: https://lkml.kernel.org/r/20250116204216.106999-2-thorsten.blum@linux.dev
Signed-off-by: Thorsten Blum <thorsten.blum@linux.dev>
Reviewed-by: SeongJae Park <sj@kernel.org>
