From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: memory-failure: convert delete_from_lru_cache() to take a folio
Date: Fri, 17 Nov 2023 16:14:44 +0000

All three callers now have a folio; pass it in instead of the page.
Saves five calls to compound_head().

Link: https://lkml.kernel.org/r/20231117161447.2461643-4-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Naoya Horiguchi <naoya.horiguchi@nec.com>
