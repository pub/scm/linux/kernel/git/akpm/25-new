From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: zram: factor out different page types read
Date: Wed, 18 Dec 2024 15:34:22 +0900

Similarly to write, split the page read code into ZRAM_HUGE read,
ZRAM_SAME read and compressed page read to simplify the code.

Link: https://lkml.kernel.org/r/20241218063513.297475-6-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Minchan Kim <minchan@kernel.org>
