From: Tiezhu Yang <yangtiezhu@loongson.cn>
Subject: checksyscalls: ignore fstat to silence build warning on LoongArch
Date: Tue, 7 Mar 2023 15:59:00 +0800

fstat is replaced by statx on the new architecture, so an exception is
added to the checksyscalls script to silence the following build warning
on LoongArch:

  CALL    scripts/checksyscalls.sh
<stdin>:569:2: warning: #warning syscall fstat not implemented [-Wcpp]

Link: https://lkml.kernel.org/r/1678175940-20872-1-git-send-email-yangtiezhu@loongson.cn
Signed-off-by: Tiezhu Yang <yangtiezhu@loongson.cn>
Suggested-by: WANG Xuerui <kernel@xen0n.name>
Suggested-by: Arnd Bergmann <arnd@arndb.de>
Reviewed-by: Arnd Bergmann <arnd@arndb.de>
Cc: Huacai Chen <chenhuacai@kernel.org>
Cc: Masahiro Yamada <masahiroy@kernel.org>
