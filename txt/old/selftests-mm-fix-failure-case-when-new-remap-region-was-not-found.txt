From: "Joel Fernandes (Google)" <joel@joelfernandes.org>
Subject: selftests: mm: fix failure case when new remap region was not found
Date: Sun, 3 Sep 2023 15:13:24 +0000

When a valid remap region could not be found, the source mapping is not
cleaned up.  Fix the goto statement such that the clean up happens.

Link: https://lkml.kernel.org/r/20230903151328.2981432-4-joel@joelfernandes.org
Signed-off-by: Joel Fernandes (Google) <joel@joelfernandes.org>
Reviewed-by: Lorenzo Stoakes <lstoakes@gmail.com>
Cc: Kalesh Singh <kaleshsingh@google.com>
Cc: "Kirill A. Shutemov" <kirill@shutemov.name>
Cc: Liam R. Howlett <Liam.Howlett@oracle.com>
Cc: Linus Torvalds <torvalds@linux-foundation.org>
Cc: Lokesh Gidra <lokeshgidra@google.com>
Cc: Michal Hocko <mhocko@suse.com>
Cc: Paul E. McKenney <paulmck@kernel.org>
Cc: Shuah Khan <shuah@kernel.org>
Cc: Suren Baghdasaryan <surenb@google.com>
Cc: Vlastimil Babka <vbabka@suse.cz>
