From: Andrew Morton <akpm@linux-foundation.org>
Subject: ocfs2-fix-slab-use-after-free-due-to-dangling-pointer-dqi_priv-checkpatch-fixes
Date: Wed Dec 18 07:52:38 PM PST 2024

WARNING: Possible repeated word: 'to'
#11: 
set as null leaving it to to be accessed.  Additionally, the read-only

WARNING: Please use correct Fixes: style 'Fixes: <12 chars of sha1> ("<title line>")' - ie: 'Fixes: fatal: not a ("nux-next'")'
#21: 
Fixes: 8f9e8f5fcc05 ("ocfs2: Fix Q_GETNEXTQUOTA for filesystem without quotas")

WARNING: Reported-by: should be immediately followed by Closes: with a URL to the report
#23: 
Reported-by: syzbot+d173bf8a5a7faeede34c@syzkaller.appspotmail.com
Tested-by: syzbot+d173bf8a5a7faeede34c@syzkaller.appspotmail.com

ERROR: space required before the open brace '{'
#47: FILE: fs/ocfs2/quota_global.c:896:
+	if (!sb_has_quota_active(sb, type)){

total: 1 errors, 3 warnings, 15 lines checked

NOTE: For some of the reported defects, checkpatch may be able to
      mechanically convert to the typical style using --fix or --fix-inplace.

./patches/ocfs2-fix-slab-use-after-free-due-to-dangling-pointer-dqi_priv.patch has style problems, please review.

NOTE: If any of the errors are false positives, please report
      them to the maintainer, see CHECKPATCH in MAINTAINERS.

Please run checkpatch prior to sending patches

Cc: Changwei Ge <gechangwei@live.cn>
Cc: Dennis Lam <dennis.lamerice@gmail.com>
Cc: Joel Becker <jlbec@evilplan.org>
Cc: Joseph Qi <joseph.qi@linux.alibaba.com>
Cc: Jun Piao <piaojun@huawei.com>
Cc: Junxiao Bi <junxiao.bi@oracle.com>
Cc: Mark Fasheh <mark@fasheh.com>
