From: Dan Carpenter <dan.carpenter@linaro.org>
Subject: selftests/mm: compaction_test: fix off by one in check_compaction()
Date: Fri, 9 Aug 2024 15:32:30 +0300

The "initial_nr_hugepages" variable is unsigned long so it takes up to 20
characters to print, plus 1 more character for the NUL terminator. 
Unfortunately, this buffer is not quite large enough for the terminator to
fit.  Also use snprintf() for a belt and suspenders approach.

Link: https://lkml.kernel.org/r/87470c06-b45a-4e83-92ff-aac2e7b9c6ba@stanley.mountain
Fixes: fb9293b6b015 ("selftests/mm: compaction_test: fix bogus test success and reduce probability of OOM-killer invocation")
Signed-off-by: Dan Carpenter <dan.carpenter@linaro.org>
Cc: Shuah Khan <shuah@kernel.org>
