From: Andrey Konovalov <andreyknvl@google.com>
Subject: lib/stackdepot: fix setting next_slab_inited in init_stack_slab
Date: Mon, 30 Jan 2023 21:49:25 +0100

Patch series "lib/stackdepot: fixes and clean-ups".

A set of fixes, comments, and clean-ups I came up with while reading
the stack depot code.


This patch (of 18):

In commit 305e519ce48e ("lib/stackdepot.c: fix global out-of-bounds in
stack_slabs"), init_stack_slab was changed to only use preallocated memory
for the next slab if the slab number limit is not reached.  However,
setting next_slab_inited was not moved together with updating stack_slabs.

Set next_slab_inited only if the preallocated memory was used for the next
slab.

Link: https://lkml.kernel.org/r/cover.1675111415.git.andreyknvl@google.com
Link: https://lkml.kernel.org/r/9fbb4d2bf9b2676a29b120980b5ffbda8e2304ee.1675111415.git.andreyknvl@google.com
Fixes: 305e519ce48e ("lib/stackdepot.c: fix global out-of-bounds in stack_slabs")
Signed-off-by: Andrey Konovalov <andreyknvl@google.com>
Reviewed-by: Alexander Potapenko <glider@google.com>
Cc: Evgenii Stepanov <eugenis@google.com>
Cc: Marco Elver <elver@google.com>
Cc: Vlastimil Babka <vbabka@suse.cz>
