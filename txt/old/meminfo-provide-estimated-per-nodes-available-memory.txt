From: Chunsheng Luo <luochunsheng@ustc.edu>
Subject: meminfo: provide estimated per-node's available memory
Date: Sun, 4 Feb 2024 03:34:14 -0500

The system offers an estimate of the per-node's available memory, in
addition to the system's available memory provided by /proc/meminfo.

Like commit 34e431b0ae39 ("/proc/meminfo: provide estimated available
memory"), it is more convenient to provide such an estimate in
/sys/bus/node/devices/nodex/meminfo.  If things change in the future, we
only have to change it in one place.

Shown below:
/sys/bus/node/devices/node1/meminfo:
Node 1 MemTotal:        4084480 kB
Node 1 MemFree:         3348820 kB
Node 1 MemAvailable:    3647972 kB
Node 1 MemUsed:          735660 kB
....

Link: https://github.com/numactl/numactl/issues/210
Link: https://lkml.kernel.org/r/20240204083414.107799-1-luochunsheng@ustc.edu
Signed-off-by: Chunsheng Luo <luochunsheng@ustc.edu>
Cc: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Cc: "Rafael J. Wysocki" <rafael@kernel.org>
