From: Mike Rapoport <rppt@kernel.org>
Subject: arch_numa-switch-over-to-numa_memblks-fix-2
Date: Tue, 27 Aug 2024 11:52:55 +0300

PFN_PHYS() translation is unnecessary here, as memblock_start_of_DRAM() +
SZ_4G is already a memory size.

Link: https://lkml.kernel.org/r/Zs2T5wkSYO9MGcab@kernel.org
Signed-off-by: Mike Rapoport <rppt@kernel.org>
Cc: Dan Williams <dan.j.williams@intel.com>
Cc: David Hildenbrand <david@redhat.com>
Cc: Jonathan Cameron <Jonathan.Cameron@huawei.com>
Cc: Zi Yan <ziy@nvidia.com>
Cc: Bruno Faccini <bfaccini@nvidia.com>
