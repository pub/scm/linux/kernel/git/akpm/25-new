From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: remove PageOwnerPriv1
Date: Wed, 21 Aug 2024 20:34:40 +0100

While there are many aliases for this flag, nobody actually uses the
*PageOwnerPriv1() nor folio_*_owner_priv_1() accessors.  Remove them.

Link: https://lkml.kernel.org/r/20240821193445.2294269-8-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
