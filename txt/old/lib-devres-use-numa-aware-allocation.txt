From: Mark-PK Tsai <mark-pk.tsai@mediatek.com>
Subject: lib: devres: use numa aware allocation
Date: Fri, 8 Jul 2022 21:19:47 +0800

Allocate device resource from local node memory when the numa locality of
the device is specified.

Link: https://lkml.kernel.org/r/20220708131952.14500-1-mark-pk.tsai@mediatek.com
Signed-off-by: Mark-PK Tsai <mark-pk.tsai@mediatek.com>
Cc: Matthias Brugger <matthias.bgg@gmail.com>
Cc: YJ Chiang <yj.chiang@mediatek.com>
Cc: Hans de Goede <hdegoede@redhat.com>
Cc: Thomas Zimmermann <tzimmermann@suse.de>
Cc: Zhen Lei <thunder.leizhen@huawei.com>
Cc: Jacob Keller <jacob.e.keller@intel.com>
