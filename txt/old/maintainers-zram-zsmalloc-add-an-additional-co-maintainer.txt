From: Minchan Kim <minchan@kernel.org>
Subject: MAINTAINERS: zram: zsmalloc: Add an additional co-maintainer
Date: Tue, 13 Dec 2022 09:07:31 -0800

Move Sergey to co-maintainer for zram/zsmalloc since he has helped to
contribute/review those areas actively for eight years, which is quite
helpful.  Since Nitin has been inactive for several years, it's time to
move his name into CREDITS.

Link: https://lkml.kernel.org/r/20221213170731.796121-1-minchan@kernel.org
Signed-off-by: Minchan Kim <minchan@kernel.org>
Reviewed-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Nitin Gupta <ngupta@vflare.org>
