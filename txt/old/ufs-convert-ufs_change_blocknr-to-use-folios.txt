From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: ufs: convert ufs_change_blocknr() to use folios
Date: mon, 16 Oct 2023 21:11:12 +0100

Convert the locked_page argument to a folio, then use folios throughout. 
Saves three hidden calls to compound_head().

Link: https://lkml.kernel.org/r/20231016201114.1928083-26-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Andreas Gruenbacher <agruenba@redhat.com>
Cc: Pankaj Raghav <p.raghav@samsung.com>
Cc: Ryusuke Konishi <konishi.ryusuke@gmail.com>
