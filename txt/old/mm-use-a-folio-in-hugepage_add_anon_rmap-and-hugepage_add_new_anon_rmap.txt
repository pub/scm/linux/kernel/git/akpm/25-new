From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: use a folio in hugepage_add_anon_rmap() and hugepage_add_new_anon_rmap()
Date: Wed, 11 Jan 2023 14:28:56 +0000

Remove uses of compound_mapcount_ptr()

Link: https://lkml.kernel.org/r/20230111142915.1001531-11-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
