From: Andrey Konovalov <andreyknvl@google.com>
Subject: kasan: fix for "kasan: rename and document kasan_(un)poison_object_data"
Date: Thu, 21 Dec 2023 19:06:37 +0100

Update references to renamed functions in comments.

Link: https://lkml.kernel.org/r/20231221180637.105098-1-andrey.konovalov@linux.dev
Fixes: ac6b240e1ede ("kasan: rename and document kasan_(un)poison_object_data")
Signed-off-by: Andrey Konovalov <andreyknvl@google.com>
Reviewed-by: Marco Elver <elver@google.com>
Cc: Alexander Potapenko <glider@google.com>
Cc: Andrey Ryabinin <ryabinin.a.a@gmail.com>
Cc: Dmitry Vyukov <dvyukov@google.com>
