From: David Hildenbrand <david@redhat.com>
Subject: mm: remove total_mapcount()
Date: Mon, 26 Feb 2024 15:13:24 +0100

All users of total_mapcount() are gone, let's remove it.

Link: https://lkml.kernel.org/r/20240226141324.278526-3-david@redhat.com
Signed-off-by: David Hildenbrand <david@redhat.com>
Reviewed-by: Matthew Wilcox (Oracle) <willy@infradead.org>
