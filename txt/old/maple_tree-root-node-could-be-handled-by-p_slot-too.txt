From: Wei Yang <richard.weiyang@gmail.com>
Subject: maple_tree: root node could be handled by !p_slot too
Date: Fri, 13 Sep 2024 06:31:28 +0000

For a root node, mte_parent_slot() return 0, this exactly fits the
following !p_slot check.

So we can remove the special handling for root node.

Link: https://lkml.kernel.org/r/20240913063128.27391-1-richard.weiyang@gmail.com
Signed-off-by: Wei Yang <richard.weiyang@gmail.com>
Reviewed-by: Liam R. Howlett <Liam.Howlett@Oracle.com>
