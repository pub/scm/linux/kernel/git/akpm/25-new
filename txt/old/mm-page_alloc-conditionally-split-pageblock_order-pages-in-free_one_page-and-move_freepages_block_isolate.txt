From: David Hildenbrand <david@redhat.com>
Subject: mm/page_alloc: conditionally split > pageblock_order pages in free_one_page() and move_freepages_block_isolate()
Date: Fri, 6 Dec 2024 10:59:50 +0100

Patch series "mm/page_alloc: rework conditional splitting >=
pageblock_order pages when freeing".

Looking into recent alloc_contig_range(__GFP_COMP) support, I realized
that we now unconditionally split up high-order pages on the page freeing
path to free in pageblock granularity, just to immediately let the buddy
merge them again in the common case.

Let's optimize for the common case (all pageblock migratetypes match), and
enable it only in configs where this is strictly required.  Further, add
some comments that explain why this special casing is required at all.

Alongside, a fix for a stale comment in page isolation code.


This patch (of 2):

Let's special-case for the common scenarios that:

(a) We are freeing pages <= pageblock_order
(b) We are freeing a page <= MAX_PAGE_ORDER and all pageblocks match
    (especially, no mixture of isolated and non-isolated pageblocks)

When we encounter a > MAX_PAGE_ORDER page, it can only come from
alloc_contig_range(), and we can process MAX_PAGE_ORDER chunks.

When we encounter a >pageblock_order <= MAX_PAGE_ORDER page, check whether
all pageblocks match, and if so (common case), don't split them up just
for the buddy to merge them back.

This makes sure that when we free MAX_PAGE_ORDER chunks to the buddy, for
example during system startups, memory onlining, or when isolating
consecutive pageblocks via alloc_contig_range()/memory offlining, that we
don't unnecessarily split up what we'll immediately merge again, because
the migratetypes match.

Rename split_large_buddy() to __free_one_page_maybe_split(), to make it
clearer what's happening, and handle in it only natural buddy orders, not
the alloc_contig_range(__GFP_COMP) special case: handle that in
free_one_page() only.

Link: https://lkml.kernel.org/r/20241206095951.98007-1-david@redhat.com
Link: https://lkml.kernel.org/r/20241206095951.98007-2-david@redhat.com
Signed-off-by: David Hildenbrand <david@redhat.com>
Reviewed-by: Zi Yan <ziy@nvidia.com>
Acked-by: Yu Zhao <yuzhao@google.com>
Acked-by: Vlastimil Babka <vbabka@suse.cz
