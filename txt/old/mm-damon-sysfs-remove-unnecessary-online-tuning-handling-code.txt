From: SeongJae Park <sj@kernel.org>
Subject: mm/damon/sysfs: remove unnecessary online tuning handling code
Date: Tue, 18 Jun 2024 11:18:02 -0700

damon/sysfs.c contains code for handling of online DAMON parameters update
edge cases.  It is no more necessary since damon_commit_ctx() takes care
of the cases.  Remove the unnecessary code.

Link: https://lkml.kernel.org/r/20240618181809.82078-6-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
