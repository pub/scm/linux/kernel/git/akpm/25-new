From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-delete-unused-mmf_oom_victim-flag-fix
Date: Sun Sep 18 05:03:09 PM PDT 2022

remove comment about now-removed mm_is_oom_victim()

Cc: Suren Baghdasaryan <surenb@google.com>
Cc: Yu Zhao <yuzhao@google.com>
