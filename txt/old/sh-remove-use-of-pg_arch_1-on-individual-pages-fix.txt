From: Andrew Morton <akpm@linux-foundation.org>
Subject: sh-remove-use-of-pg_arch_1-on-individual-pages-fix
Date: Thu Mar 28 11:44:56 AM PDT 2024

fix folio_flags call

Cc: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Reported-by: Naresh Kamboju <naresh.kamboju@linaro.org>
