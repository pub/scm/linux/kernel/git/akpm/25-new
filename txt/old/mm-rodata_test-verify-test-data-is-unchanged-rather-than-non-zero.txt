From: Petr Tesarik <ptesarik@suse.com>
Subject: mm/rodata_test: verify test data is unchanged, rather than non-zero
Date: Tue, 19 Nov 2024 12:37:39 +0100

Verify that the test variable holds the initialization value, rather than
any non-zero value.

Link: https://lkml.kernel.org/r/386ffda192eb4a26f68c526c496afd48a5cd87ce.1732016064.git.ptesarik@suse.com
Signed-off-by: Petr Tesarik <ptesarik@suse.com>
Reviewed-by: Kees Cook <kees@kernel.org>
Cc: Jinbum Park <jinb.park7@gmail.com>
