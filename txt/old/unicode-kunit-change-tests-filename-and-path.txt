From: Gabriela Bittencourt <gbittencourt@lkcamp.dev>
Subject: unicode: kunit: change tests filename and path
Date: Fri, 11 Oct 2024 15:25:10 +0800

Change utf8 kunit test filename and path to follow the style convention on
Documentation/dev-tools/kunit/style.rst

[davidgow@google.com: rebased, fixed module build (Gabriel Krisman Bertazi)]
Link: https://lkml.kernel.org/r/20241011072509.3068328-8-davidgow@google.com
Co-developed-by: Pedro Orlando <porlando@lkcamp.dev>
Signed-off-by: Pedro Orlando <porlando@lkcamp.dev>
Co-developed-by: Danilo Pereira <dpereira@lkcamp.dev>
Signed-off-by: Danilo Pereira <dpereira@lkcamp.dev>
Signed-off-by: Gabriela Bittencourt <gbittencourt@lkcamp.dev>
Signed-off-by: David Gow <davidgow@google.com>
Reviewed-by: David Gow <davidgow@google.com>
Acked-by: Shuah Khan <skhan@linuxfoundation.org>
Cc: Andy Shevchenko <andy@kernel.org>
Cc: Anil S Keshavamurthy <anil.s.keshavamurthy@intel.com>
Cc: Arnd Bergmann <arnd@arndb.de>
Cc: Brendan Higgins <brendanhiggins@google.com>
Cc: Bruno Sobreira Fran=C3=A7a <brunofrancadevsec@gmail.com>
Cc: Charlie Jenkins <charlie@rivosinc.com>
Cc: Christophe Leroy <christophe.leroy@csgroup.eu>
Cc: Daniel Latypov <dlatypov@google.com>
Cc: David Howells <dhowells@redhat.com>
Cc: David S. Miller <davem@davemloft.net>
Cc: Diego Vieira <diego.daniel.professional@gmail.com>
Cc: Fangrui Song <maskray@google.com>
Cc: Geert Uytterhoeven <geert@linux-m68k.org>
Cc: Guenter Roeck <linux@roeck-us.net>
Cc: Gustavo A. R. Silva <gustavoars@kernel.org>
Cc: Jakub Kicinski <kuba@kernel.org>
Cc: Jason A. Donenfeld <Jason@zx2c4.com>
Cc: Kees Cook <kees@kernel.org>
Cc: Luis Felipe Hernandez <luis.hernandez093@gmail.com>
Cc: Marco Elver <elver@google.com>
Cc: Mark Brown <broonie@kernel.org>
Cc: Mark Rutland <mark.rutland@arm.com>
Cc: "Masami Hiramatsu (Google)" <mhiramat@kernel.org>
Cc: Mickaël Salaün <mic@digikod.net>
Cc: Nathan Chancellor <nathan@kernel.org>
Cc: Naveen N. Rao <naveen.n.rao@linux.ibm.com>
Cc: Nicolas Pitre <npitre@baylibre.com>
Cc: Palmer Dabbelt <palmer@rivosinc.com>
Cc: Rae Moar <rmoar@google.com>
Cc: Rasmus Villemoes <linux@rasmusvillemoes.dk>
Cc: Simon Horman <horms@kernel.org>
Cc: Stephen Rothwell <sfr@canb.auug.org.au>
Cc: "Steven Rostedt (Google)" <rostedt@goodmis.org>
Cc: Vlastimil Babka <vbabka@suse.cz>
Cc: Yury Norov <yury.norov@gmail.com>
