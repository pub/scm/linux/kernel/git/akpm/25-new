From: Bagas Sanjaya <bagasdotme@gmail.com>
Subject: MAINTAINERS: remove Ohad Ben-Cohen from hwspinlock subsystem
Date: Mon, 18 Dec 2023 20:28:31 +0700

Commit 62c46d55688894 ("MAINTAINERS: Removing Ohad from remoteproc/rpmsg
maintenance") removes his MAINTAINERS entry in regards to remoteproc
subsystem due to his inactivity (the last commit with his Signed-off-by is
99c429cb4e628e ("remoteproc/wkup_m3: Use MODULE_DEVICE_TABLE to export
alias") which is authored in 2015 and his last LKML message prior to
62c46d55688894 was [1]).

Remove also his MAINTAINERS entry for hwspinlock subsystem as there is no
point of Cc'ing maintainers who never respond in a long time.

[1]: https://lore.kernel.org/r/CAK=Wgbbcyi36ef1-PV8VS=M6nFoQnFGUDWy6V7OCnkt0dDrtfg@mail.gmail.com/

Link: https://lkml.kernel.org/r/20231218132830.5104-2-bagasdotme@gmail.com
Signed-off-by: Bagas Sanjaya <bagasdotme@gmail.com>
Acked-by: Ohad Ben Cohen <ohad@wizery.com>
Cc: Baolin Wang <baolin.wang@linux.alibaba.com>
Cc: Bjorn Andersson <andersson@kernel.org>
Cc: Mathieu Poirier <mathieu.poirier@linaro.org>
