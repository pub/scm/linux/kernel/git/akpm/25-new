From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: rmap: pass the folio to __page_check_anon_rmap()
Date: Thu, 6 Jul 2023 20:52:51 +0100

The lone caller already has the folio, so pass it in instead of deriving
it from the page again.

Link: https://lkml.kernel.org/r/20230706195251.2707542-1-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
