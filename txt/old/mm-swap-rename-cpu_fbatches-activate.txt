From: Yu Zhao <yuzhao@google.com>
Subject: mm/swap: rename cpu_fbatches->activate
Date: Wed, 10 Jul 2024 20:13:14 -0600

Rename cpu_fbatches->activate to cpu_fbatches->lru_activate, and its
handler folio_activate_fn() to lru_activate() so that all the boilerplate
can be removed at the end of this series.

Link: https://lkml.kernel.org/r/20240711021317.596178-3-yuzhao@google.com
Signed-off-by: Yu Zhao <yuzhao@google.com>
