From: Chen Haonan <chen.haonan2@zte.com.cn>
Subject: mm: use vma_pages() for vma objects
Date: Wed, 6 Dec 2023 18:36:27 +0800

vma_pages() is more readable and also better at avoiding error codes, so
use vma_pages() instead of direct operations on vma

Link: https://lkml.kernel.org/r/tencent_151850CF327EB055BBC83298A929BD06CD0A@qq.com
Signed-off-by: Chen Haonan <chen.haonan2@zte.com.cn>
