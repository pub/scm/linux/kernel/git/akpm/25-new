From: SeongJae Park <sj@kernel.org>
Subject: mm/damon: move comments and fields for damos-quota-prioritization to the end
Date: Mon, 19 Feb 2024 11:44:17 -0800

The comments and definition of 'struct damos_quota' lists a few fields for
effective quota generation first, fields for regions prioritization under
the quota, and then remaining fields for effective quota generation. 
Readers' should unnecesssarily switch their context in the middle.  List
all the fields for the effective quota first, and then fields for the
prioritization for making it easier to read.

Link: https://lkml.kernel.org/r/20240219194431.159606-7-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
