From: "Liam R. Howlett" <Liam.Howlett@oracle.com>
Subject: mm/ksm: fix race with ksm_exit() in VMA iteration
Date: Tue, 7 Mar 2023 15:59:51 -0500

ksm_exit() may remove the mm from the ksm_scan between the unlocking of
the ksm_mmlist and the start of the VMA iteration.  This results in the
mmap_read_lock() not being taken and a report from lockdep that the mm
isn't locked in the maple tree code.

Fix the race by checking if this mm has been removed before iterating the
VMAs.  __ksm_exit() uses the mmap lock to synchronize the freeing of an
mm, so it is safe to keep iterating over the VMAs when it is going to be
freed.

This change will slow down the mm exit during the race condition, but will
speed up the non-race scenarios iteration over the VMA list, which should
be much more common.

Link: https://lkml.kernel.org/r/20230307205951.2465275-1-Liam.Howlett@oracle.com
Fixes: a5f18ba07276 ("mm/ksm: use vma iterators instead of vma linked list")
Signed-off-by: Liam R. Howlett <Liam.Howlett@oracle.com>
Reported-by: Pengfei Xu <pengfei.xu@intel.com>
  Link: https://lore.kernel.org/lkml/ZAdUUhSbaa6fHS36@xpf.sh.intel.com/
Reported-by: <syzbot+2ee18845e89ae76342c5@syzkaller.appspotmail.com>
  Link: https://syzkaller.appspot.com/bug?id=64a3e95957cd3deab99df7cd7b5a9475af92c93e
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: <heng.su@intel.com>
Cc: Pengfei Xu <pengfei.xu@intel.com>
Cc: <stable@vger.kernel.org>
