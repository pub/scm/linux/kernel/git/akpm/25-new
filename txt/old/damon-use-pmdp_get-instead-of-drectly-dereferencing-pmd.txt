From: Levi Yun <ppbuk5246@gmail.com>
Subject: damon: use pmdp_get instead of drectly dereferencing pmd
Date: Fri, 28 Jul 2023 06:21:57 +0900

As ptep_get, Use the pmdp_get wrapper when we accessing pmdval instead of
directly dereferencing pmd.

Link: https://lkml.kernel.org/r/20230727212157.2985025-1-ppbuk5246@gmail.com
Signed-off-by: Levi Yun <ppbuk5246@gmail.com>
Reviewed-by: SeongJae Park <sj@kernel.org>
