From: "Vishal Moola (Oracle)" <vishal.moola@gmail.com>
Subject: ext4: convert mpage_prepare_extent_to_map() to use filemap_get_folios_tag()
Date: Wed, 4 Jan 2023 13:14:35 -0800

Convert the function to use folios throughout.  This is in preparation for
the removal of find_get_pages_range_tag().  Now supports large folios. 
This change removes 11 calls to compound_head().

Link: https://lkml.kernel.org/r/20230104211448.4804-11-vishal.moola@gmail.com
Signed-off-by: Vishal Moola (Oracle) <vishal.moola@gmail.com>
