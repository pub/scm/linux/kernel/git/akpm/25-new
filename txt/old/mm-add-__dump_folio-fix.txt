From: Matthew Wilcox <willy@infradead.org>
Subject: mm-add-__dump_folio-fix
Date: Thu, 29 Feb 2024 04:37:31 +0000

fix build issue

Link: https://lkml.kernel.org/r/ZeAKCyTn_xS3O9cE@casper.infradead.org
Signed-off-by: Matthew Wilcox <willy@infradead.org>
Reported-by: SeongJae Park <sj@kernel.org>
