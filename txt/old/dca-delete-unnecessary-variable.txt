From: Dan Carpenter <error27@gmail.com>
Subject: dca: delete unnecessary variable
Date: Mon, 27 Feb 2023 13:06:12 +0300

It's more readable to just pass NULL directly instead of using a variable
for that.

Link: https://lkml.kernel.org/r/Y/yAlDytLH0ZNLNz@kili
Signed-off-by: Dan Carpenter <error27@gmail.com>
Cc: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
