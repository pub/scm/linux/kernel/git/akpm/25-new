From: SeongJae Park <sj@kernel.org>
Subject: include/linux/mm: declare different type of split_vma() for !CONFIG_MMU
Date: Fri, 6 Jan 2023 17:18:57 +0000

Commit 95000286ff79 ("mm: switch vma_merge(), split_vma(), and __split_vma
to vma iterator") on mm-unstable didn't make the change to nommu.c, but
the declaration change was applied to !CONFIG_MMU, too.  It causes below
build failure.

    linux/mm/nommu.c:1347:5: error: conflicting types for 'split_vma'
     1347 | int split_vma(struct mm_struct *mm, struct vm_area_struct *vma,
          |     ^~~~~~~~~
    In file included from linux/mm/nommu.c:20:
    linux/include/linux/mm.h:2846:12: note: previous declaration of 'split_vma' was here
     2846 | extern int split_vma(struct vma_iterator *vmi, struct vm_area_struct *,
          |            ^~~~~~~~~

Fix the build failure by adding the split_vma() declaration for
!CONFIG_MMU case.

Link: https://lkml.kernel.org/r/20230106171857.149918-1-sj@kernel.org
Fixes: 95000286ff79 ("mm: switch vma_merge(), split_vma(), and __split_vma to vma iterator")
Signed-off-by: SeongJae Park <sj@kernel.org>
Cc: Liam Howlett <liam.howlett@oracle.com>
