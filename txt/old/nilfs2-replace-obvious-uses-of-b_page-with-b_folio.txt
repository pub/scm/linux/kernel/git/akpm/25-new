From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: nilfs2: replace obvious uses of b_page with b_folio
Date: Thu, 15 Dec 2022 21:44:00 +0000

These places just use b_page to get to the buffer's address_space or the
index of the page the buffer is in.

Link: https://lkml.kernel.org/r/20221215214402.3522366-11-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: Jan Kara <jack@suse.cz>
