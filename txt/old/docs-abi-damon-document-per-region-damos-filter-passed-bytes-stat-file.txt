From: SeongJae Park <sj@kernel.org>
Subject: Docs/ABI/damon: document per-region DAMOS filter-passed bytes stat file
Date: Mon, 6 Jan 2025 11:34:01 -0800

Document the new ABI for per-region operations set layer-handled DAMOS
filters passed bytes statistic.

Link: https://lkml.kernel.org/r/20250106193401.109161-17-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
Cc: Jonathan Corbet <corbet@lwn.net>
