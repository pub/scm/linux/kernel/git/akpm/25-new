From: Kairui Song <kasong@tencent.com>
Subject: lib/xarray: introduce a new helper xas_get_order
Date: Tue, 16 Apr 2024 15:17:21 +0800

simplify comment, sparse warning fix, per Matthew Wilcox

Link: https://lkml.kernel.org/r/20240416071722.45997-4-ryncsn@gmail.com
Signed-off-by: Kairui Song <kasong@tencent.com>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
