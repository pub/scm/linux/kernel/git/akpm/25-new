From: Peng Zhang <zhangpeng.00@bytedance.com>
Subject: maple_tree: add a test case to check maple_alloc
Date: Tue, 11 Apr 2023 12:10:05 +0800

Add a test case to check whether the number of maple_alloc structures is
actually equal to mas->alloc->total.

Link: https://lkml.kernel.org/r/20230411041005.26205-2-zhangpeng.00@bytedance.com
Signed-off-by: Peng Zhang <zhangpeng.00@bytedance.com>
Cc: Liam R. Howlett <Liam.Howlett@Oracle.com>
