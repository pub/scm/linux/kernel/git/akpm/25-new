From: Heiko Carstens <hca@linux.ibm.com>
Subject: checkstack: sort output by size and function name
Date: Mon, 20 Nov 2023 19:37:18 +0100

Sort output by size and in addition by function name.  This increases
readability for cases where there are many functions with the same stack
usage.

Link: https://lkml.kernel.org/r/20231120183719.2188479-3-hca@linux.ibm.com
Signed-off-by: Heiko Carstens <hca@linux.ibm.com>
Cc: Maninder Singh <maninder1.s@samsung.com>
Cc: Masahiro Yamada <masahiroy@kernel.org>
Cc: Vaneet Narang <v.narang@samsung.com>
