From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: zram: remove crypto include
Date: Mon, 27 Jan 2025 16:29:14 +0900

Remove a leftover crypto header include.

Link: https://lkml.kernel.org/r/20250127072932.1289973-4-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Minchan Kim <minchan@kernel.org>
