From: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Subject: nilfs2: remove sc_timer_task
Date: Tue, 27 Aug 2024 02:41:14 +0900

After commit f5d4e04634c9 ("nilfs2: fix use-after-free of timer for log
writer thread") is applied, nilfs_construct_timeout(), which is called by
a timer and wakes up the log writer thread, is never called after the log
writer thread has terminated.

As a result, the member variable "sc_timer_task" of the "nilfs_sc_info"
structure, which was added when timer_setup() was adopted to retain a
reference to the log writer thread's task even after it had terminated, is
no longer needed, as it should be; we can simply use "sc_task" instead,
which holds a reference to the log writer thread's task for its lifetime. 
So, eliminate "sc_timer_task" by this means.

Link: https://lkml.kernel.org/r/20240826174116.5008-7-konishi.ryusuke@gmail.com
Signed-off-by: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Cc: Huang Xiaojia <huangxiaojia2@huawei.com>
