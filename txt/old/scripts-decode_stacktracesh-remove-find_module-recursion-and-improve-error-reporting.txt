From: Luca Ceresoli <luca.ceresoli@bootlin.com>
Subject: scripts/decode_stacktrace.sh: remove find_module recursion and improve error reporting
Date: Fri, 23 Aug 2024 10:27:42 +0200

Patch series "scripts/decode_stacktrace.sh: improve error reporting and
usability", v2.

This small series improves usability of scripts/decode_stacktrace.sh by
improving the usage text and correctly reporting when modules are built
without debugging symbols.


This patch (of 3):

The find_module() function can fail for two reasons:

 * the module was not found
 * the module was found but without debugging info

In both cases the user is reported the same error:

   WARNING! Modules path isn't set, but is needed to parse this symbol

This is misleading in case the modules path is set correctly.

find_module() is currently implemented as a recursive function based on
global variables in order to check up to 4 different paths.  This is not
straightforward to read and even less to modify.

Besides, the debuginfo code at the beginning of find_module() is executed
identically every time the function is entered, i.e.  up to 4 times per
each module search due to recursion.

To be able to improve error reporting, first rewrite the find_module()
function to remove recursion.  The new version of the function iterates
over all the same (up to 4) paths as before and for each of them does the
same checks as before.  At the end of the iteration it is now able to
print an appropriate error message, so that has been moved from the caller
into find_module().

Finally, when the module is found but without debugging info, mention the
two Kconfig variables one needs to set in order to have the needed
debugging symbols.

Link: https://lkml.kernel.org/r/20240823-decode_stacktrace-find_module-improvements-v2-0-d7a57d35558b@bootlin.com
Link: https://lkml.kernel.org/r/20240823-decode_stacktrace-find_module-improvements-v2-1-d7a57d35558b@bootlin.com
Signed-off-by: Luca Ceresoli <luca.ceresoli@bootlin.com>
Reviewed-by: Stephen Boyd <swboyd@chromium.org>
Cc: Alexis Lothoré (eBPF Foundation) <alexis.lothore@bootlin.com>
Cc: Konstantin Khlebnikov <koct9i@gmail.com>
Cc: Luca Ceresoli <luca.ceresoli@bootlin.com>
Cc: Sasha Levin <sashal@kernel.org>
Cc: Thomas Petazzoni <thomas.petazzoni@bootlin.com>
