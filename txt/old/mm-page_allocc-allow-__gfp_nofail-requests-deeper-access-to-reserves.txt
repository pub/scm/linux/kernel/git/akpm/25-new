From: Mel Gorman <mgorman@techsingularity.net>
Subject: mm/page_alloc.c: allow __GFP_NOFAIL requests deeper access to reserves
Date: Mon, 9 Jan 2023 15:16:29 +0000

Currently __GFP_NOFAIL allocations without any other flags can access 25%
of the reserves but these requests imply that the system cannot make
forward progress until the allocation succeeds.  Allow __GFP_NOFAIL access
to 75% of the min reserve.

Link: https://lkml.kernel.org/r/20230109151631.24923-6-mgorman@techsingularity.net
Signed-off-by: Mel Gorman <mgorman@techsingularity.net>
Acked-by: Vlastimil Babka <vbabka@suse.cz>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Michal Hocko <mhocko@suse.com>
Cc: NeilBrown <neilb@suse.de>
Cc: Thierry Reding <thierry.reding@gmail.com>
