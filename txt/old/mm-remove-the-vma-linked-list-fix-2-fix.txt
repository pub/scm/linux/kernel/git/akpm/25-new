From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-remove-the-vma-linked-list-fix-2-fix
Date: Wed Jun 29 04:45:49 PM PDT 2022

alter description, per Liam

Cc: "Liam R. Howlett" <Liam.Howlett@Oracle.com>
Cc: Mauro Carvalho Chehab <mchehab@kernel.org>
