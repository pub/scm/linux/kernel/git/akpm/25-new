From: SeongJae Park <sj@kernel.org>
Subject: mm/damon/core: replace per-quota regions priority histogram buffer usage with per-context one
Date: Sun, 25 Aug 2024 21:23:21 -0700

Replace the usage of per-quota region priorities histogram buffer with the
per-context one.  After this change, the per-quota histogram is not used
by anyone, and hence it is ready to be removed.

Link: https://lkml.kernel.org/r/20240826042323.87025-3-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
