From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: shmem: convert shmem_writepage() to use a folio throughout
Date: Fri, 2 Sep 2022 20:46:02 +0100

Even though we will split any large folio that comes in, write the code to
handle large folios so as to not leave a trap for whoever tries to handle
large folios in the swap cache.

Link: https://lkml.kernel.org/r/20220902194653.1739778-7-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
