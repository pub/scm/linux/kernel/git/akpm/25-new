From: Liu Shixin <liushixin2@huawei.com>
Subject: mm/filemap: don't decrease mmap_miss when folio has workingset flag
Date: Tue, 26 Mar 2024 14:50:26 +0800

add comments

Link: https://lkml.kernel.org/r/20240326065026.1910584-1-liushixin2@huawei.com
Signed-off-by: Liu Shixin <liushixin2@huawei.com>
Reviewed-by: Jan Kara <jack@suse.cz>
Cc: Al Viro <viro@ZenIV.linux.org.uk>
Cc: Christian Brauner <brauner@kernel.org>
Cc: Jinjiang Tu <tujinjiang@huawei.com>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
