From: Usama Arif <usamaarif642@gmail.com>
Subject: mm: set p->zeromap to NULL after freeing it
Date: Wed Jul 10 18:37:57 2024 +0100

alloc_swap_info can reuse swap_info_struct from previously used swap. 
Reset p->zeromap to NULL so that its not set to a corrupted pointer from
previous swap.

Signed-off-by: Usama Arif <usamaarif642@gmail.com>
Reported-by: kernel test robot <oliver.sang@intel.com>
Closes: https://lore.kernel.org/oe-lkp/202407101031.c6c3c651-lkp@intel.com
Cc: Hugh Dickins <hughd@google.com>
Cc: Andi Kleen <ak@linux.intel.com>
Cc: Chengming Zhou <chengming.zhou@linux.dev>
Cc: David Hildenbrand <david@redhat.com>
Cc: Huang Ying <ying.huang@intel.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Nhat Pham <nphamcs@gmail.com>
Cc: Shakeel Butt <shakeel.butt@linux.dev>
Cc: Yosry Ahmed <yosryahmed@google.com>
