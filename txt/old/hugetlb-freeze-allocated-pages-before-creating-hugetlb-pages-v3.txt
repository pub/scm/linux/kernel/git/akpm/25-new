From: Mike Kravetz <mike.kravetz@oracle.com>
Subject: hugetlb-freeze-allocated-pages-before-creating-hugetlb-pages-v3
Date: Wed, 21 Sep 2022 13:27:02 -0700

fix NULL pointer dereference in alloc_buddy_huge_page caused by not
checking for page before attempting to freeze.  Thanks Naoya.

Link: https://lkml.kernel.org/r/20220921202702.106069-1-mike.kravetz@oracle.com
Signed-off-by: Mike Kravetz <mike.kravetz@oracle.com>
Reviewed-by: Oscar Salvador <osalvador@suse.de>
Reviewed-by: Muchun Song <songmuchun@bytedance.com>
Reviewed-by: Miaohe Lin <linmiaohe@huawei.com>
Cc: Joao Martins <joao.m.martins@oracle.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Michal Hocko <mhocko@suse.com>
Cc: Peter Xu <peterx@redhat.com>
