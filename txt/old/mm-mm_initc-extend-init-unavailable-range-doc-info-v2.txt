From: Serge Semin <fancer.lancer@gmail.com>
Subject: mm-mm_initc-extend-init-unavailable-range-doc-info-v2
Date: Sat, 2 Dec 2023 14:18:52 +0300

update per Mike

Link: https://lkml.kernel.org/r/20231202111855.18392-1-fancer.lancer@gmail.com
Signed-off-by: Serge Semin <fancer.lancer@gmail.com>
Reviewed-by: Mike Rapoport (IBM) <rppt@kernel.org>
