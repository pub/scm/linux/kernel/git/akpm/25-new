From: SeongJae Park <sj@kernel.org>
Subject: Docs/admin-guide/mm/damon/usage: move debugfs intro to the bottom of the section
Date: Thu, 7 Sep 2023 02:29:21 +0000

On the DAMON usage introduction section, the introduction of DAMON
debugfs interface, which is deprecated, is above kernel API, which is
actively supported.  Move the DAMON debugfs intro to bottom, so that
readers have less chances to read it.

Link: https://lkml.kernel.org/r/20230907022929.91361-4-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Steven Rostedt (Google) <rostedt@goodmis.org>
