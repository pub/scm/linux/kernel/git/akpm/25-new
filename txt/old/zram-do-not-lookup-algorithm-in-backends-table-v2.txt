From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: zram: do not lookup algorithm in backends table
Date: Fri, 24 Jun 2022 15:06:06 +0900

add comment

Link: https://lkml.kernel.org/r/20220624060606.1014474-1-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Nitin Gupta <ngupta@vflare.org>
