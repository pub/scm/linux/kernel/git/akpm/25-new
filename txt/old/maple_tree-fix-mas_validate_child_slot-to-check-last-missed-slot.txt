From: Peng Zhang <zhangpeng.00@bytedance.com>
Subject: maple_tree: fix mas_validate_child_slot() to check last missed slot
Date: Tue, 11 Jul 2023 11:54:40 +0800

Don't break the loop before checking the last slot.  Also here check if
non-leaf nodes are missing children.

Link: https://lkml.kernel.org/r/20230711035444.526-5-zhangpeng.00@bytedance.com
Signed-off-by: Peng Zhang <zhangpeng.00@bytedance.com>
Reviewed-by: Liam R. Howlett <Liam.Howlett@oracle.com>
Tested-by: Geert Uytterhoeven <geert@linux-m68k.org>
