From: Tanzir Hasan <tanzirh@google.com>
Subject: lib/trace_readwrite.c:: replace asm-generic/io with linux/io
Date: Thu, 21 Dec 2023 20:32:33 +0000

asm-generic/io.h can be replaced with linux/io.h and the file will still
build correctly.  It is an asm-generic file which should be avoided if
possible.

Link: https://lkml.kernel.org/r/20231221-tracereadwrite-v1-1-a434f25180c7@google.com
Signed-off-by: Tanzir Hasan <tanzirh@google.com>
Suggested-by: Al Viro <viro@zeniv.linux.org.uk>
