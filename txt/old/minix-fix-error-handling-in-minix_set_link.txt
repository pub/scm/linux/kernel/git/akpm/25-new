From: Christoph Hellwig <hch@lst.de>
Subject: minix: fix error handling in minix_set_link
Date: Wed, 18 Jan 2023 18:30:23 +0100

If minix_prepare_chunk fails, updating c/mtime and marking the dir inode
dirty is wrong, as the inode hasn't been modified.  Also propagate the
error to the caller.

Note that this moves the dir_put_page call later, but that matches other
uses of this helper in the directory code.

Link: https://lkml.kernel.org/r/20230118173027.294869-4-hch@lst.de
Signed-off-by: Christoph Hellwig <hch@lst.de>
Cc: Evgeniy Dushistov <dushistov@mail.ru>
Cc: Jan Kara <jack@suse.cz>
Cc: Joel Becker <jlbec@evilplan.org>
Cc: Joseph Qi <joseph.qi@linux.alibaba.com>
Cc: Mark Fasheh <mark@fasheh.com>
Cc: Matthew Wilcox <willy@infradead.org>
