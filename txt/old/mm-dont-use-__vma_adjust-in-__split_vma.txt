From: "Liam R. Howlett" <Liam.Howlett@Oracle.com>
Subject: mm: don't use __vma_adjust() in __split_vma()
Date: Fri, 20 Jan 2023 11:26:44 -0500

Use the abstracted locking and maple tree operations.  Since __split_vma()
is the only user of the __vma_adjust() function to use the insert
argument, drop that argument.  Remove the NULL passed through from
fs/exec's shift_arg_pages() and mremap() at the same time.

Link: https://lkml.kernel.org/r/20230120162650.984577-44-Liam.Howlett@oracle.com
Signed-off-by: Liam R. Howlett <Liam.Howlett@oracle.com>
