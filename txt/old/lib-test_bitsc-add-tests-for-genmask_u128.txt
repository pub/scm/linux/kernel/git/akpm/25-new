From: Anshuman Khandual <anshuman.khandual@arm.com>
Subject: lib/test_bits.c: add tests for GENMASK_U128()
Date: Thu, 25 Jul 2024 11:18:08 +0530

This adds GENMASK_U128() tests although currently only 64 bit wide masks
are being tested.

Link: https://lkml.kernel.org/r/20240725054808.286708-3-anshuman.khandual@arm.com
Signed-off-by: Anshuman Khandual <anshuman.khandual@arm.com>
Cc: Arnd Bergmann <arnd@arndb.de>
Cc: Rasmus Villemoes <linux@rasmusvillemoes.dk>
Cc: Yury Norov <yury.norov@gmail.com>
