From: Andrew Morton <akpm@linux-foundation.org>
Subject: shmem-prepare-shmem-quota-infrastructure-fix
Date: Fri Jul 14 08:54:52 AM PDT 2023

make shmem_mark_dquot_dirty and shmem_dquot_write_info static

Reported-by: kernel test robot <lkp@intel.com>
Closes: https://lore.kernel.org/oe-kbuild-all/202307141341.R961dyMt-lkp@intel.com/
Cc: Carlos Maiolino <cem@kernel.org>
