From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: shmem: remove shmem_getpage()
Date: Fri, 2 Sep 2022 20:46:29 +0100

With all callers removed, remove this wrapper function.  The flags are now
mysteriously called SGP, but I think we can live with that.

Link: https://lkml.kernel.org/r/20220902194653.1739778-34-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
