From: Chengming Zhou <zhouchengming@bytedance.com>
Subject: mm/zsmalloc: remove the deferred free mechanism
Date: Tue, 27 Feb 2024 03:02:55 +0000

Since the only user of kick_deferred_free() has gone, remove all the
deferred mechanism related code.

Link: https://lkml.kernel.org/r/20240226-zsmalloc-zspage-rcu-v1-2-456b0ef1a89d@bytedance.com
Signed-off-by: Chengming Zhou <zhouchengming@bytedance.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Nhat Pham <nphamcs@gmail.com>
Cc: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Yosry Ahmed <yosryahmed@google.com>
