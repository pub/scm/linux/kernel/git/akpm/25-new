From: Muhammad Usama Anjum <usama.anjum@collabora.com>
Subject: selftests/mm: mrelease_test: conform test to TAP format output
Date: Fri, 2 Feb 2024 16:31:13 +0500

Conform the layout, informational and status messages to TAP.  No
functional change is intended other than the layout of output messages.

Link: https://lkml.kernel.org/r/20240202113119.2047740-7-usama.anjum@collabora.com
Signed-off-by: Muhammad Usama Anjum <usama.anjum@collabora.com>
Cc: Shuah Khan <shuah@kernel.org>
