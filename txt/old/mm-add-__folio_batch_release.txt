From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: add __folio_batch_release()
Date: Wed, 21 Jun 2023 17:45:46 +0100

This performs the same role as __pagevec_release(), ie skipping the check
for batch length of 0.

Link: https://lkml.kernel.org/r/20230621164557.3510324-3-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
