From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: remove page_idle and page_young wrappers
Date: Tue, 2 Apr 2024 21:12:50 +0100

All users have now been converted to the folio equivalents, so remove the
page wrappers.

Link: https://lkml.kernel.org/r/20240402201252.917342-4-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: David Hildenbrand <david@redhat.com>
