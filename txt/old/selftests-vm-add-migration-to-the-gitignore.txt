From: Muhammad Usama Anjum <usama.anjum@collabora.com>
Subject: selftests: vm: add migration to the .gitignore
Date: Sat, 21 May 2022 14:43:13 +0500

Add newly added migration test object to .gitignore file.

Link: https://lkml.kernel.org/r/20220521094313.166505-1-usama.anjum@collabora.com
Fixes: 0c2d08728470 ("mm: add selftests for migration entries")
Signed-off-by: Muhammad Usama Anjum <usama.anjum@collabora.com>
Reviewed-by: Alistair Popple <apopple@nvidia.com>
