From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-fix-get_mctgt_type-kernel-doc-fix
Date: Mon Aug 21 11:53:43 AM PDT 2023

changes suggested by Randy

Cc: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Cc: Mike Rapoport (IBM) <rppt@kernel.org>
Cc: Randy Dunlap <rdunlap@infradead.org>
