From: Li kunyu <kunyu@nfschina.com>
Subject: kernel: relay: remove unnecessary NULL values from relay_open_buf
Date: Fri, 14 Jul 2023 07:44:59 +0800

buf is assigned first, so it does not need to initialize the assignment.

Link: https://lkml.kernel.org/r/20230713234459.2908-1-kunyu@nfschina.com
Signed-off-by: Li kunyu <kunyu@nfschina.com>
Reviewed-by: Andrew Morton <akpm@linux-foudation.org>
