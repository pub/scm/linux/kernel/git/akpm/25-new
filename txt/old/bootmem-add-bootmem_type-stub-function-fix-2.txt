From: Arnd Bergmann <arnd@arndb.de>
Subject: bootmem: add bootmem_type stub function
Date: Tue, 15 Oct 2024 14:36:06 +0000

When CONFIG_HAVE_BOOTMEM_INFO_NODE is disabled, the bootmem_type() and
bootmem_info() functions are not defined:

mm/sparse.c: In function 'free_map_bootmem':
mm/sparse.c:730:24: error: implicit declaration of function 'bootmem_type' [-Wimplicit-function-declaration]
  730 |                 type = bootmem_type(page);
      |                        ^~~~~~~~~~~~

mm/sparse.c: In function 'free_map_bootmem':
mm/sparse.c:735:39: error: implicit declaration of function 'bootmem_info'; did you mean 'bootmem_type'? [-Wimplicit-function-declaration]

Add trivial stub functions to make it build.

Link: https://lkml.kernel.org/r/20241015143802.577613-1-arnd@kernel.org
Fixes: 43d552a255a6 ("bootmem: stop using page->index")
Signed-off-by: Arnd Bergmann <arnd@arndb.de>
Cc: "Matthew Wilcox (Oracle)" <willy@infradead.org>
