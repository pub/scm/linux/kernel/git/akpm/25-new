From: Usama Arif <usamaarif642@gmail.com>
Subject: mm: initialize zeromap to NULL at swapon and set it to NULL at swapoff
Date:   Wed Jul 10 11:21:56 2024 +0100

If swapon fails before zeromap is initialized, kvfree should operate on a
NULL pointer.

Link: https://lkml.kernel.org/r/053bd429-ae19-4beb-a733-a7a838b1e010@gmail.com
Fixes: 127f851ba92f ("mm: store zero pages to be swapped out in a bitmap")
Signed-off-by: Usama Arif <usamaarif642@gmail.com>
Reported-by: kernel test robot <oliver.sang@intel.com>
Closes: https://lore.kernel.org/oe-lkp/202407101031.c6c3c651-lkp@intel.com
Cc: Andi Kleen <ak@linux.intel.com>
Cc: Chengming Zhou <chengming.zhou@linux.dev>
Cc: David Hildenbrand <david@redhat.com>
Cc: Huang Ying <ying.huang@intel.com>
Cc: Hugh Dickins <hughd@google.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Nhat Pham <nphamcs@gmail.com>
Cc: Shakeel Butt <shakeel.butt@linux.dev>
Cc: Yosry Ahmed <yosryahmed@google.com>
