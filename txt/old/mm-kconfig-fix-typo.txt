From: Sophia Gabriella <sophia.gabriellla@outlook.com>
Subject: mm: Kconfig: fix typo
Date: Thu, 28 Jul 2022 16:51:39 +0000

Fixes a typo in the help section for ZSWAP.

Link: https://lkml.kernel.org/r/Message-ID:
Signed-off-by: Sophia Gabriella <sophia.gabriellla@outlook.com>
