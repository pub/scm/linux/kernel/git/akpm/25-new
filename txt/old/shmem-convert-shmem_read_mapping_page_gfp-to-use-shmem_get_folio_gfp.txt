From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: shmem: convert shmem_read_mapping_page_gfp() to use shmem_get_folio_gfp()
Date: Fri, 2 Sep 2022 20:46:19 +0100

Saves a couple of calls to compound_head().

Link: https://lkml.kernel.org/r/20220902194653.1739778-24-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
