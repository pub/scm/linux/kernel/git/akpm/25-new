From: Ben Dooks <ben.dooks@codethink.co.uk>
Subject: devres: show which resource was invalid in __devm_ioremap_resource()
Date: Wed, 21 Jun 2023 17:30:50 +0100

The other error prints in this call show the resource which wsan't valid,
so add this to the first print when it checks for basic validity of the
resource.

Link: https://lkml.kernel.org/r/20230621163050.477668-1-ben.dooks@codethink.co.uk
Signed-off-by: Ben Dooks <ben.dooks@codethink.co.uk>
Cc: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
