From: Yu Zhao <yuzhao@google.com>
Subject: mm-swap-remove-boilerplate-fix
Date: Sun, 4 Aug 2024 15:36:31 -0600

handle zero-length local_lock_t

Link: https://lkml.kernel.org/r/Zq_0X04WsqgUnz30@google.com
Signed-off-by: Yu Zhao <yuzhao@google.com>
Reported-by: Hugh Dickins <hughd@google.com>
Tested-by: Hugh Dickins <hughd@google.com>
