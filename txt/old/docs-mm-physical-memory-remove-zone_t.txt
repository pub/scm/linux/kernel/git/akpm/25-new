From: I Hsin Cheng <richard120310@gmail.com>
Subject: docs/mm: physical memory: Remove zone_t
Date: Wed, 15 Jan 2025 15:03:55 +0800

"zone_t" doesn't exist in current code base anymore, remove the
description of it.

Link: https://lkml.kernel.org/r/20250115070355.41769-1-richard120310@gmail.com
Signed-off-by: I Hsin Cheng <richard120310@gmail.com>
Reviewed-by: Randy Dunlap <rdunlap@infradead.org>
Cc: Ching-Chun (Jim) Huang <jserv@ccns.ncku.edu.tw>
Cc: Jonathan Corbet <corbet@lwn.net>
