From: Keith Busch <kbusch@kernel.org>
Subject: dmapool: push new blocks in ascending order
Date: Tue, 21 Feb 2023 08:54:00 -0800

Some users of the dmapool need their allocations to happen in ascending
order.  The recent optimizations pushed the blocks in reverse order, so
restore the previous behavior by linking the next available block from
low-to-high.

usb/chipidea/udc.c qh_pool called "ci_hw_qh".  My initial thought was
dmapool isn't the right API if you need a specific order when allocating
from it, but I can't readily test any changes to that driver.  Restoring
the previous behavior is easy enough.

Link: https://lkml.kernel.org/r/20230221165400.1595247-1-kbusch@meta.com
Fixes: ced6d06a81fb69 ("dmapool: link blocks across pages")
Signed-off-by: Keith Busch <kbusch@kernel.org>
Reported-by: Bryan O'Donoghue <bryan.odonoghue@linaro.org>
Tested-by: Bryan O'Donoghue <bryan.odonoghue@linaro.org>
Cc: Christoph Hellwig <hch@infradead.org>
