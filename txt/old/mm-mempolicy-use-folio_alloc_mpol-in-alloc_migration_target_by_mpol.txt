From: Kefeng Wang <wangkefeng.wang@huawei.com>
Subject: mm: mempolicy: use folio_alloc_mpol() in alloc_migration_target_by_mpol()
Date: Wed, 15 May 2024 15:07:08 +0800

Convert to use folio_alloc_mpol() to make vma_alloc_folio_noprof() to use
folio throughout.

Link: https://lkml.kernel.org/r/20240515070709.78529-4-wangkefeng.wang@huawei.com
Signed-off-by: Kefeng Wang <wangkefeng.wang@huawei.com>
Cc: Hugh Dickins <hughd@google.com>
