From: Suren Baghdasaryan <surenb@google.com>
Subject: lib: do limited memory accounting for modules with ARCH_NEEDS_WEAK_PER_CPU
Date: Tue, 2 Apr 2024 11:09:33 -0700

ARCH_NEEDS_WEAK_PER_CPU does not allow percpu variable definitions inside
a function, therefore memory allocation profiling can't use it.  This
definition is used only for modules, so we still can account core kernel
allocations and for modules we can do limited allocation accounting by
charging all of them to a single counter.  This is not ideal but better
than no accounting at all.

Link: https://lkml.kernel.org/r/20240402180933.1663992-2-surenb@google.com
Reported-by: kernel test robot <lkp@intel.com>
Closes: https://lore.kernel.org/oe-kbuild-all/202403290334.USWrYrMw-lkp@intel.com/
Signed-off-by: Suren Baghdasaryan <surenb@google.com>
Tested-by: Kees Cook <keescook@chromium.org>
Cc: Kent Overstreet <kent.overstreet@linux.dev>
Cc: Stephen Rothwell <sfr@canb.auug.org.au>
