From: Arnd Bergmann <arnd@arndb.de>
Subject: mm: export copy_to_kernel_nofault
Date: Fri, 18 Oct 2024 15:11:09 +0000

This symbol is now used on the kasan test module, so it needs to be
exported.

ERROR: modpost: "copy_to_kernel_nofault" [mm/kasan/kasan_test.ko] undefined!

Link: https://lkml.kernel.org/r/20241018151112.3533820-1-arnd@kernel.org
Signed-off-by: Arnd Bergmann <arnd@arndb.de>
Acked-by: David Hildenbrand <david@redhat.com>
Reviewed-by: Andrey Konovalov <andreyknvl@gmail.com>
