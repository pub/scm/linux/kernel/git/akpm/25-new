From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-shmem-extend-shmem_unused_huge_shrink-to-all-sizes-fix
Date: Tue Aug 27 07:16:26 PM PDT 2024

comment tweak, per David

Cc: David Hildenbrand <david@redhat.com>
Cc: Hugh Dickins <hughd@google.com>
Cc: Baolin Wang <baolin.wang@linux.alibaba.com>
Cc: Kirill A. Shutemov <kirill.shutemov@linux.intel.com>
