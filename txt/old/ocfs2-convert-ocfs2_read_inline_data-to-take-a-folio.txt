From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: ocfs2: convert ocfs2_read_inline_data() to take a folio
Date: Thu, 5 Dec 2024 17:16:47 +0000

All callers now have a folio, so pass it in.  We can use folio_fill_tail()
instead of open-coding it.  Saves a call to compound_head().

Link: https://lkml.kernel.org/r/20241205171653.3179945-20-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: Joseph Qi <joseph.qi@linux.alibaba.com>
Cc: Changwei Ge <gechangwei@live.cn>
Cc: Joel Becker <jlbec@evilplan.org>
Cc: Jun Piao <piaojun@huawei.com>
Cc: Junxiao Bi <junxiao.bi@oracle.com>
Cc: Mark Fasheh <mark@fasheh.com>
Cc: Mark Tinguely <mark.tinguely@oracle.com>
