From: "Vishal Moola (Oracle)" <vishal.moola@gmail.com>
Subject: hugetlb: Simplify hugetlb_wp() arguments
Date: Mon, 8 Apr 2024 10:21:44 -0700

simplify the function arguments, per Oscar and Muchun.

Link: https://lkml.kernel.org/r/ZhQtoFNZBNwBCeXn@fedora
Signed-off-by: Vishal Moola (Oracle) <vishal.moola@gmail.com>
Suggested-by: Muchun Song <muchun.song@linux.dev>
Suggested-by: Oscar Salvador <osalvador@suse.de>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
