From: "Liam R. Howlett" <Liam.Howlett@Oracle.com>
Subject: mm/mmap: convert brk to use vma iterator
Date: Fri, 20 Jan 2023 11:26:09 -0500

Use the vma iterator API for the brk() system call.  This will provide
type safety at compile time.

Link: https://lkml.kernel.org/r/20230120162650.984577-9-Liam.Howlett@oracle.com
Signed-off-by: Liam R. Howlett <Liam.Howlett@oracle.com>
