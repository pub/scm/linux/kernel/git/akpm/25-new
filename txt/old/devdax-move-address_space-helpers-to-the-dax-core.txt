From: Dan Williams <dan.j.williams@intel.com>
Subject: devdax: move address_space helpers to the DAX core
Date: Fri, 14 Oct 2022 16:58:32 -0700

In preparation for decamping get_dev_pagemap() and
put_devmap_managed_page() from code paths outside of DAX, device-dax needs
to track mapping references similar to the tracking done for fsdax.  Reuse
the same infrastructure as fsdax (dax_insert_entry() and
dax_delete_mapping_entry()).  For now, just move that infrastructure into
a common location with no other code changes.

The move involves splitting iomap and supporting helpers into fs/dax.c and
all 'struct address_space' and DAX-entry manipulation into
drivers/dax/mapping.c.  grab_mapping_entry() is renamed
dax_grab_mapping_entry(), and some common definitions and declarations are
moved to include/linux/dax.h.

No functional change is intended, just code movement.

The interactions between drivers/dax/mapping.o and mm/memory-failure.o
result in drivers/dax/mapping.o and the rest of the dax core losing its
option to be compiled as a module.  That can be addressed later given the
fact the CONFIG_FS_DAX has always been forcing the dax core to be
built-in.  I.e.  this is only a potential vmlinux size regression for
CONFIG_FS_DAX=n and CONFIG_DEV_DAX=m builds which are not common.

Link: https://lkml.kernel.org/r/166579191217.2236710.8295835680465098304.stgit@dwillia2-xfh.jf.intel.com
Signed-off-by: Dan Williams <dan.j.williams@intel.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Jan Kara <jack@suse.cz>
Cc: "Darrick J. Wong" <djwong@kernel.org>
Cc: Jason Gunthorpe <jgg@nvidia.com>
Cc: Christoph Hellwig <hch@lst.de>
Cc: John Hubbard <jhubbard@nvidia.com>
Cc: Alex Deucher <alexander.deucher@amd.com>
Cc: Alistair Popple <apopple@nvidia.com>
Cc: Ben Skeggs <bskeggs@redhat.com>
Cc: "Christian König" <christian.koenig@amd.com>
Cc: Daniel Vetter <daniel@ffwll.ch>
Cc: Dave Chinner <david@fromorbit.com>
Cc: David Airlie <airlied@linux.ie>
Cc: Felix Kuehling <Felix.Kuehling@amd.com>
Cc: Jerome Glisse <jglisse@redhat.com>
Cc: Karol Herbst <kherbst@redhat.com>
Cc: kernel test robot <lkp@intel.com>
Cc: Lyude Paul <lyude@redhat.com>
Cc: "Pan, Xinhui" <Xinhui.Pan@amd.com>
