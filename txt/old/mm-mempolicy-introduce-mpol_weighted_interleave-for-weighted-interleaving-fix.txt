From: Gregory Price <gregory.price@memverge.com>
Subject: mm-mempolicy-introduce-mpol_weighted_interleave-for-weighted-interleaving-fix.
Date: Wed, 31 Jan 2024 00:12:24 -0500

kill next_node in favor of operating directly on il_prev

Link: https://lkml.kernel.org/r/ZbnWuB4dRCEFRz2m@memverge.com
Signed-off-by: Gregory Price <gregory.price@memverge.com>
Reviewed-by: "Huang, Ying" <ying.huang@intel.com>
Cc: Andi Kleen <ak@linux.intel.com>
Cc: Dan Williams <dan.j.williams@intel.com>
Cc: Frank van der Linden <fvdl@google.com>
Cc: Hasan Al Maruf <Hasan.Maruf@amd.com>
Cc: Honggyu Kim <honggyu.kim@sk.com>
Cc: Huang Ying <ying.huang@intel.com>
Cc: Hyeongtak Ji <hyeongtak.ji@sk.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Jonathan Cameron <Jonathan.Cameron@huawei.com>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Michal Hocko <mhocko@kernel.org>
Cc: Michal Hocko <mhocko@suse.com>
Cc: Rakie Kim <rakie.kim@sk.com>
Cc: Ravi Jonnalagadda <ravis.opensrc@micron.com>
Cc: Srinivasulu Thanneeru <sthanneeru.opensrc@micron.com>
