From: "Vishal Moola (Oracle)" <vishal.moola@gmail.com>
Subject: f2fs: convert last_fsync_dnode() to use filemap_get_folios_tag()
Date: Wed, 4 Jan 2023 13:14:40 -0800

Convert to use a folio_batch instead of pagevec.  This is in preparation
for the removal of find_get_pages_range_tag().

Link: https://lkml.kernel.org/r/20230104211448.4804-16-vishal.moola@gmail.com
Signed-off-by: Vishal Moola (Oracle) <vishal.moola@gmail.com>
Acked-by: Chao Yu <chao@kernel.org>
