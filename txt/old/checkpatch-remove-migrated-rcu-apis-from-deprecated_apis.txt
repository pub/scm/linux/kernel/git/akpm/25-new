From: David Reaver <me@davidreaver.com>
Subject: checkpatch: remove migrated RCU APIs from deprecated_apis
Date: Wed, 8 Jan 2025 11:24:54 -0800

The deprecated_apis map was created in [1] so checkpatch would flag
deprecated RCU APIs.  These deprecated APIs have since been removed from
the kernel.  This patch removes them from this map so checkpatch doesn't
waste time looking for them, and so readers of checkpatch looking for
deprecated APIs don't waste time searching for them.

Link: https://lore.kernel.org/all/20181111192904.3199-13-paulmck@linux.ibm.com/ [1]

Link: https://lkml.kernel.org/r/20250108192456.47871-1-me@davidreaver.com
Signed-off-by: David Reaver <me@davidreaver.com>
Reviewed-by: Paul E. McKenney <paulmck@kernel.org>
Reviewed-by: Kuan-Wei Chiu <visitorckw@gmail.com>
Acked-by: Joe Perches <joe@perches.com>
Cc: Andy Whitcroft <apw@canonical.com>
Cc: Dwaipayan Ray <dwaipayanray1@gmail.com>
Cc: Krister Johansen <kjlx@templeofstupid.com>
Cc: Lukas Bulwahn <lukas.bulwahn@gmail.com>
