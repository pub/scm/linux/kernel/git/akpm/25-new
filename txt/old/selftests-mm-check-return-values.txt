From: Muhammad Usama Anjum <usama.anjum@collabora.com>
Subject: selftests: mm: check return values
Date: Mon, 20 May 2024 11:52:48 -0700

Check return value and return error/skip the tests.

Link: https://lkml.kernel.org/r/20240520185248.1801945-1-usama.anjum@collabora.com
Fixes: 46fd75d4a3c9 ("selftests: mm: add pagemap ioctl tests")
Signed-off-by: Muhammad Usama Anjum <usama.anjum@collabora.com>
Cc: Shuah Khan <shuah@kernel.org>
