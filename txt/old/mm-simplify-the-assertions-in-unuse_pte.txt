From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: simplify the assertions in unuse_pte()
Date: Mon, 11 Dec 2023 16:22:07 +0000

We should only see anon folios in this function (and there are many
assumptions of that already), so we can simplify these two assertions.

Link: https://lkml.kernel.org/r/20231211162214.2146080-3-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
