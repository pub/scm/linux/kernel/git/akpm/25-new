From: Haifeng Xu <haifeng.xu@shopee.com>
Subject: memcg, oom: simplify mem_cgroup_oom_synchronize
Date: Fri, 7 Apr 2023 09:06:44 +0000

Since commit 29ef680ae7c2 ("memcg, oom: move out_of_memory back to the
charge path"), only if oom_kill_disable is set, oom killer will be
delayed to page fault path.  So the oom_kill_disable check is
unnecessary.

In the charge patch, even if the oom_lock in memcg can't be acquired, the
oom handing can also be invoked.  In order to keep the behavior consistent
with it, remove the lock check.

Also the explicit wakeup for the lock holder is unneeded because the lock
contender won't be scheduled out.

Link: https://lkml.kernel.org/r/20230407090644.399108-1-haifeng.xu@shopee.com
Signed-off-by: Haifeng Xu <haifeng.xu@shopee.com>
Suggested-by: Michal Hocko <mhocko@kernel.org>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Shakeel Butt <shakeelb@google.com>
