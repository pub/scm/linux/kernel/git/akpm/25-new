From: "Liam R. Howlett" <Liam.Howlett@Oracle.com>
Subject: mm/mmap: don't use __vma_adjust() in shift_arg_pages()
Date: Fri, 20 Jan 2023 11:26:46 -0500

Introduce shrink_vma() which uses the vma_prepare() and vma_complete()
functions to reduce the vma coverage.

Convert shift_arg_pages() to use expand_vma() and the new shrink_vma()
function.  Remove support from __vma_adjust() to reduce a vma size since
shift_arg_pages() is the only user that shrinks a VMA in this way.

Link: https://lkml.kernel.org/r/20230120162650.984577-46-Liam.Howlett@oracle.com
Signed-off-by: Liam R. Howlett <Liam.Howlett@oracle.com>
