From: Samuel Holland <samuel.holland@sifive.com>
Subject: ARM: do not select ARCH_HAS_KERNEL_FPU_SUPPORT
Date: Wed, 8 May 2024 18:37:10 -0700

On 32-bit ARM, conversions between `double` and `long long` require
runtime library support.  Since the kernel does not currently provide this
library support, the amdgpu driver fails to build:

  ERROR: modpost: "__aeabi_l2d" [drivers/gpu/drm/amd/amdgpu/amdgpu.ko] undefined!
  ERROR: modpost: "__aeabi_d2ulz" [drivers/gpu/drm/amd/amdgpu/amdgpu.ko] undefined!

As Arnd reports, there are likely no 32-bit ARM platforms which can use
the amdgpu driver anyway, due to lack of features like 64-bit prefetchable
BARs.  Since amdgpu is currently the only real user of
ARCH_HAS_KERNEL_FPU_SUPPORT, drop support for this option instead of
bothering to implement the library functions.

Link: https://lkml.kernel.org/r/20240509013727.648600-1-samuel.holland@sifive.com
Fixes: 12624fe2d707 ("ARM: implement ARCH_HAS_KERNEL_FPU_SUPPORT")
Signed-off-by: Samuel Holland <samuel.holland@sifive.com>
Reported-by: Thiago Jung Bauermann <thiago.bauermann@linaro.org>
Closes: https://lore.kernel.org/lkml/87wmp4oo3y.fsf@linaro.org/
Reported-by: kernel test robot <lkp@intel.com>
Closes: https://lore.kernel.org/oe-kbuild-all/202404042327.jRpt81kP-lkp@intel.com/
Suggested-by: Ard Biesheuvel <ardb@kernel.org>
