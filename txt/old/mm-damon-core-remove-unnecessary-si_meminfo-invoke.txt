From: Huan Yang <link@vivo.com>
Subject: mm/damon/core: remove unnecessary si_meminfo invoke.
Date: Wed, 20 Sep 2023 09:57:27 +0800

si_meminfo() will read and assign more info not just free/ram pages.  For
just DAMOS_WMARK_FREE_MEM_RATE use, only get free and ram pages is ok to
save cpu.

Link: https://lkml.kernel.org/r/20230920015727.4482-1-link@vivo.com
Signed-off-by: Huan Yang <link@vivo.com>
Reviewed-by: SeongJae Park <sj@kernel.org>
