From: Liu Shixin <liushixin2@huawei.com>
Subject: mm-vmscan-try-to-reclaim-swapcache-pages-if-no-swap-space-v6
Date: Fri, 15 Sep 2023 16:34:17 +0800

fix NULL pointing derefence and hung task problem reported by Sachin

Link: https://lkml.kernel.org/r/20230915083417.3190512-1-liushixin2@huawei.com
Signed-off-by: Liu Shixin <liushixin2@huawei.com>
Tested-by: Yosry Ahmed <yosryahmed@google.com>
Reviewed-by: "Huang, Ying" <ying.huang@intel.com>
Reviewed-by: Yosry Ahmed <yosryahmed@google.com>
Cc: "Huang, Ying" <ying.huang@intel.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Kefeng Wang <wangkefeng.wang@huawei.com>
Cc: Michal Hocko <mhocko@suse.com>
Cc: Sachin Sant <sachinp@linux.ibm.com>
Cc: Yosry Ahmed <yosryahmed@google.com>
Cc: Muchun Song <muchun.song@linux.dev>
Cc: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Shakeel Butt <shakeelb@google.com>
