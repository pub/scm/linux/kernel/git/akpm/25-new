From: Kefeng Wang <wangkefeng.wang@huawei.com>
Subject: mm: migrate: add folio_isolate_movable()
Date: Thu, 29 Aug 2024 22:54:53 +0800

Like isolate_lru_page(), make isolate_movable_page() as a wrapper around
folio_isolate_movable(), since isolate_movable_page() always fails on a
tail page, return immediately for a tail page in the warpper, and the
wrapper will be removed once all callers are converted to
folio_isolate_movable().

Note all isolate_movable_page() users increased page reference, so replace
redundant folio_get_nontail_page() with folio_get() and add a reference
count check into folio_isolate_movable().

Link: https://lkml.kernel.org/r/20240829145456.2591719-3-wangkefeng.wang@huawei.com
Signed-off-by: Kefeng Wang <wangkefeng.wang@huawei.com>
Reviewed-by: Vishal Moola (Oracle) <vishal.moola@gmail.com>
Cc: Baolin Wang <baolin.wang@linux.alibaba.com>
Cc: David Hildenbrand <david@redhat.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Zi Yan <ziy@nvidia.com>
