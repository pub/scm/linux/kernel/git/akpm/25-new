From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: make is_free_buddy_page() take a const argument
Date: Tue, 26 Mar 2024 17:10:27 +0000

This function does not modify its argument; let the callers know that so
they can make better optimisation decisions.

Link: https://lkml.kernel.org/r/20240326171045.410737-6-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
