From: Andrei Vagin <avagin@google.com>
Subject: fs-proc-task_mmu-report-soft_dirty-bits-through-the-pagemap_scan-ioctl-v3
Date: Tue, 7 Nov 2023 08:41:37 -0800

update tools/include/uapi/linux/fs.h

Link: https://lkml.kernel.org/r/20231107164139.576046-1-avagin@google.com
Signed-off-by: Andrei Vagin <avagin@google.com>
Reviewed-by: Muhammad Usama Anjum <usama.anjum@collabora.com>
Cc: Michał Mirosław <mirq-linux@rere.qmqm.pl>
