From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-always-lock-new-vma-before-inserting-into-vma-tree-fix
Date: Mon Aug 14 12:13:22 PM PDT 2023

fix reject fixing in vma_link(), per Jann

Cc: Jann Horn <jannh@google.com>
Cc: Liam R. Howlett <Liam.Howlett@oracle.com>
Cc: Suren Baghdasaryan <surenb@google.com>
