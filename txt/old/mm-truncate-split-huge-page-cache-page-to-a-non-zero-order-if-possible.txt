From: Zi Yan <ziy@nvidia.com>
Subject: mm: truncate: split huge page cache page to a non-zero order if possible
Date: Mon, 3 Apr 2023 16:18:38 -0400

To minimize the number of pages after a huge page truncation, we do not
need to split it all the way down to order-0.  The huge page has at most
three parts, the part before offset, the part to be truncated, the part
remaining at the end.  Find the greatest common divisor of them to
calculate the new page order from it, so we can split the huge page to
this order and keep the remaining pages as large and as few as possible.

Link: https://lkml.kernel.org/r/20230403201839.4097845-7-zi.yan@sent.com
Signed-off-by: Zi Yan <ziy@nvidia.com>
Cc: Kirill A. Shutemov <kirill.shutemov@linux.intel.com>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Michal Koutný <mkoutny@suse.com>
Cc: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Ryan Roberts <ryan.roberts@arm.com>
Cc: Yang Shi <shy828301@gmail.com>
Cc: Yu Zhao <yuzhao@google.com>
Cc: Zach O'Keefe <zokeefe@google.com>
