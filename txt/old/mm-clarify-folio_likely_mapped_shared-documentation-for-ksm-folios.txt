From: David Hildenbrand <david@redhat.com>
Subject: mm: clarify folio_likely_mapped_shared() documentation for KSM folios
Date: Wed, 31 Jul 2024 18:07:58 +0200

For KSM folios, the function actually does what it is supposed to do: even
having multiple mappings inside the same MM is considered "sharing", as
there is no real relationship between these KSM page mappings -- in
contrast to mapping the same file range twice and having the same
pagecache page mapped twice.

Link: https://lkml.kernel.org/r/20240731160758.808925-1-david@redhat.com
Signed-off-by: David Hildenbrand <david@redhat.com>
