From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: page_io: remove buffer_head include
Date: Thu, 15 Dec 2022 21:43:56 +0000

page_io never uses buffer_heads to do I/O.

Link: https://lkml.kernel.org/r/20221215214402.3522366-7-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: Jan Kara <jack@suse.cz>
