From: SeongJae Park <sj@kernel.org>
Subject: Docs/admin-guide/mm/damon/usage: link design doc for details of kdamond and context
Date: Thu, 7 Sep 2023 02:29:25 +0000

The explanation of kdamond and context is duplicated in the design and
the usage documents.  Replace that in the usage with links to those in
the design document.

Link: https://lkml.kernel.org/r/20230907022929.91361-8-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Steven Rostedt (Google) <rostedt@goodmis.org>
