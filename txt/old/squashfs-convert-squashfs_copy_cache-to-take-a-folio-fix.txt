From: Dan Carpenter <dan.carpenter@linaro.org>
Subject: squashfs: fix a NULL vs IS_ERR() bug
Date: Wed, 8 Jan 2025 12:16:30 +0300

__filemap_get_folio() never returns NULL, it returns error pointers.  This
incorrect check would lead to an Oops on the following line when we pass
"push_folio" to folio_test_uptodate().

Link: https://lkml.kernel.org/r/7b7f44d6-9153-4d7c-b65b-2d78febe6c7a@stanley.mountain
Signed-off-by: Dan Carpenter <dan.carpenter@linaro.org>
Cc: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Cc: Phillip Lougher <phillip@squashfs.org.uk>
