From: Alexey Dobriyan <adobriyan@gmail.com>
Subject: proc: support proc-empty-vm test on i386
Date: Fri, 30 Jun 2023 21:34:33 +0300

Unmap everything starting from 4GB length until it unmaps, otherwise test
has to detect which virtual memory split kernel is using.

Link: https://lkml.kernel.org/r/20230630183434.17434-1-adobriyan@gmail.com
Signed-off-by: Alexey Dobriyan <adobriyan@gmail.com>
Cc: Björn Töpel <bjorn@kernel.org>
