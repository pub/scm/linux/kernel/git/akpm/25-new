From: Andrew Morton <akpm@linux-foundation.org>
Subject: xfs-support-cow-in-fsdax-mode-fix
Date: Sat Jun  4 11:45:10 AM PDT 2022

make xfs_dax_fault() static

Reported-by: kernel test robot <lkp@intel.com>
Cc: Shiyang Ruan <ruansy.fnst@fujitsu.com>
