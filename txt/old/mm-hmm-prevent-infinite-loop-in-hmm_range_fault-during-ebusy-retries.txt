From: sooraj <sooraj20636@gmail.com>
Subject: mm/hmm: prevent infinite loop in hmm_range_fault during EBUSY retries
Date: Tue, 28 Jan 2025 01:34:22 -0500

When hmm_vma_walk_test() skips a VMA (e.g., unsupported VM_IO/PFNMAP
range), it must update hmm_vma_walk->last to the end of the skipped VMA. 
Failing to do so causes hmm_range_fault() to restart from the same address
during -EBUSY retries, reprocessing the skipped VMA indefinitely.  This
results in an infinite loop if the VMA remains non-processable.

Update hmm_vma_walk->last to the VMA's end address in hmm_vma_walk_test()
when skipping the range.  This ensures subsequent iterations resume
correctly after the skipped VMA, preventing infinite retry loops.

Link: https://lkml.kernel.org/r/20250128063422.7604-1-sooraj20636@gmail.com
Signed-off-by: sooraj <sooraj20636@gmail.com>
Cc: Jason Gunthorpe <jgg@nvidia.com>
Cc: Jerome Glisse <jglisse@redhat.com>
Cc: Alistair Popple <apopple@nvidia.com>
