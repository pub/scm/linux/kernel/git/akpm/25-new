From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: convert free_pages_and_swap_cache() to use folios_put()
Date: Tue, 27 Feb 2024 17:42:50 +0000

Process the pages in batch-sized quantities instead of all-at-once.

Link: https://lkml.kernel.org/r/20240227174254.710559-17-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: David Hildenbrand <david@redhat.com>
Cc: Mel Gorman <mgorman@suse.de>
Cc: Ryan Roberts <ryan.roberts@arm.com>
