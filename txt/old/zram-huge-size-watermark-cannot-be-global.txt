From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: zram: huge size watermark cannot be global
Date: Mon, 31 Oct 2022 14:41:04 +0900

ZRAM will pass pool specific limit on number of pages per-zspages which
will affect pool's characteristics.  Namely huge size class watermark
value.  Move huge_class_size to struct zram, because this value now can be
unique to the pool (zram device).

Link: https://lkml.kernel.org/r/20221031054108.541190-6-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Alexey Romanov <avromanov@sberdevices.ru>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Nitin Gupta <ngupta@vflare.org>
