From: Kefeng Wang <wangkefeng.wang@huawei.com>
Subject: mm: memory: convert do_cow_fault to use folios
Date: Thu, 12 Jan 2023 16:30:02 +0800

The page functions are converted to corresponding folio functions in
do_cow_fault().

Link: https://lkml.kernel.org/r/20230112083006.163393-4-wangkefeng.wang@huawei.com
Signed-off-by: Kefeng Wang <wangkefeng.wang@huawei.com>
Cc: Matthew Wilcox <willy@infradead.org>
