From: Andrew Morton <akpm@linux-foundation.org>
Subject: procfs-add-path-to-proc-pid-fdinfo-fix
Date: Wed Aug 17 10:04:52 AM PDT 2022

warning: Local variable 'anon_aops' shadows outer variable

Reported-by: kernel test robot <lkp@intel.com>
Cc: Kalesh Singh <kaleshsingh@google.com>
