From: Andrew Morton <akpm@linux-foundation.org>
Subject: alloc_tag-load-module-tags-into-separate-contiguous-memory-fix-fix
Date: Sun Nov  3 08:26:32 PM PST 2024

update comment, per Dan

Cc: Dan Carpenter <dan.carpenter@linaro.org>
Cc: Suren Baghdasaryan <surenb@google.com>
