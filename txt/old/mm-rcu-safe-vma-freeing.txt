From: Michel Lespinasse <michel@lespinasse.org>
Subject: mm: rcu safe VMA freeing
Date: Mon, 27 Feb 2023 09:36:09 -0800

This prepares for page faults handling under VMA lock, looking up VMAs
under protection of an rcu read lock, instead of the usual mmap read lock.

Link: https://lkml.kernel.org/r/20230227173632.3292573-11-surenb@google.com
Signed-off-by: Michel Lespinasse <michel@lespinasse.org>
Signed-off-by: Suren Baghdasaryan <surenb@google.com>
