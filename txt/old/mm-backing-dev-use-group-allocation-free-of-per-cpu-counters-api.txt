From: Kefeng Wang <wangkefeng.wang@huawei.com>
Subject: mm: backing-dev: use group allocation/free of per-cpu counters API
Date: Mon, 25 Mar 2024 11:56:35 +0800

Use group allocation/free of per-cpu counters api to accelerate
wb_init/exit() and simplify code.

Link: https://lkml.kernel.org/r/20240325035635.49342-1-wangkefeng.wang@huawei.com
Signed-off-by: Kefeng Wang <wangkefeng.wang@huawei.com>
Cc: Dennis Zhou <dennis@kernel.org>
