From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: remove references to page_add_new_anon_rmap in comments
Date: Mon, 11 Dec 2023 16:22:11 +0000

Refer to folio_add_new_anon_rmap() instead.

Link: https://lkml.kernel.org/r/20231211162214.2146080-7-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: David Hildenbrand <david@redhat.com>
