From: Bagas Sanjaya <bagasdotme@gmail.com>
Subject: .mailmap: map Benjamin Poirier's address
Date: Mon, 30 Oct 2023 21:24:55 +0700

Map out to his gmail address as he had left SUSE some time ago.

Link: https://lkml.kernel.org/r/20231030142454.22127-2-bagasdotme@gmail.com
Signed-off-by: Bagas Sanjaya <bagasdotme@gmail.com>
Acked-by: Benjamin Poirier <benjamin.poirier@gmail.com>
Cc: Bjorn Andersson <quic_bjorande@quicinc.com>
Cc: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Cc: Heiko Stuebner <heiko@sntech.de>
Cc: Jakub Kicinski <kuba@kernel.org>
Cc: Konrad Dybcio <konrad.dybcio@linaro.org>
Cc: Oleksij Rempel <o.rempel@pengutronix.de>
Cc: Stephen Hemminger <stephen@networkplumber.org>
