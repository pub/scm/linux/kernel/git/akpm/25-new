From: SeongJae Park <sj@kernel.org>
Subject: Docs/ABI/damon: document scheme filters files
Date: Mon, 5 Dec 2022 23:08:30 +0000

Document newly added DAMON sysfs interface files for DAMOS filtering on
the DAMON ABI document.

Link: https://lkml.kernel.org/r/20221205230830.144349-12-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Shuah Khan <shuah@kernel.org>
