From: Wang Jinchao <wangjinchao@xfusion.com>
Subject: crash_core: remove duplicated including of kexec.h
Date: Fri, 15 Dec 2023 16:54:51 +0800

Remove second include of linux/kexec.h

Link: https://lkml.kernel.org/r/202312151654+0800-wangjinchao@xfusion.com
Signed-off-by: Wang Jinchao <wangjinchao@xfusion.com>
Acked-by: Baoquan He <bhe@redhat.com>
Cc: Dave Young <dyoung@redhat.com>
Cc: Vivek Goyal <vgoyal@redhat.com>
