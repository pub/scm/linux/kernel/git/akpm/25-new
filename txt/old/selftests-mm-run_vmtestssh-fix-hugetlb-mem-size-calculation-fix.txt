From: Peter Xu <peterx@redhat.com>
Subject: fixup! selftests/mm: run_vmtests.sh: fix hugetlb mem size calculation
Date: Wed, 3 Apr 2024 16:03:24 -0400

Fix up a breakage on uffd hugetlb test due to removal of a temp variable,
as reported by Ryan [1].

Instead of using the previous calculation, use the largest we can
have (which is put in freepgs) and cut it into half for userfault tests.

[1] https://lore.kernel.org/r/1c20b717-c5b5-4bdf-8fcd-d46db135b7fa@arm.com

Link: https://lkml.kernel.org/r/20240403200324.1603493-1-peterx@redhat.com
Signed-off-by: Peter Xu <peterx@redhat.com>
Reported-by: Ryan Roberts <ryan.roberts@arm.com>
Tested-by: Ryan Roberts <ryan.roberts@arm.com>
Cc: Muhammad Usama Anjum <usama.anjum@collabora.com>
Cc: David Hildenbrand <david@redhat.com>
Cc: Muchun Song <muchun.song@linux.dev>
