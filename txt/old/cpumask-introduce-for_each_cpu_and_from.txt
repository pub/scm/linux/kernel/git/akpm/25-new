From: Yury Norov <yury.norov@gmail.com>
Subject: cpumask: introduce for_each_cpu_and_from()
Date: Thu, 28 Dec 2023 12:09:28 -0800

Patch series "lib/group_cpus: rework grp_spread_init_one() and make it
O(1)", v4.

grp_spread_init_one() implementation is sub-optimal because it traverses
bitmaps from the beginning, instead of picking from the previous
iteration.

Fix it and use find_bit API where appropriate.  While here, optimize
cpumasks allocation and drop unneeded cpumask_empty() call.


This patch (of 9):

Similarly to for_each_cpu_and(), introduce a for_each_cpu_and_from(),
which is handy when it's needed to traverse 2 cpumasks or bitmaps,
starting from a given position.

Link: https://lkml.kernel.org/r/20231228200936.2475595-1-yury.norov@gmail.com
Link: https://lkml.kernel.org/r/20231228200936.2475595-2-yury.norov@gmail.com
Signed-off-by: Yury Norov <yury.norov@gmail.com>
Cc: Andy Shevchenko <andriy.shevchenko@linux.intel.com>
Cc: Ming Lei <ming.lei@redhat.com>
Cc: Rasmus Villemoes <linux@rasmusvillemoes.dk>
Cc: Thomas Gleixner <tglx@linutronix.de>
