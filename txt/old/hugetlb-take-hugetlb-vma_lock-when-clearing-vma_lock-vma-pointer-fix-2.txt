From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm/hugetlb.c: make __hugetlb_vma_unlock_write_put() static
Date: Fri Oct  7 12:59:20 PM PDT 2022

Reported-by: kernel test robot <lkp@intel.com>
Cc: Mike Kravetz <mike.kravetz@oracle.com>
