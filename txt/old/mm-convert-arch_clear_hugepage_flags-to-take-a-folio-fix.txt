From: Matthew Wilcox <willy@infradead.org>
Subject: mm-convert-arch_clear_hugepage_flags-to-take-a-folio-fix
Date: Wed, 27 Mar 2024 14:37:40 +0000

fix arm64 build

Link: https://lkml.kernel.org/r/ZgQvNKGdlDkwhQEX@casper.infradead.org
Signed-off-by: Matthew Wilcox <willy@infradead.org>
Reported-by: Ryan Roberts <ryan.roberts@arm.com>
