From: Yu Zhao <yuzhao@google.com>
Subject: mm-mglru-rework-workingset-protection-fix
Date: Tue, 24 Dec 2024 12:04:44 -0700

Revert change to folio_mark_accessed() that unncessarily dirties cache
lines shared between different cores.

Link: https://lkml.kernel.org/r/Z2sFzKmHPI0kI_fq@google.com
Signed-off-by: Yu Zhao <yuzhao@google.com>
Tested-by: kernel test robot <oliver.sang@intel.com>
