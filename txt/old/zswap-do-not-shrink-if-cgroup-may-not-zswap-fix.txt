From: Andrew Morton <akpm@linux-foundation.org>
Subject: zswap-do-not-shrink-if-cgroup-may-not-zswap-fix
Date: Thu Jun  1 01:17:06 PM PDT 2023

fix return of unintialized value

Reported-by: kernel test robot <lkp@intel.com>
Closes: https://lore.kernel.org/oe-kbuild-all/202306011435.2BxsHFUE-lkp@intel.com/
Cc: Dan Streetman <ddstreet@ieee.org>
Cc: Domenico Cerasuolo <cerasuolodomenico@gmail.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Nhat Pham <nphamcs@gmail.com>
Cc: Seth Jennings <sjenning@redhat.com>
Cc: Vitaly Wool <vitaly.wool@konsulko.com>
Cc: Yosry Ahmed <yosryahmed@google.com>
