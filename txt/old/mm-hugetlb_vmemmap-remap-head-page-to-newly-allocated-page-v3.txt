From: Joao Martins <joao.m.martins@oracle.com>
Subject: mm-hugetlb_vmemmap-remap-head-page-to-newly-allocated-page-v3
Date: Wed, 9 Nov 2022 20:06:23 +0000

 Comments from Muchun:
 * Delete the comment above the tlb flush
 * Move the head vmemmap page copy into vmemmap_remap_free()
 * Add and del the new head page to the vmemmap_pages (to be freed
   in case of error)
 * Move the remap of the head like the tail pages in vmemmap_remap_pte()
   but special casing only when addr == reuse_Addr
 * Removes the PAGE_SIZE alignment check as the code has the assumption
   that start/end are page-aligned (and VM_BUG_ON otherwise).
 * Adjusted commit message taking the above changes into account.

Link: https://lkml.kernel.org/r/20221109200623.96867-1-joao.m.martins@oracle.com
Signed-off-by: Joao Martins <joao.m.martins@oracle.com>
Cc: Mike Kravetz <mike.kravetz@oracle.com>
Cc: Muchun Song <songmuchun@bytedance.com>
