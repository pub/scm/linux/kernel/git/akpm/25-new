From: Johannes Weiner <hannes@cmpxchg.org>
Subject: mm-compaction-refactor-__compaction_suitable-fix
Date: Fri, 2 Jun 2023 10:49:42 -0400

Vlastimil points out the function comment is stale now. Update it.

Link: https://lkml.kernel.org/r/20230602144942.GC161817@cmpxchg.org
Signed-off-by: Johannes Weiner <hannes@cmpxchg.org>
Cc: Baolin Wang <baolin.wang@linux.alibaba.com>
Cc: Mel Gorman <mgorman@techsingularity.net>
Cc: Michal Hocko <mhocko@suse.com>
Cc: Vlastimil Babka <vbabka@suse.cz>
