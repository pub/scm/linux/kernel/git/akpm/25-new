From: David Hildenbrand <david@redhat.com>
Subject: mm/gup_test: free memory allocated via kvcalloc() using kvfree()
Date: Mon, 12 Dec 2022 19:20:18 +0100

We have to free via kvfree(), not via kfree().

Link: https://lkml.kernel.org/r/20221212182018.264900-1-david@redhat.com
Fixes: c77369b437f9 ("mm/gup_test: start/stop/read functionality for PIN LONGTERM test")
Signed-off-by: David Hildenbrand <david@redhat.com>
Reported-by: kernel test robot <lkp@intel.com>
Reported-by: Julia Lawall <julia.lawall@lip6.fr>
