From: Andrew Morton <akpm@linux-foundation.org>
Subject: acpi-hmat-calculate-abstract-distance-with-hmat-checkpatch-fixes
Date: Mon Jul 24 10:59:52 AM PDT 2023


WARNING: Avoid logging continuation uses where feasible
#54: FILE: drivers/acpi/numa/hmat.c:768:
+	pr_cont("read_latency: %u, write_latency: %u, read_bandwidth: %u, write_bandwidth: %u\n",

WARNING: __meminitdata should be placed after hmat_adist_nb
#174: FILE: drivers/acpi/numa/hmat.c:888:
+static __meminitdata struct notifier_block hmat_adist_nb =

ERROR: that open brace { should be on the previous line
#175: FILE: drivers/acpi/numa/hmat.c:889:
+static __meminitdata struct notifier_block hmat_adist_nb =
+{

WARNING: From:/Signed-off-by: email name mismatch: 'From: Huang Ying <ying.huang@intel.com>' != 'Signed-off-by: "Huang, Ying" <ying.huang@intel.com>'

total: 1 errors, 3 warnings, 185 lines checked

NOTE: For some of the reported defects, checkpatch may be able to
      mechanically convert to the typical style using --fix or --fix-inplace.

./patches/acpi-hmat-calculate-abstract-distance-with-hmat.patch has style problems, please review.

NOTE: If any of the errors are false positives, please report
      them to the maintainer, see CHECKPATCH in MAINTAINERS.

Please run checkpatch prior to sending patches

Cc: "Huang, Ying" <ying.huang@intel.com>
