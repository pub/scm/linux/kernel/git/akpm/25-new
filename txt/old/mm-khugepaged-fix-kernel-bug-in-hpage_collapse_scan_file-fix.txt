From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-khugepaged-fix-kernel-bug-in-hpage_collapse_scan_file-fix
Date: Wed Mar 29 02:50:56 PM PDT 2023

update include/trace/events/huge_memory.h's SCAN_STATUS

Cc: Himadri Pandya <himadrispandya@gmail.com>
Cc: Ivan Orlov <ivan.orlov0322@gmail.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Kirill A. Shutemov <kirill.shutemov@linux.intel.com>
Cc: Rik van Riel <riel@surriel.com>
Cc: Shuah Khan <skhan@linuxfoundation.org>
Cc: Song Liu <songliubraving@fb.com>
