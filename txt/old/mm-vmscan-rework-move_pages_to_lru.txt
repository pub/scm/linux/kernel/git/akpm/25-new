From: Muchun Song <songmuchun@bytedance.com>
Subject: mm: vmscan: rework move_pages_to_lru()
Date: Tue, 21 Jun 2022 20:56:52 +0800

In a later patch, we will reparent the LRU pages.  The pages moved to
appropriate LRU list can be reparented during the process of the
move_pages_to_lru().  So holding a lruvec lock by the caller is wrong, we
should use the more general interface of folio_lruvec_relock_irq() to
acquire the correct lruvec lock.

Link: https://lkml.kernel.org/r/20220621125658.64935-6-songmuchun@bytedance.com
Signed-off-by: Muchun Song <songmuchun@bytedance.com>
Acked-by: Johannes Weiner <hannes@cmpxchg.org>
Acked-by: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Michal Hocko <mhocko@kernel.org>
Cc: Michal Koutný <mkoutny@suse.com>
Cc: Shakeel Butt <shakeelb@google.com>
Cc: Waiman Long <longman@redhat.com>
Cc: Xiongchun Duan <duanxiongchun@bytedance.com>
