From: Jarkko Sakkinen <jarkko@kernel.org>
Subject: mailmap: add alias for jarkko@profian.com
Date: Tue, 7 Jun 2022 19:41:39 +0300

Add alias for patches that I contribute on behalf of Profian
(my current employer).

Link: https://lkml.kernel.org/r/20220607164140.1230876-1-jarkko@kernel.org
Signed-off-by: Jarkko Sakkinen <jarkko@kernel.org>
