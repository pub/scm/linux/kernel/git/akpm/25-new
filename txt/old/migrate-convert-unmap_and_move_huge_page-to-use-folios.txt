From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: migrate: convert unmap_and_move_huge_page() to use folios
Date: Fri, 2 Sep 2022 20:46:47 +0100

Saves several calls to compound_head() and removes a couple of uses of
page->lru.

Link: https://lkml.kernel.org/r/20220902194653.1739778-52-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
