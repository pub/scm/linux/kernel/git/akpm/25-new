From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: zram: do not leak page on recompress_store error path
Date: Sat, 22 Feb 2025 07:25:46 +0900

Ensure the page used for local object data is freed on error out path.

Link: https://lkml.kernel.org/r/20250221222958.2225035-16-senozhatsky@chromium.org
Fixes: 3f909a60cec1 ("zram: rework recompress target selection strategy")
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Cc: Minchan Kim <minchan@kernel.org>
Cc: Sebastian Andrzej Siewior <bigeasy@linutronix.de>
Cc: Yosry Ahmed <yosry.ahmed@linux.dev>
