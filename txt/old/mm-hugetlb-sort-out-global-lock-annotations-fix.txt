From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-hugetlb-sort-out-global-lock-annotations-fix
Date: Wed Aug 28 12:48:16 PM PDT 2024

move section directives to the end of the definitions, per convention

Cc: Davidlohr Bueso <dave@stgolabs.net>
Cc: Mateusz Guzik <mjguzik@gmail.com>
Cc: Muchun Song <muchun.song@linux.dev>
