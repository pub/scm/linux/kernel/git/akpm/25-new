From: Peng Zhang <zhangpeng.00@bytedance.com>
Subject: maple_tree: add test for expanding range in RCU mode
Date: Wed, 28 Jun 2023 15:36:55 +0800

Add test for expanding range in RCU mode. If we use the fast path of the
slot store to expand range in RCU mode, this test will fail.

Link: https://lkml.kernel.org/r/20230628073657.75314-3-zhangpeng.00@bytedance.com
Signed-off-by: Peng Zhang <zhangpeng.00@bytedance.com>
Reviewed-by: Liam R. Howlett <Liam.Howlett@oracle.com>
