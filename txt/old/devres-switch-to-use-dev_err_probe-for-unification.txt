From: Andy Shevchenko <andriy.shevchenko@linux.intel.com>
Subject: devres: switch to use dev_err_probe() for unification
Date: Wed, 3 Apr 2024 13:46:56 +0300

Patch series "devres: A couple of cleanups".

A couple of ad-hoc cleanups. No functional changes intended. 


This patch (of 2):

The devm_*() APIs are supposed to be called during the ->probe() stage. 
Many drivers (especially new ones) have switched to use dev_err_probe()
for error messaging for the sake of unification.  Let's do the same in the
devres APIs.

Link: https://lkml.kernel.org/r/20240403104820.557487-1-andriy.shevchenko@linux.intel.com
Link: https://lkml.kernel.org/r/20240403104820.557487-2-andriy.shevchenko@linux.intel.com
Signed-off-by: Andy Shevchenko <andriy.shevchenko@linux.intel.com>
Reviewed-by: Philipp Stanner <pstanner@redhat.com>
