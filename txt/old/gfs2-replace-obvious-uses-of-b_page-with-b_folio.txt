From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: gfs2: replace obvious uses of b_page with b_folio
Date: Thu, 15 Dec 2022 21:43:58 +0000

These places just use b_page to get to the buffer's address_space.

Link: https://lkml.kernel.org/r/20221215214402.3522366-9-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: Jan Kara <jack@suse.cz>
