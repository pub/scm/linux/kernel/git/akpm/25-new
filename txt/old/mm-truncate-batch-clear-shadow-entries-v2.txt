From: Yu Zhao <yuzhao@google.com>
Subject: mm-truncate-batch-clear-shadow-entries-v2
Date: Wed, 10 Jul 2024 00:09:33 -0600

restore comment, rename clear_shadow_entry() to clear_shadow_entries()

Link: https://lkml.kernel.org/r/20240710060933.3979380-1-yuzhao@google.com
Reported-by: Bharata B Rao <bharata@amd.com>
Closes: https://lore.kernel.org/d2841226-e27b-4d3d-a578-63587a3aa4f3@amd.com/
Tested-by: Bharata B Rao <bharata@amd.com>
Signed-off-by: Yu Zhao <yuzhao@google.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Mel Gorman <mgorman@techsingularity.net>
