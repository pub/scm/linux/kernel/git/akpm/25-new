From: Andrew Morton <akpm@linux-foundation.org>
Subject: mm-memcontrol-deprecate-charge-moving-fix
Date: Sun Dec 18 04:51:34 PM PST 2022

fix memory.rst underlining

Reported-by: Stephen Rothwell <sfr@canb.auug.org.au>
Cc: Johannes Weiner <hannes@cmpxchg.org>
