From: SeongJae Park <sj@kernel.org>
Subject: Docs/admin-guide/mm/damon/usage: fix a typo on a reference link
Date: Wed, 21 Feb 2024 09:08:51 -0800

There was a typo cuasing a document build warning below.  Fix it.

    WARNING: undefined label: 'damon_design_confiurable_operations_set'

Link: https://lkml.kernel.org/r/20240221170852.55529-2-sj@kernel.org
Fixes: afc858f0e6db ("Docs/mm/damon: move DAMON operation sets list from the usage to the design document")
Signed-off-by: SeongJae Park <sj@kernel.org>
