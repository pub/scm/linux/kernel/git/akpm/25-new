From: SeongJae Park <sj@kernel.org>
Subject: mm/damon/lru_sort: use stat generator
Date: Tue, 13 Sep 2022 17:44:44 +0000

This commit makes DAMON_LRU_SORT to generate the module parameters for
DAMOS statistics using the generator macro to simplify the code and reduce
duplicates.

Link: https://lkml.kernel.org/r/20220913174449.50645-18-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
