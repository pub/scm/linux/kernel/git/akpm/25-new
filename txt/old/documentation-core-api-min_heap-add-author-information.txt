From: Kuan-Wei Chiu <visitorckw@gmail.com>
Subject: Documentation/core-api: min_heap: add author information
Date: Sat, 30 Nov 2024 02:12:22 +0800

As with other documentation files, author information is added to
min_heap.rst, providing contact details for any questions regarding the
Min Heap API or the document itself.

Link: https://lkml.kernel.org/r/20241129181222.646855-5-visitorckw@gmail.com
Signed-off-by: Kuan-Wei Chiu <visitorckw@gmail.com>
Cc: Ching-Chun (Jim) Huang <jserv@ccns.ncku.edu.tw>
Cc: Geert Uytterhoeven <geert@linux-m68k.org>
Cc: Jonathan Corbet <corbet@lwn.net>
