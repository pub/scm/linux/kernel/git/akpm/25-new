From: Kunwu Chan <chentao@kylinos.cn>
Subject: nilfs2: remove duplicate 'unlikely()' usage
Date: Wed, 4 Sep 2024 19:16:03 +0900

Nested unlikely() calls, IS_ERR already uses unlikely() internally

Link: https://lkml.kernel.org/r/20240904101618.17716-1-konishi.ryusuke@gmail.com
Signed-off-by: Kunwu Chan <chentao@kylinos.cn>
Signed-off-by: Ryusuke Konishi <konishi.ryusuke@gmail.com>
