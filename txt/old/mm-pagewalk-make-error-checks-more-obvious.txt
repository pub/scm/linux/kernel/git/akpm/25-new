From: Rolf Eike Beer <eb@emlix.com>
Subject: mm: pagewalk: make error checks more obvious
Date: Mon, 22 Aug 2022 15:00:05 +0200

Patch series "Minor improvements for pagewalk code".

For some project I had to use the pagewalk API for certain things and
during this have read through the code multiple times.  Our usage has
changed several times depending on our current state of research as well.

During all of this I have made some tweaks to the code to be able to follow it 
better when hunting my own problems, and not call into some things that I 
actually don't need. The patches are more or less independent of each other. 


This patch (of 6):

The err variable only needs to be checked when it was assigned directly
before, it is not carried on to any later checks.  Move the checks into
the same "if" conditions where they are assigned.  Also just return the
error at the relevant places.  While at it move these err variables to a
more local scope at some places.

[eb@emlix.com: add back missing variable initializations]
  Link: https://lkml.kernel.org/r/2123960.ggj6I0NvhH@mobilepool36.emlix.com
  Link: https://lkml.kernel.org/r/2123960.ggj6I0NvhH@mobilepool36.emlix.com
Link: https://lkml.kernel.org/r/3200642.44csPzL39Z@devpool047
Link: https://lkml.kernel.org/r/2203731.iZASKD2KPV@devpool047
Signed-off-by: Rolf Eike Beer <eb@emlix.com>
