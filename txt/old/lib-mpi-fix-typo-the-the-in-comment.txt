From: Slark Xiao <slark_xiao@163.com>
Subject: lib/mpi: fix typo 'the the' in comment
Date: Fri, 22 Jul 2022 18:19:22 +0800

Replace 'the the' with 'the' in the comment.

Link: https://lkml.kernel.org/r/20220722101922.81126-1-slark_xiao@163.com
Signed-off-by: Slark Xiao <slark_xiao@163.com>
Cc: Hongbo Li <herberthbli@tencent.com>
Cc: Herbert Xu <herbert@gondor.apana.org.au>
