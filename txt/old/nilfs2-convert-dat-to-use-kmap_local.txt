From: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Subject: nilfs2: convert DAT to use kmap_local
Date: Mon, 22 Jan 2024 23:01:54 +0900

Concerning the code of the metadata file DAT for disk address translation,
convert all parts that use the deprecated kmap_atomic to use kmap_local. 
All transformations are directly possible.

Link: https://lkml.kernel.org/r/20240122140202.6950-8-konishi.ryusuke@gmail.com
Signed-off-by: Ryusuke Konishi <konishi.ryusuke@gmail.com>
