From: Thomas Gleixner <tglx@linutronix.de>
Subject: MAINTAINERS: add entry for debug objects
Date: Wed, 07 Jun 2023 01:14:05 +0200

This is overdue and an oversight.

Add myself to this file deespite the fact that I'm trying to reduce the
number of entries in this file which have my name attached, but in the
hope that patches wont get picked up elsewhere completely unreviewed and
unnoticed.

Link: https://lkml.kernel.org/r/87v8g0ji9u.ffs@tglx
Signed-off-by: Thomas Gleixner <tglx@linutronix.de>
