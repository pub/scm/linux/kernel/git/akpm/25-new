From: SeongJae Park <sj@kernel.org>
Subject: mm/damon/reclaim: use the quota params generator macro
Date: Tue, 13 Sep 2022 17:44:47 +0000

This commit makes DAMON_RECLAIM to generate the module parameters for
DAMOS quotas using the generator macro to simplify the code and reduce
duplicates.

Link: https://lkml.kernel.org/r/20220913174449.50645-21-sj@kernel.org
Signed-off-by: SeongJae Park <sj@kernel.org>
