From: Sergey Senozhatsky <senozhatsky@chromium.org>
Subject: Docs/ABI/zram: document zram recompress sysfs knobs
Date: Tue, 15 Nov 2022 11:03:14 +0900

Document zram re-compression sysfs knobs.

Link: https://lkml.kernel.org/r/20221115020314.386235-1-senozhatsky@chromium.org
Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Acked-by: Minchan Kim <minchan@kernel.org>
Cc: Nitin Gupta <ngupta@vflare.org>
