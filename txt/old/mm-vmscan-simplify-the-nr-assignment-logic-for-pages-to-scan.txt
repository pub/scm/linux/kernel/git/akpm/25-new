From: Chao Xu <amos.xuchao@gmail.com>
Subject: mm/vmscan: simplify the nr assignment logic for pages to scan
Date: Thu, 10 Nov 2022 19:31:30 +0800

By default the assignment logic of anonymouns or file inactive pages and
active pages to scan using the same duplicated code snippet.  To simplify
the logic, merge the same part.

Link: https://lkml.kernel.org/r/20221110113130.284290-1-Chao.Xu9@zeekrlife.com
Signed-off-by: Chao Xu <Chao.Xu9@zeekrlife.com>
