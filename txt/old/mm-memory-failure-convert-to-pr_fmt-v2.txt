From: Kefeng Wang <wangkefeng.wang@huawei.com>
Subject: mm-memory-failure-convert-to-pr_fmt-v2
Date: Wed, 27 Jul 2022 11:25:11 +0800

Link: https://lkml.kernel.org/r/20220727032511.145506-1-wangkefeng.wang@huawei.com
Signed-off-by: Kefeng Wang <wangkefeng.wang@huawei.com>
Acked-by: Naoya Horiguchi <naoya.horiguchi@nec.com>
Reviewed-by: Miaohe Lin <linmiaohe@huawei.com>
