From: Dan Williams <dan.j.williams@intel.com>
Subject: mm/memremap_pages: replace zone_device_page_init() with pgmap_request_folios()
Date: Fri, 14 Oct 2022 16:59:06 -0700

Switch to the common method, shared across all MEMORY_DEVICE_* types, for
requesting access to a ZONE_DEVICE page.  The
MEMORY_DEVICE_{PRIVATE,COHERENT} specific expectation that newly requested
pages are locked is moved to the callers.

[akpm@linux-foundation.org: restore removal of zone_device_page_init]
Link: https://lkml.kernel.org/r/166579194621.2236710.8168919102434295671.stgit@dwillia2-xfh.jf.intel.com
Signed-off-by: Dan Williams <dan.j.williams@intel.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Jan Kara <jack@suse.cz>
Cc: "Darrick J. Wong" <djwong@kernel.org>
Cc: Christoph Hellwig <hch@lst.de>
Cc: John Hubbard <jhubbard@nvidia.com>
Cc: Alistair Popple <apopple@nvidia.com>
Cc: Jason Gunthorpe <jgg@nvidia.com>
Cc: Felix Kuehling <Felix.Kuehling@amd.com>
Cc: Alex Deucher <alexander.deucher@amd.com>
Cc: "Christian König" <christian.koenig@amd.com>
Cc: "Pan, Xinhui" <Xinhui.Pan@amd.com>
Cc: David Airlie <airlied@linux.ie>
Cc: Daniel Vetter <daniel@ffwll.ch>
Cc: Ben Skeggs <bskeggs@redhat.com>
Cc: Karol Herbst <kherbst@redhat.com>
Cc: Lyude Paul <lyude@redhat.com>
Cc: "Jérôme Glisse" <jglisse@redhat.com>
Cc: Dave Chinner <david@fromorbit.com>
Cc: kernel test robot <lkp@intel.com>
