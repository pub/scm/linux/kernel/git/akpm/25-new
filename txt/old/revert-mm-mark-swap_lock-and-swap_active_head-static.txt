From: Liu Shixin <liushixin2@huawei.com>
Subject: Revert "mm: mark swap_lock and swap_active_head static"
Date: Mon, 29 Aug 2022 21:22:58 +0800

Patch series "Delay the initializaton of zswap", v4.

In the initialization of zswap, about 18MB memory will be allocated for
zswap_pool.  Since not all users use zswap, the memory may be wasted. 
Save the memory for these users by delaying the initialization of zswap to
first enablement.  

This patch (of 5):

This reverts commit 633423a09cb5cfe61438283e1ce49c23cf4a0611.

swap_lock and swap_active_head will be used in the next patch, so export
them again.

Link: https://lkml.kernel.org/r/20220829132302.3367054-3-liushixin2@huawei.com
Signed-off-by: Liu Shixin <liushixin2@huawei.com>
Cc: Christoph Hellwig <hch@lst.de>
Cc: Dan Streetman <ddstreet@ieee.org>
Cc: Kefeng Wang <wangkefeng.wang@huawei.com>
Cc: Konrad Rzeszutek Wilk <konrad.wilk@oracle.com>
Cc: Nathan Chancellor <nathan@kernel.org>
Cc: Seth Jennings <sjenning@redhat.com>
Cc: Vitaly Wool <vitaly.wool@konsulko.com>
