From: Kaiyang Zhao <kaiyang2@cs.cmu.edu>
Subject: mm: print the promo watermark in zoneinfo
Date: Thu, 1 Aug 2024 23:25:48 +0000

Print the promo watermark in zoneinfo just like other watermarks.  This
helps users check and verify all the watermarks are appropriate.

Link: https://lkml.kernel.org/r/20240801232548.36604-3-kaiyang2@cs.cmu.edu
Signed-off-by: Kaiyang Zhao <kaiyang2@cs.cmu.edu>
Cc: Johannes Weiner <hannes@cmpxchg.org>
