From: Punit Agrawal <punit.agrawal@bytedance.com>
Subject: selftests: proc: mark proc-pid-vm as x86_64 only
Date: Wed, 9 Nov 2022 22:11:04 +0000

The proc-pid-vm test does not have support for architectures other than
x86_64.  Mark it as such in the Makefile and in the process remove the
special casing in the test itself.

Link: https://lkml.kernel.org/r/20221109221104.1797802-2-punit.agrawal@bytedance.com
Signed-off-by: Punit Agrawal <punit.agrawal@bytedance.com>
Cc: Alexey Dobriyan <adobriyan@gmail.com>
Cc: Shuah Khan <shuah@kernel.org>
