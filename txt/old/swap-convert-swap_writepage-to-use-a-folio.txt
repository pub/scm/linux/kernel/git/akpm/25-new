From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: swap: convert swap_writepage() to use a folio
Date: Fri, 2 Sep 2022 20:46:36 +0100

Removes many calls to compound_head().

Link: https://lkml.kernel.org/r/20220902194653.1739778-41-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
