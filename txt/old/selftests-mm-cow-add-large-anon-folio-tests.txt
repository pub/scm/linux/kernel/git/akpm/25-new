From: Ryan Roberts <ryan.roberts@arm.com>
Subject: selftests/mm/cow: add large anon folio tests
Date: Thu, 10 Aug 2023 15:29:42 +0100

Add tests similar to the existing THP tests, but which operate on memory
backed by large anonymous folios, which are smaller than THP.

This reuses all the existing infrastructure.  If the test suite detects
that large anonyomous folios are not supported by the kernel, the new
tests are skipped.

Link: https://lkml.kernel.org/r/20230810142942.3169679-6-ryan.roberts@arm.com
Signed-off-by: Ryan Roberts <ryan.roberts@arm.com>
Cc: Anshuman Khandual <anshuman.khandual@arm.com>
Cc: Catalin Marinas <catalin.marinas@arm.com>
Cc: David Hildenbrand <david@redhat.com>
Cc: Huang, Ying <ying.huang@intel.com>
Cc: Itaru Kitayama <itaru.kitayama@gmail.com>
Cc: Kirill A. Shutemov <kirill.shutemov@linux.intel.com>
Cc: Luis Chamberlain <mcgrof@kernel.org>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Yang Shi <shy828301@gmail.com>
Cc: Yin Fengwei <fengwei.yin@intel.com>
Cc: Yu Zhao <yuzhao@google.com>
Cc: Zi Yan <ziy@nvidia.com>
