From: Qi Zheng <zhengqi.arch@bytedance.com>
Subject: mm: pgtable: add missing flag and statistics for kernel PTE page
Date: Thu, 1 Feb 2024 16:05:40 +0800

For kernel PTE page, we do not need to allocate and initialize its split
ptlock, but as a page table page, it's still necessary to add PG_table
flag and NR_PAGETABLE statistics for it.

Link: https://lkml.kernel.org/r/f023a6687b9f2109401e7522b727aa4708dc05f1.1706774109.git.zhengqi.arch@bytedance.com
Signed-off-by: Qi Zheng <zhengqi.arch@bytedance.com>
Reviewed-by: Muchun Song <muchun.song@linux.dev>
Cc: David Hildenbrand <david@redhat.com>
Cc: Matthew Wilcox <willy@infradead.org>
