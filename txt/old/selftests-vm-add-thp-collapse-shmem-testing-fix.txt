From: "Zach O'Keefe" <zokeefe@google.com>
Subject: selftests/vm: fix "add thp collapse file and tmpfs testing" for tmpfs
Date: Tue, 13 Sep 2022 14:25:17 -0700

In write_settings() and save_settings() we properly only read/write
/sys/block/<device>/queue/read_ahead_kb if the test file is not backed by
tmpfs.  However, in get_finfo(), we unconditionally attempt to resolve the
major/minor device pair and search for device interface, which we fail to
do when the device is the null device (major 0), as is the case when
stat(2)'ing a tmpfs-backed file.

Only resolve path to queue/read_ahead_kb when dealing with non-tmpfs
files.

Link: https://lkml.kernel.org/r/20220913212517.3163701-1-zokeefe@google.com
Fixes: 64b9fb1fa904 ("selftests/vm: add thp collapse file and tmpfs testing")
Signed-off-by: Zach O'Keefe <zokeefe@google.com>
