From: Sidhartha Kumar <sidhartha.kumar@oracle.com>
Subject: mm-move-folio_set_compound_order-to-mm-internalh-update
Date: Tue, 13 Dec 2022 13:20:53 -0800

alter the folio_set_order() warning

Signed-off-by: Sidhartha Kumar <sidhartha.kumar@oracle.com>
Suggested-by: Mike Kravetz <mike.kravetz@oracle.com>
Suggested-by: Muchun Song <songmuchun@bytedance.com>
Suggested-by: Matthew Wilcox <willy@infradead.org>
Suggested-by: John Hubbard <jhubbard@nvidia.com>
