From: Nikolay Borisov <nborisov@suse.com>
Subject: scripts/bloat-o-meter: switch argument parsing to using argparse
Date: Fri, 1 Jul 2022 14:35:12 +0300

This will facilitate further extension to the arguments the script takes. 
As an added benefit it also produces saner usage output, where mutual
exclusivity of the c|d|t parameters is clearly visible:

./scripts/bloat-o-meter  -h
usage: bloat-o-meter [-h] [-c | -d | -t] file1 file2

Simple script used to compare the symbol sizes of 2 object files

positional arguments:
  file1       First file to compare
  file2       Second file to compare

optional arguments:
  -h, --help  show this help message and exit
  -c          categorize output based on symbol type
  -d          Show delta of Data Section
  -t          Show delta of text Section

Link: https://lkml.kernel.org/r/20220701113513.1938008-1-nborisov@suse.com
Signed-off-by: Nikolay Borisov <nborisov@suse.com>
