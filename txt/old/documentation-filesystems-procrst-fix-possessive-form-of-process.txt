From: Andrew Morton <akpm@linux-foundation.org>
Subject: Documentation/filesystems/proc.rst: fix possessive form of "process"
Date: Fri Jan 10 04:38:41 PM PST 2025

The possessive form of "process" is "process's".  Fix up various
misdirected attempts at this.  Also reflow some paragraphs.

Cc: David Hildenbrand <david@redhat.com>
Cc: Wang Yaxin <wang.yaxin@zte.com.cn>
Cc: xu xin <xu.xin16@zte.com.cn>
Cc: Yang Yang <yang.yang29@zte.com.cn>
