From: Dev Jain <dev.jain@arm.com>
Subject: selftests/mm: parse VMA range in one go
Date: Fri, 22 Mar 2024 17:35:51 +0530

Use sscanf() to directly parse the VMA range. No functional change is intended.

Link: https://lkml.kernel.org/r/20240322120551.818764-1-dev.jain@arm.com
Signed-off-by: Dev Jain <dev.jain@arm.com>
Cc: Anshuman Khandual <anshuman.khandual@arm.com>
Cc: Shuah Khan <shuah@kernel.org>
