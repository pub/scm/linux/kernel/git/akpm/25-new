From: Andrew Morton <akpm@linux-foundation.org>
Subject: ipc/msg.c: fix percpu_counter use after free
Date: Thu Oct 20 09:19:22 PM PDT 2022

These percpu counters are referenced in free_ipcs->freeque, so destroy
them later.

Fixes: 72d1e611082e ("ipc/msg: mitigate the lock contention with percpu counter")
Reported-by: syzbot+96e659d35b9d6b541152@syzkaller.appspotmail.com
Tested-by: Mark Rutland <mark.rutland@arm.com>
Cc: Jiebin Sun <jiebin.sun@intel.com>
