From: Andrew Morton <akpm@linux-foundation.org>
Subject: vmstat-add-pcp-remote-node-draining-via-cpu_vm_stats_fold-fix
Date: Mon Mar 20 03:28:50 PM PDT 2023

fix cpu_vm_stats_fold() stub

Cc: Marcelo Tosatti <mtosatti@redhat.com>
