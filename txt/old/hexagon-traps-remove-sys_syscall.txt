From: Nathan Chancellor <nathan@kernel.org>
Subject: hexagon: traps: remove sys_syscall()
Date: Thu, 30 Nov 2023 15:58:31 -0700

Clang warns:

  arch/hexagon/kernel/traps.c:335:6: warning: no previous prototype for function 'sys_syscall' [-Wmissing-prototypes]
    335 | long sys_syscall(void)
        |      ^
  arch/hexagon/kernel/traps.c:335:1: note: declare 'static' if the function is not intended to be used outside of this translation unit
    335 | long sys_syscall(void)
        | ^
        | static

This function is not used anywhere, so remove it.

Link: https://lkml.kernel.org/r/20231130-hexagon-missing-prototypes-v1-18-5c34714afe9e@kernel.org
Signed-off-by: Nathan Chancellor <nathan@kernel.org>
Cc: Arnd Bergmann <arnd@arndb.de>
Cc: Brian Cain <bcain@quicinc.com>
