From: Sidhartha Kumar <sidhartha.kumar@oracle.com>
Subject: maple_tree: move up mas_wr_store_setup() and mas_wr_prealloc_setup()
Date: Wed, 14 Aug 2024 12:19:30 -0400

Subsequent patches require these definitions to be higher, no functional
changes intended.

Link: https://lkml.kernel.org/r/20240814161944.55347-4-sidhartha.kumar@oracle.com
Signed-off-by: Sidhartha Kumar <sidhartha.kumar@oracle.com>
Reviewed-by: Liam R. Howlett <Liam.Howlett@oracle.com>
Cc: Matthew Wilcox (Oracle) <willy@infradead.org>
Cc: Suren Baghdasaryan <surenb@google.com>
