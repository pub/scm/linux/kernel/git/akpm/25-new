From: Konrad Dybcio <konrad.dybcio@linaro.org>
Subject: mailmap: map Rajendra Nayak's old address to his current one
Date: Mon, 13 Mar 2023 10:03:43 +0100

Rajendra's old email is still picked up by the likes of get_maintainer.pl
and keeps bouncing like all other @codeaurora.org addresses.  Map it to
his current one.

Link: https://lkml.kernel.org/r/20230313090343.2148346-1-konrad.dybcio@linaro.org
Signed-off-by: Konrad Dybcio <konrad.dybcio@linaro.org>
Cc: Rajendra Nayak <quic_rjendra@quicinc.com>
Cc: Andy Gross <agross@kernel.org>
Cc: Baolin Wang <baolin.wang@linux.alibaba.com>
Cc: Bjorn Andersson <andersson@kernel.org>
Cc: Colin Ian King <colin.i.king@gmail.com>
Cc: Jakub Kicinski <kuba@kernel.org>
Cc: Kirill Tkhai <tkhai@ya.ru>
Cc: Marijn Suijten <marijn.suijten@somainline.org>
Cc: Qais Yousef <qyousef@layalina.io>
Cc: Stephen Hemminger <stephen@networkplumber.org>
Cc: Vasily Averin <vasily.averin@linux.dev>
