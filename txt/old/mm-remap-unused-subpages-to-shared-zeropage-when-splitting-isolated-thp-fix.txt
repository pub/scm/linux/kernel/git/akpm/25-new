From: Huan Yang <link@vivo.com>
Subject: mm: move the easily assessable conditions forward
Date: Thu, 15 Aug 2024 16:31:01 +0800

Currently try_to_map_unused_to_zeropage() tries to use shared zero page to
save some memory of sub page.

If forbidding zeropage there is no need to do anything rather than
attempting to assess wthether to use it afterwards.

Link: https://lkml.kernel.org/r/20240815083102.653820-1-link@vivo.com
Signed-off-by: Huan Yang <link@vivo.com>
Cc: Yu Zhao <yuzhao@google.com>
Cc: Alexander Zhu <alexlzhu@fb.com>
Cc: Andi Kleen <ak@linux.intel.com>
Cc: Barry Song <baohua@kernel.org>
Cc: David Hildenbrand <david@redhat.com>
Cc: Domenico Cerasuolo <cerasuolodomenico@gmail.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Kairui Song <ryncsn@gmail.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Mike Rapoport <rppt@kernel.org>
Cc: Rik van Riel <riel@surriel.com>
Cc: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Ryan Roberts <ryan.roberts@arm.com>
Cc: Shakeel Butt <shakeel.butt@linux.dev>
Cc: Shuang Zhai <zhais@google.com>
Cc: Usama Arif <usamaarif642@gmail.com>
