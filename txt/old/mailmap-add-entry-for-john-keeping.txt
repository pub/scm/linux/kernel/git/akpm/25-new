From: John Keeping <john@keeping.me.uk>
Subject: mailmap: add entry for John Keeping
Date: Wed, 31 May 2023 15:48:39 +0100

Map my corporate address to my personal one, as I am leaving the
company.

Link: https://lkml.kernel.org/r/20230531144839.1157112-1-john@keeping.me.uk
Signed-off-by: John Keeping <john@keeping.me.uk>
