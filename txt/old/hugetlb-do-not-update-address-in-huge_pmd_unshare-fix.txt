From: Stephen Rothwell <sfr@canb.auug.org.au>
Subject: hugetlb: fix an unused variable warning/error
Date: Wed, 22 Jun 2022 17:04:46 +1000

Link: https://lkml.kernel.org/r/20220622171117.70850960@canb.auug.org.au
Signed-off-by: Stephen Rothwell <sfr@canb.auug.org.au>
Cc: Mike Kravetz <mike.kravetz@oracle.com>
