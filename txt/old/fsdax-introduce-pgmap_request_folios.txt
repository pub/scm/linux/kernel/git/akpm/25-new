From: Dan Williams <dan.j.williams@intel.com>
Subject: fsdax: introduce pgmap_request_folios()
Date: Fri, 14 Oct 2022 16:57:55 -0700

The next step in sanitizing DAX page and pgmap lifetime is to take page
references when a pgmap user maps a page or otherwise puts it into use. 
Unlike the page allocator where the it picks the page/folio, ZONE_DEVICE
users know in advance which folio they want to access.  Additionally,
ZONE_DEVICE implementations know when the pgmap is alive.  Introduce
pgmap_request_folios() that pins @nr_folios folios at a time provided they
are contiguous and of the same folio_order().

Some WARN assertions are added to document expectations and catch bugs in
future kernel work, like a potential conversion of fsdax to use multi-page
folios, but they otherwise are not expected to fire.

Note that the paired pgmap_release_folios() implementation temporarily, in
this path, takes an @pgmap argument to drop pgmap references.  A follow-on
patch arranges for free_zone_device_page() to drop pgmap references in all
cases.  In other words, the intent is that only put_folio() (on each folio
requested pgmap_request_folio()) is needed to to undo
pgmap_request_folios().

The intent is that this also replaces zone_device_page_init(), but that
too requires some more preparatory reworks to unify the various
MEMORY_DEVICE_* types.

[nathan@kernel.org: mark folio_span_valid() as __maybe_unused]
  Link: https://lkml.kernel.org/r/20221018152645.3195108-1-nathan@kernel.org
Link: https://lkml.kernel.org/r/166579187573.2236710.10151157417629496558.stgit@dwillia2-xfh.jf.intel.com
Signed-off-by: Dan Williams <dan.j.williams@intel.com>
Suggested-by: Jason Gunthorpe <jgg@nvidia.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Jan Kara <jack@suse.cz>
Cc: "Darrick J. Wong" <djwong@kernel.org>
Cc: Christoph Hellwig <hch@lst.de>
Cc: John Hubbard <jhubbard@nvidia.com>
Cc: Alistair Popple <apopple@nvidia.com>
Cc: Alex Deucher <alexander.deucher@amd.com>
Cc: Ben Skeggs <bskeggs@redhat.com>
Cc: "Christian König" <christian.koenig@amd.com>
Cc: Daniel Vetter <daniel@ffwll.ch>
Cc: Dave Chinner <david@fromorbit.com>
Cc: David Airlie <airlied@linux.ie>
Cc: Felix Kuehling <Felix.Kuehling@amd.com>
Cc: Jerome Glisse <jglisse@redhat.com>
Cc: Karol Herbst <kherbst@redhat.com>
Cc: kernel test robot <lkp@intel.com>
Cc: Lyude Paul <lyude@redhat.com>
Cc: "Pan, Xinhui" <Xinhui.Pan@amd.com>
