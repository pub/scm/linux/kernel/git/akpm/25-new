From: Zhongkun He <hezhongkun.hzk@bytedance.com>
Subject: mm/z3fold: remove unneeded spinlock in z3fold_alloc
Date: Sun, 4 Feb 2024 21:15:43 +0800

The spinlock in z3fold_alloc() is used to protect page->lru, but now it
was removed in commit e774a7bc7f0ad ("mm: zswap: remove page reclaim logic
from z3fold"), so remove the spinlock too.

Link: https://lkml.kernel.org/r/20240204131543.1469661-1-hezhongkun.hzk@bytedance.com
Signed-off-by: Zhongkun He <hezhongkun.hzk@bytedance.com>
Cc: Domenico Cerasuolo <cerasuolodomenico@gmail.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
