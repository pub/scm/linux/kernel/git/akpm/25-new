From: Eric DeVolder <eric.devolder@oracle.com>
Subject: kexec: rename ARCH_HAS_KEXEC_PURGATORY
Date: Wed, 12 Jul 2023 12:15:45 -0400

The Kconfig refactor to consolidate KEXEC and CRASH options utilized
option names of the form ARCH_SUPPORTS_<option>. Thus rename the
ARCH_HAS_KEXEC_PURGATORY to ARCH_SUPPORTS_KEXEC_PURGATORY to follow
the same.

Link: https://lkml.kernel.org/r/20230712161545.87870-15-eric.devolder@oracle.com
Signed-off-by: Eric DeVolder <eric.devolder@oracle.com>
