From: Andrew Morton <akpm@linux-foundation.org>
Subject: checkpatch-check-return-of-git_commit_info-fix
Date: Mon Jan  6 06:16:23 PM PST 2025

s/12 chars of sha1/12+ chars of sha1/, per Jon

Link: https://lkml.kernel.org/r/87o70kt232.fsf@trenco.lwn.net
Cc: Andy Whitcroft <apw@canonical.com>
Cc: Dwaipayan Ray <dwaipayanray1@gmail.com>
Cc: Joe Perches <joe@perches.com>
Cc: Lukas Bulwahn <lukas.bulwahn@gmail.com>
Cc: Tamir Duberstein <tamird@gmail.com>
Cc: Jonathan Corbet <corbet@lwn.net>
