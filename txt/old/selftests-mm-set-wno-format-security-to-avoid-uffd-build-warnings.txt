From: John Hubbard <jhubbard@nvidia.com>
Subject: selftests/mm: set -Wno-format-security to avoid uffd build warnings
Date: Thu, 1 Jun 2023 18:33:53 -0700

The uffd_test_start() is perhaps a little too elaborate about how it
dispatches tests, leading to a clang warning that looks roughly like
this:

"uffd-unit-tests.c:1198:20: warning: format string is not a string literal
(potentially insecure) [-Wformat-security] ...note: treat the string as
an argument to avoid this.
    uffd_test_start(test_name);
"

However, it doesn't seem worth it to rewrite the way uffd_test_start()
works, given that these tests are already deeply unsafe to begin with.

Fix this by just disabling the compiler warning, but only for
uffd-unit-tests.

Link: https://lkml.kernel.org/r/20230602013358.900637-8-jhubbard@nvidia.com
Signed-off-by: John Hubbard <jhubbard@nvidia.com>
Cc: Mel Gorman <mgorman@techsingularity.net>
Cc: Mike Kravetz <mike.kravetz@oracle.com>
Cc: Johannes Weiner <hannes@cmpxchg.org>
Cc: David Hildenbrand <david@redhat.com>
Cc: Peter Xu <peterx@redhat.com>
