From: Markus Elfring <Markus.Elfring@web.de>
Subject: mm/mempolicy: fix exception handling in shared_policy_replace()
Date: Thu, 23 Mar 2023 18:30:48 +0100

The label “err_out” was used to jump to another pointer check despite of
the detail in the implementation of the function “shared_policy_replace”
that it was determined already that a corresponding variable contained a
null pointer because of a failed call of the function “kmem_cache_alloc”.

1. Use more appropriate labels instead.

2. The implementation of the function “mpol_put” contains a pointer check
   for its single input parameter.
   Thus delete a redundant check in the caller.


This issue was detected by using the Coccinelle software.

Fixes: 42288fe366c4f1ce7522bc9f27d0bc2a81c55264 ("mm: mempolicy: Convert shared_policy mutex to spinlock")
Signed-off-by: Markus Elfring <elfring@users.sourceforge.net>
Cc: KOSAKI Motohiro <kosaki.motohiro@jp.fujitsu.com>
Cc: Mel Gorman <mgorman@suse.de>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 mm/mempolicy.c |   11 +++++------
 1 file changed, 5 insertions(+), 6 deletions(-)

--- a/mm/mempolicy.c~mm-mempolicy-fix-exception-handling-in-shared_policy_replace
+++ a/mm/mempolicy.c
@@ -2736,13 +2736,12 @@ restart:
 		sp_insert(sp, new);
 	write_unlock(&sp->lock);
 	ret = 0;
+put_mpol:
+	mpol_put(mpol_new);
 
-err_out:
-	if (mpol_new)
-		mpol_put(mpol_new);
 	if (n_new)
 		kmem_cache_free(sn_cache, n_new);
-
+exit:
 	return ret;
 
 alloc_new:
@@ -2750,10 +2749,10 @@ alloc_new:
 	ret = -ENOMEM;
 	n_new = kmem_cache_alloc(sn_cache, GFP_KERNEL);
 	if (!n_new)
-		goto err_out;
+		goto exit;
 	mpol_new = kmem_cache_alloc(policy_cache, GFP_KERNEL);
 	if (!mpol_new)
-		goto err_out;
+		goto put_mpol;
 	atomic_set(&mpol_new->refcnt, 1);
 	goto restart;
 }
_
