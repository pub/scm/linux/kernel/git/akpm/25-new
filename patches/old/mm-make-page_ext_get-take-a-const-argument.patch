From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: mm: make page_ext_get() take a const argument
Date: Tue, 26 Mar 2024 17:10:25 +0000

In order to constify other functions, we need page_ext_get() to be const. 
This is no problem as lookup_page_ext() already takes a const argument.

Link: https://lkml.kernel.org/r/20240326171045.410737-4-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 include/linux/page_ext.h    |    4 ++--
 include/linux/pgalloc_tag.h |    2 --
 mm/page_ext.c               |    2 +-
 3 files changed, 3 insertions(+), 5 deletions(-)

--- a/include/linux/page_ext.h~mm-make-page_ext_get-take-a-const-argument
+++ a/include/linux/page_ext.h
@@ -77,7 +77,7 @@ static inline void page_ext_init(void)
 }
 #endif
 
-extern struct page_ext *page_ext_get(struct page *page);
+extern struct page_ext *page_ext_get(const struct page *page);
 extern void page_ext_put(struct page_ext *page_ext);
 
 static inline void *page_ext_data(struct page_ext *page_ext,
@@ -117,7 +117,7 @@ static inline void page_ext_init_flatmem
 {
 }
 
-static inline struct page_ext *page_ext_get(struct page *page)
+static inline struct page_ext *page_ext_get(const struct page *page)
 {
 	return NULL;
 }
--- a/include/linux/pgalloc_tag.h~mm-make-page_ext_get-take-a-const-argument
+++ a/include/linux/pgalloc_tag.h
@@ -12,8 +12,6 @@
 #include <linux/page_ext.h>
 
 extern struct page_ext_operations page_alloc_tagging_ops;
-extern struct page_ext *page_ext_get(struct page *page);
-extern void page_ext_put(struct page_ext *page_ext);
 
 static inline union codetag_ref *codetag_ref_from_page_ext(struct page_ext *page_ext)
 {
--- a/mm/page_ext.c~mm-make-page_ext_get-take-a-const-argument
+++ a/mm/page_ext.c
@@ -514,7 +514,7 @@ void __meminit pgdat_page_ext_init(struc
  * Context: Any context.  Caller may not sleep until they have called
  * page_ext_put().
  */
-struct page_ext *page_ext_get(struct page *page)
+struct page_ext *page_ext_get(const struct page *page)
 {
 	struct page_ext *page_ext;
 
_
