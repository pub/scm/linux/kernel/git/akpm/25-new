From: Samuel Holland <samuel.holland@sifive.com>
Subject: x86/fpu: fix asm/fpu/types.h include guard
Date: Fri, 29 Mar 2024 00:18:24 -0700

Patch series "Unified cross-architecture kernel-mode FPU API", v4.

This series unifies the kernel-mode FPU API across several architectures
by wrapping the existing functions (where needed) in consistently-named
functions placed in a consistent header location, with mostly the same
semantics: they can be called from preemptible or non-preemptible task
context, and are not assumed to be reentrant.  Architectures are also
expected to provide CFLAGS adjustments for compiling FPU-dependent code. 
For the moment, SIMD/vector units are out of scope for this common API.

This allows us to remove the ifdeffery and duplicated Makefile logic at
each FPU user.  It then implements the common API on RISC-V, and converts
a couple of users to the new API: the AMDGPU DRM driver, and the FPU self
test.

The underlying goal of this series is to allow using newer AMD GPUs (e.g. 
Navi) on RISC-V boards such as SiFive's HiFive Unmatched.  Those GPUs need
CONFIG_DRM_AMD_DC_FP to initialize, which requires kernel-mode FPU
support.


This patch (of 15):

The include guard should match the filename, or it will conflict with
the newly-added asm/fpu.h.

Link: https://lkml.kernel.org/r/20240329072441.591471-1-samuel.holland@sifive.com
Link: https://lkml.kernel.org/r/20240329072441.591471-10-samuel.holland@sifive.com
Signed-off-by: Samuel Holland <samuel.holland@sifive.com>
Acked-by: Dave Hansen <dave.hansen@linux.intel.com>
Acked-by: Christian König <christian.koenig@amd.com> 
Cc: Borislav Petkov (AMD) <bp@alien8.de>
Cc: Catalin Marinas <catalin.marinas@arm.com>
Cc: Christoph Hellwig <hch@lst.de>
Cc: Huacai Chen <chenhuacai@kernel.org>
Cc: Ingo Molnar <mingo@redhat.com>
Cc: Jonathan Corbet <corbet@lwn.net>
Cc: Masahiro Yamada <masahiroy@kernel.org>
Cc: Nathan Chancellor <nathan@kernel.org>
Cc: Nicolas Schier <nicolas@fjasle.eu>
Cc: Russell King <linux@armlinux.org.uk>
Cc: Thomas Gleixner <tglx@linutronix.de>
Cc: Will Deacon <will@kernel.org>
Cc: Alex Deucher <alexander.deucher@amd.com>
Cc: Michael Ellerman <mpe@ellerman.id.au>
Cc: Palmer Dabbelt <palmer@rivosinc.com>
Cc: WANG Xuerui <git@xen0n.name>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 arch/x86/include/asm/fpu/types.h |    6 +++---
 1 file changed, 3 insertions(+), 3 deletions(-)

--- a/arch/x86/include/asm/fpu/types.h~x86-fpu-fix-asm-fpu-typesh-include-guard
+++ a/arch/x86/include/asm/fpu/types.h
@@ -2,8 +2,8 @@
 /*
  * FPU data structures:
  */
-#ifndef _ASM_X86_FPU_H
-#define _ASM_X86_FPU_H
+#ifndef _ASM_X86_FPU_TYPES_H
+#define _ASM_X86_FPU_TYPES_H
 
 #include <asm/page_types.h>
 
@@ -596,4 +596,4 @@ struct fpu_state_config {
 /* FPU state configuration information */
 extern struct fpu_state_config fpu_kernel_cfg, fpu_user_cfg;
 
-#endif /* _ASM_X86_FPU_H */
+#endif /* _ASM_X86_FPU_TYPES_H */
_
