From: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Subject: nilfs2: describe the members of nilfs_bmap_operations structure
Date: Fri, 16 Aug 2024 16:43:17 +0900

Add missing member variable descriptions in the kernel-doc comments for
the nilfs_bmap_operations structure, hiding the internal operations with
the "private:" tag.  This eliminates the following warnings output by the
kernel-doc script:

 fs/nilfs2/bmap.h:74: warning: Function parameter or struct member
  'bop_lookup' not described in 'nilfs_bmap_operations'
 fs/nilfs2/bmap.h:74: warning: Function parameter or struct member
  'bop_lookup_contig' not described in 'nilfs_bmap_operations'
 ...
 fs/nilfs2/bmap.h:74: warning: Function parameter or struct member
  'bop_gather_data' not described in 'nilfs_bmap_operations'

Link: https://lkml.kernel.org/r/20240816074319.3253-7-konishi.ryusuke@gmail.com
Signed-off-by: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 fs/nilfs2/bmap.h |   15 ++++++++++++++-
 1 file changed, 14 insertions(+), 1 deletion(-)

--- a/fs/nilfs2/bmap.h~nilfs2-describe-the-members-of-nilfs_bmap_operations-structure
+++ a/fs/nilfs2/bmap.h
@@ -44,6 +44,19 @@ struct nilfs_bmap_stats {
 
 /**
  * struct nilfs_bmap_operations - bmap operation table
+ * @bop_lookup:               single block search operation
+ * @bop_lookup_contig:        consecutive block search operation
+ * @bop_insert:               block insertion operation
+ * @bop_delete:               block delete operation
+ * @bop_clear:                block mapping resource release operation
+ * @bop_propagate:            operation to propagate dirty state towards the
+ *                            mapping root
+ * @bop_lookup_dirty_buffers: operation to collect dirty block buffers
+ * @bop_assign:               disk block address assignment operation
+ * @bop_mark:                 operation to mark in-use blocks as dirty for
+ *                            relocation by GC
+ * @bop_seek_key:             find valid block key operation
+ * @bop_last_key:             find last valid block key operation
  */
 struct nilfs_bmap_operations {
 	int (*bop_lookup)(const struct nilfs_bmap *, __u64, int, __u64 *);
@@ -66,7 +79,7 @@ struct nilfs_bmap_operations {
 	int (*bop_seek_key)(const struct nilfs_bmap *, __u64, __u64 *);
 	int (*bop_last_key)(const struct nilfs_bmap *, __u64 *);
 
-	/* The following functions are internal use only. */
+	/* private: internal use only */
 	int (*bop_check_insert)(const struct nilfs_bmap *, __u64);
 	int (*bop_check_delete)(struct nilfs_bmap *, __u64);
 	int (*bop_gather_data)(struct nilfs_bmap *, __u64 *, __u64 *, int);
_
