From: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Subject: nilfs2: add missing argument descriptions for ioctl-related helpers
Date: Fri, 16 Aug 2024 16:43:13 +0900

Add missing argument descriptions and return value information to the
kernel-doc comments for ioctl helper functions, and eliminate the
following warnings output by the kernel-doc script:

 fs/nilfs2/ioctl.c:120: warning: Function parameter or struct member
  'dentry' not described in 'nilfs_fileattr_get'
 fs/nilfs2/ioctl.c:120: warning: Function parameter or struct member 'fa'
  not described in 'nilfs_fileattr_get'
 fs/nilfs2/ioctl.c:133: warning: Function parameter or struct member 'idmap'
  not described in 'nilfs_fileattr_set'
 fs/nilfs2/ioctl.c:133: warning: Function parameter or struct member
  'dentry' not described in 'nilfs_fileattr_set'
 fs/nilfs2/ioctl.c:133: warning: Function parameter or struct member 'fa'
  not described in 'nilfs_fileattr_set'
 fs/nilfs2/ioctl.c:164: warning: Function parameter or struct member 'inode'
  not described in 'nilfs_ioctl_getversion'
 fs/nilfs2/ioctl.c:164: warning: Function parameter or struct member 'argp'
  not described in 'nilfs_ioctl_getversion'

Link: https://lkml.kernel.org/r/20240816074319.3253-3-konishi.ryusuke@gmail.com
Signed-off-by: Ryusuke Konishi <konishi.ryusuke@gmail.com>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 fs/nilfs2/ioctl.c |   17 +++++++++++++++--
 1 file changed, 15 insertions(+), 2 deletions(-)

--- a/fs/nilfs2/ioctl.c~nilfs2-add-missing-argument-descriptions-for-ioctl-related-helpers
+++ a/fs/nilfs2/ioctl.c
@@ -115,7 +115,11 @@ static int nilfs_ioctl_wrap_copy(struct
 }
 
 /**
- * nilfs_fileattr_get - ioctl to support lsattr
+ * nilfs_fileattr_get - retrieve miscellaneous file attributes
+ * @dentry: the object to retrieve from
+ * @fa:     fileattr pointer
+ *
+ * Return: always 0 as success.
  */
 int nilfs_fileattr_get(struct dentry *dentry, struct fileattr *fa)
 {
@@ -127,7 +131,12 @@ int nilfs_fileattr_get(struct dentry *de
 }
 
 /**
- * nilfs_fileattr_set - ioctl to support chattr
+ * nilfs_fileattr_set - change miscellaneous file attributes
+ * @idmap:  idmap of the mount
+ * @dentry: the object to change
+ * @fa:     fileattr pointer
+ *
+ * Return: 0 on success, or a negative error code on failure.
  */
 int nilfs_fileattr_set(struct mnt_idmap *idmap,
 		       struct dentry *dentry, struct fileattr *fa)
@@ -160,6 +169,10 @@ int nilfs_fileattr_set(struct mnt_idmap
 
 /**
  * nilfs_ioctl_getversion - get info about a file's version (generation number)
+ * @inode: inode object
+ * @argp:  userspace memory where the generation number of @inode is stored
+ *
+ * Return: 0 on success, or %-EFAULT on error.
  */
 static int nilfs_ioctl_getversion(struct inode *inode, void __user *argp)
 {
_
