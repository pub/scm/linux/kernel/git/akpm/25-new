From: Yosry Ahmed <yosryahmed@google.com>
Subject: x86/mm: delete unused cpu argument to leave_mm()
Date: Fri, 26 Jan 2024 08:06:43 +0000

The argument is unused since commit 3d28ebceaffa ("x86/mm: Rework lazy
TLB to track the actual loaded mm"), delete it.

Link: https://lkml.kernel.org/r/20240126080644.1714297-1-yosryahmed@google.com
Signed-off-by: Yosry Ahmed <yosryahmed@google.com>
Cc: Andy Lutomirski <luto@kernel.org>
Cc: Borislav Petkov (AMD) <bp@alien8.de>
Cc: Dave Hansen <dave.hansen@linux.intel.com>
Cc: Ingo Molnar <mingo@redhat.com>
Cc: Peter Zijlstra <peterz@infradead.org>
Cc: Thomas Gleixner <tglx@linutronix.de>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 arch/x86/include/asm/mmu.h    |    2 +-
 arch/x86/kernel/alternative.c |    2 +-
 arch/x86/mm/tlb.c             |    2 +-
 arch/x86/xen/mmu_pv.c         |    2 +-
 drivers/cpuidle/cpuidle.c     |    2 +-
 include/linux/mmu_context.h   |    2 +-
 6 files changed, 6 insertions(+), 6 deletions(-)

--- a/arch/x86/include/asm/mmu.h~x86-mm-delete-unused-cpu-argument-to-leave_mm
+++ a/arch/x86/include/asm/mmu.h
@@ -75,7 +75,7 @@ typedef struct {
 		.lock = __MUTEX_INITIALIZER(mm.context.lock),		\
 	}
 
-void leave_mm(int cpu);
+void leave_mm(void);
 #define leave_mm leave_mm
 
 #endif /* _ASM_X86_MMU_H */
--- a/arch/x86/kernel/alternative.c~x86-mm-delete-unused-cpu-argument-to-leave_mm
+++ a/arch/x86/kernel/alternative.c
@@ -1805,7 +1805,7 @@ static inline temp_mm_state_t use_tempor
 	 * restoring the previous mm.
 	 */
 	if (this_cpu_read(cpu_tlbstate_shared.is_lazy))
-		leave_mm(smp_processor_id());
+		leave_mm();
 
 	temp_state.mm = this_cpu_read(cpu_tlbstate.loaded_mm);
 	switch_mm_irqs_off(NULL, mm, current);
--- a/arch/x86/mm/tlb.c~x86-mm-delete-unused-cpu-argument-to-leave_mm
+++ a/arch/x86/mm/tlb.c
@@ -299,7 +299,7 @@ static void load_new_mm_cr3(pgd_t *pgdir
 	write_cr3(new_mm_cr3);
 }
 
-void leave_mm(int cpu)
+void leave_mm(void)
 {
 	struct mm_struct *loaded_mm = this_cpu_read(cpu_tlbstate.loaded_mm);
 
--- a/arch/x86/xen/mmu_pv.c~x86-mm-delete-unused-cpu-argument-to-leave_mm
+++ a/arch/x86/xen/mmu_pv.c
@@ -913,7 +913,7 @@ static void drop_mm_ref_this_cpu(void *i
 	struct mm_struct *mm = info;
 
 	if (this_cpu_read(cpu_tlbstate.loaded_mm) == mm)
-		leave_mm(smp_processor_id());
+		leave_mm();
 
 	/*
 	 * If this cpu still has a stale cr3 reference, then make sure
--- a/drivers/cpuidle/cpuidle.c~x86-mm-delete-unused-cpu-argument-to-leave_mm
+++ a/drivers/cpuidle/cpuidle.c
@@ -237,7 +237,7 @@ noinstr int cpuidle_enter_state(struct c
 	}
 
 	if (target_state->flags & CPUIDLE_FLAG_TLB_FLUSHED)
-		leave_mm(dev->cpu);
+		leave_mm();
 
 	/* Take note of the planned idle state. */
 	sched_idle_set_state(target_state);
--- a/include/linux/mmu_context.h~x86-mm-delete-unused-cpu-argument-to-leave_mm
+++ a/include/linux/mmu_context.h
@@ -11,7 +11,7 @@
 #endif
 
 #ifndef leave_mm
-static inline void leave_mm(int cpu) { }
+static inline void leave_mm(void) { }
 #endif
 
 /*
_
