From: Zi Yan <ziy@nvidia.com>
Subject: mm/huge_memory: do not drop the original folio during truncate
Date: Fri, 14 Feb 2025 16:18:24 -0500

The caller expects to handle the original folio itself.

Also make __split_unmapped_folio() never fail, per discussion with David
Hildenbrand.

Link: https://lkml.kernel.org/r/E2B8D781-01EE-4832-8587-4C5FF57F91B3@nvidia.com
Link: https://lore.kernel.org/all/67af65cb.050a0220.21dd3.004a.GAE@google.com/
Link: https://lore.kernel.org/linux-mm/db77d017-4a1e-4a47-9064-e335cb0313af@redhat.com/
Signed-off-by: Zi Yan <ziy@nvidia.com>
Reported-by: syzbot+012c6245eaea0e23f7f9@syzkaller.appspotmail.com
Tested-by: syzbot+012c6245eaea0e23f7f9@syzkaller.appspotmail.com
Cc: Baolin Wang <baolin.wang@linux.alibaba.com>
Cc: David Hildenbrand <david@redhat.com>
Cc: Hugh Dickins <hughd@google.com>
Cc: John Hubbard <jhubbard@nvidia.com>
Cc: Kefeng Wang <wangkefeng.wang@huawei.com>
Cc: Kirill A. Shuemov <kirill.shutemov@linux.intel.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Miaohe Lin <linmiaohe@huawei.com>
Cc: Ryan Roberts <ryan.roberts@arm.com>
Cc: Yang Shi <yang@os.amperecomputing.com>
Cc: Yu Zhao <yuzhao@google.com>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 mm/huge_memory.c |   22 ++++++----------------
 1 file changed, 6 insertions(+), 16 deletions(-)

--- a/mm/huge_memory.c~mm-huge_memory-add-two-new-not-yet-used-functions-for-folio_split-fix
+++ a/mm/huge_memory.c
@@ -3386,16 +3386,12 @@ bool can_split_folio(struct folio *folio
  * It splits @folio into @new_order folios and copies the @folio metadata to
  * all the resulting folios.
  */
-static int __split_folio_to_order(struct folio *folio, int new_order)
+static void __split_folio_to_order(struct folio *folio, int new_order)
 {
-	int curr_order = folio_order(folio);
 	long nr_pages = folio_nr_pages(folio);
 	long new_nr_pages = 1 << new_order;
 	long index;
 
-	if (curr_order <= new_order)
-		return -EINVAL;
-
 	/*
 	 * Skip the first new_nr_pages, since the new folio from them have all
 	 * the flags from the original folio.
@@ -3490,8 +3486,6 @@ static int __split_folio_to_order(struct
 
 	if (!new_order)
 		ClearPageCompound(&folio->page);
-
-	return 0;
 }
 
 /*
@@ -3585,7 +3579,6 @@ static int __split_unmapped_folio(struct
 		int old_order = folio_order(folio);
 		struct folio *release;
 		struct folio *end_folio = folio_next(folio);
-		int status;
 
 		/* order-1 anonymous folio is not supported */
 		if (folio_test_anon(folio) && split_order == 1)
@@ -3618,12 +3611,7 @@ static int __split_unmapped_folio(struct
 		split_page_owner(&folio->page, old_order, split_order);
 		pgalloc_tag_split(folio, old_order, split_order);
 
-		status = __split_folio_to_order(folio, split_order);
-
-		if (status < 0) {
-			stop_split = true;
-			ret = -EINVAL;
-		}
+		__split_folio_to_order(folio, split_order);
 
 after_split:
 		/*
@@ -3661,8 +3649,10 @@ after_split:
 				     folio_test_swapcache(origin_folio)) ?
 					     folio_nr_pages(release) : 0));
 
-			if (release != origin_folio)
-				lru_add_page_tail(origin_folio, &release->page,
+			if (release == origin_folio)
+				continue;
+			
+			lru_add_page_tail(origin_folio, &release->page,
 						lruvec, list);
 
 			/* Some pages can be beyond EOF: drop them from page cache */
_
