From: "Matthew Wilcox (Oracle)" <willy@infradead.org>
Subject: ocfs2: convert ocfs2_read_inline_data() to take a folio
Date: Thu, 5 Dec 2024 17:16:47 +0000

All callers now have a folio, so pass it in.  We can use folio_fill_tail()
instead of open-coding it.  Saves a call to compound_head().

Link: https://lkml.kernel.org/r/20241205171653.3179945-20-willy@infradead.org
Signed-off-by: Matthew Wilcox (Oracle) <willy@infradead.org>
Reviewed-by: Joseph Qi <joseph.qi@linux.alibaba.com>
Cc: Changwei Ge <gechangwei@live.cn>
Cc: Joel Becker <jlbec@evilplan.org>
Cc: Jun Piao <piaojun@huawei.com>
Cc: Junxiao Bi <junxiao.bi@oracle.com>
Cc: Mark Fasheh <mark@fasheh.com>
Cc: Mark Tinguely <mark.tinguely@oracle.com>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 fs/ocfs2/alloc.c |    2 +-
 fs/ocfs2/aops.c  |   20 ++++++--------------
 fs/ocfs2/aops.h  |    2 +-
 3 files changed, 8 insertions(+), 16 deletions(-)

--- a/fs/ocfs2/alloc.c~ocfs2-convert-ocfs2_read_inline_data-to-take-a-folio
+++ a/fs/ocfs2/alloc.c
@@ -7135,7 +7135,7 @@ int ocfs2_convert_inline_data_to_extents
 		 * This should populate the 1st page for us and mark
 		 * it up to date.
 		 */
-		ret = ocfs2_read_inline_data(inode, &folio->page, di_bh);
+		ret = ocfs2_read_inline_data(inode, folio, di_bh);
 		if (ret) {
 			mlog_errno(ret);
 			need_free = 1;
--- a/fs/ocfs2/aops.c~ocfs2-convert-ocfs2_read_inline_data-to-take-a-folio
+++ a/fs/ocfs2/aops.c
@@ -215,10 +215,9 @@ bail:
 	return err;
 }
 
-int ocfs2_read_inline_data(struct inode *inode, struct page *page,
+int ocfs2_read_inline_data(struct inode *inode, struct folio *folio,
 			   struct buffer_head *di_bh)
 {
-	void *kaddr;
 	loff_t size;
 	struct ocfs2_dinode *di = (struct ocfs2_dinode *)di_bh->b_data;
 
@@ -230,7 +229,7 @@ int ocfs2_read_inline_data(struct inode
 
 	size = i_size_read(inode);
 
-	if (size > PAGE_SIZE ||
+	if (size > folio_size(folio) ||
 	    size > ocfs2_max_inline_data_with_xattr(inode->i_sb, di)) {
 		ocfs2_error(inode->i_sb,
 			    "Inode %llu has with inline data has bad size: %Lu\n",
@@ -239,15 +238,8 @@ int ocfs2_read_inline_data(struct inode
 		return -EROFS;
 	}
 
-	kaddr = kmap_atomic(page);
-	if (size)
-		memcpy(kaddr, di->id2.i_data.id_data, size);
-	/* Clear the remaining part of the page */
-	memset(kaddr + size, 0, PAGE_SIZE - size);
-	flush_dcache_page(page);
-	kunmap_atomic(kaddr);
-
-	SetPageUptodate(page);
+	folio_fill_tail(folio, 0, di->id2.i_data.id_data, size);
+	folio_mark_uptodate(folio);
 
 	return 0;
 }
@@ -266,7 +258,7 @@ static int ocfs2_readpage_inline(struct
 		goto out;
 	}
 
-	ret = ocfs2_read_inline_data(inode, &folio->page, di_bh);
+	ret = ocfs2_read_inline_data(inode, folio, di_bh);
 out:
 	folio_unlock(folio);
 
@@ -1506,7 +1498,7 @@ static int ocfs2_write_begin_inline(stru
 		ocfs2_set_inode_data_inline(inode, di);
 
 	if (!folio_test_uptodate(folio)) {
-		ret = ocfs2_read_inline_data(inode, &folio->page, wc->w_di_bh);
+		ret = ocfs2_read_inline_data(inode, folio, wc->w_di_bh);
 		if (ret) {
 			ocfs2_commit_trans(osb, handle);
 
--- a/fs/ocfs2/aops.h~ocfs2-convert-ocfs2_read_inline_data-to-take-a-folio
+++ a/fs/ocfs2/aops.h
@@ -41,7 +41,7 @@ int ocfs2_write_begin_nolock(struct addr
 		struct folio **foliop, void **fsdata,
 		struct buffer_head *di_bh, struct folio *mmap_folio);
 
-int ocfs2_read_inline_data(struct inode *inode, struct page *page,
+int ocfs2_read_inline_data(struct inode *inode, struct folio *folio,
 			   struct buffer_head *di_bh);
 int ocfs2_size_fits_inline_data(struct buffer_head *di_bh, u64 new_size);
 
_
