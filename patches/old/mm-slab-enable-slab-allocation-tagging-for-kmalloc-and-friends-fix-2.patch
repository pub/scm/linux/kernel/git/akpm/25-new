From: Randy Dunlap <rdunlap@infradead.org>
Subject: mm/slab: fix kcalloc() kernel-doc warnings
Date: Tue, 26 Mar 2024 21:46:49 -0700

Fix the parameter names for kcalloc() in slab.h to prevent kernel-doc
warnings:

include/linux/slab.h:730: warning: Function parameter or struct member '_n' not described in 'kcalloc'
include/linux/slab.h:730: warning: Function parameter or struct member '_size' not described in 'kcalloc'
include/linux/slab.h:730: warning: Function parameter or struct member '_flags' not described in 'kcalloc'
include/linux/slab.h:730: warning: Excess function parameter 'n' description in 'kcalloc'
include/linux/slab.h:730: warning: Excess function parameter 'size' description in 'kcalloc'
include/linux/slab.h:730: warning: Excess function parameter 'flags' description in 'kcalloc'

Link: https://lkml.kernel.org/r/20240327044649.9199-1-rdunlap@infradead.org
Fixes: bc7b83f5e4bf ("mm/slab: enable slab allocation tagging for kmalloc and friends")
Signed-off-by: Randy Dunlap <rdunlap@infradead.org>
Reported-by: Stephen Rothwell <sfr@canb.auug.org.au>
Link: https://lore.kernel.org/linux-next/20240325123603.1bdd6588@canb.auug.org.au/
Tested-by: Suren Baghdasaryan <surenb@google.com>
Cc: Kent Overstreet <kent.overstreet@linux.dev>
Cc: Vlastimil Babka <vbabka@suse.cz>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 include/linux/slab.h |    2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

--- a/include/linux/slab.h~mm-slab-enable-slab-allocation-tagging-for-kmalloc-and-friends-fix-2
+++ a/include/linux/slab.h
@@ -727,7 +727,7 @@ static inline __realloc_size(2, 3) void
  * @size: element size.
  * @flags: the type of memory to allocate (see kmalloc).
  */
-#define kcalloc(_n, _size, _flags)		kmalloc_array(_n, _size, (_flags) | __GFP_ZERO)
+#define kcalloc(n, size, flags)		kmalloc_array(n, size, (flags) | __GFP_ZERO)
 
 void *kmalloc_node_track_caller_noprof(size_t size, gfp_t flags, int node,
 				  unsigned long caller) __alloc_size(1);
_
