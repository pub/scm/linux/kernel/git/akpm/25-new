From: Miaohe Lin <linmiaohe@huawei.com>
Subject: mm: reduce the rcu lock duration
Date: Mon, 30 May 2022 19:30:13 +0800

Patch series "A few cleanup and fixup patches for migration", v4.

This series contains a few patches to remove unneeded lock page and
PageMovable check, reduce the rcu lock duration.  Also we fix potential
pte_unmap on an not mapped pte.  More details can be found in the
respective changelogs.


This patch (of 4):

Commit 3268c63eded4 ("mm: fix move/migrate_pages() race on task struct")
extends the period of the rcu_read_lock until after the permissions checks
are done to prevent the task pointed to from changing from under us.  But
the task_struct refcount is also taken at that time, the reference to task
is guaranteed to be stable.  So it's unnecessary to extend the period of
the rcu_read_lock.  Release the rcu lock after task refcount is
successfully grabbed to reduce the rcu holding time.

Link: https://lkml.kernel.org/r/20220530113016.16663-1-linmiaohe@huawei.com
Link: https://lkml.kernel.org/r/20220530113016.16663-2-linmiaohe@huawei.com
Signed-off-by: Miaohe Lin <linmiaohe@huawei.com>
Reviewed-by: Muchun Song <songmuchun@bytedance.com>
Reviewed-by: Christoph Hellwig <hch@lst.de>
Reviewed-by: Oscar Salvador <osalvador@suse.de>
Reviewed-by: David Hildenbrand <david@redhat.com>
Cc: Huang Ying <ying.huang@intel.com>
Cc: David Howells <dhowells@redhat.com>
Cc: Christoph Lameter <cl@linux.com>
Cc: Peter Xu <peterx@redhat.com>
Cc: Alistair Popple <apopple@nvidia.com>
Cc: Mike Kravetz <mike.kravetz@oracle.com>
Cc: David Howells <dhowells@redhat.com>
Cc: kernel test robot <lkp@intel.com>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 mm/mempolicy.c |    3 +--
 mm/migrate.c   |    3 +--
 2 files changed, 2 insertions(+), 4 deletions(-)

--- a/mm/mempolicy.c~mm-reduce-the-rcu-lock-duration
+++ a/mm/mempolicy.c
@@ -1617,6 +1617,7 @@ static int kernel_migrate_pages(pid_t pi
 		goto out;
 	}
 	get_task_struct(task);
+	rcu_read_unlock();
 
 	err = -EINVAL;
 
@@ -1625,11 +1626,9 @@ static int kernel_migrate_pages(pid_t pi
 	 * Use the regular "ptrace_may_access()" checks.
 	 */
 	if (!ptrace_may_access(task, PTRACE_MODE_READ_REALCREDS)) {
-		rcu_read_unlock();
 		err = -EPERM;
 		goto out_put;
 	}
-	rcu_read_unlock();
 
 	task_nodes = cpuset_mems_allowed(task);
 	/* Is the user allowed to access the target nodes? */
--- a/mm/migrate.c~mm-reduce-the-rcu-lock-duration
+++ a/mm/migrate.c
@@ -1902,17 +1902,16 @@ static struct mm_struct *find_mm_struct(
 		return ERR_PTR(-ESRCH);
 	}
 	get_task_struct(task);
+	rcu_read_unlock();
 
 	/*
 	 * Check if this process has the right to modify the specified
 	 * process. Use the regular "ptrace_may_access()" checks.
 	 */
 	if (!ptrace_may_access(task, PTRACE_MODE_READ_REALCREDS)) {
-		rcu_read_unlock();
 		mm = ERR_PTR(-EPERM);
 		goto out;
 	}
-	rcu_read_unlock();
 
 	mm = ERR_PTR(security_task_movememory(task));
 	if (IS_ERR(mm))
_
