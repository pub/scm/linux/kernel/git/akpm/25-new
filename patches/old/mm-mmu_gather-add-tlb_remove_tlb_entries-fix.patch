From: Arnd Bergmann <arnd@kernel.org>
Subject: mm/mmu_gather: change __tlb_remove_tlb_entry() to an inline function
Date: Wed, 21 Feb 2024 16:45:21 +0100

From: Arnd Bergmann <arnd@arndb.de>

clang complains about tlb_remove_tlb_entries() not using the 'ptep' variable
when __tlb_remove_tlb_entry() is an empty macro:

include/asm-generic/tlb.h:627:10: error: parameter 'ptep' set but not used [-Werror,-Wunused-but-set-parameter]

Change it to an equivalent inline function that avoids the warning since
the compiler can see how the variable gets passed into it.

Link: https://lkml.kernel.org/r/20240221154549.2026073-1-arnd@kernel.org
Fixes: 66958b447695 ("mm/mmu_gather: add tlb_remove_tlb_entries()")
Signed-off-by: Arnd Bergmann <arnd@arndb.de>
Cc: David Hildenbrand <david@redhat.com>
Cc: Ryan Roberts <ryan.roberts@arm.com>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 include/asm-generic/tlb.h |    4 +++-
 1 file changed, 3 insertions(+), 1 deletion(-)

--- a/include/asm-generic/tlb.h~mm-mmu_gather-add-tlb_remove_tlb_entries-fix
+++ a/include/asm-generic/tlb.h
@@ -592,7 +592,9 @@ static inline void tlb_flush_p4d_range(s
 }
 
 #ifndef __tlb_remove_tlb_entry
-#define __tlb_remove_tlb_entry(tlb, ptep, address) do { } while (0)
+static inline void __tlb_remove_tlb_entry(struct mmu_gather *tlb, pte_t *ptep, unsigned long address)
+{
+}
 #endif
 
 /**
_
