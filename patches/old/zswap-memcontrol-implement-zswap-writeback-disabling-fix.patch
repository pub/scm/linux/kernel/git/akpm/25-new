From: Nhat Pham <nphamcs@gmail.com>
Subject: zswap: memcontrol: implement zswap writeback disabling (fix)
Date: Wed, 20 Dec 2023 16:57:25 -0800

Add a caveat about recurring zswap store failures leading to reclaim
inefficiency.

Link: https://lkml.kernel.org/r/20231221005725.3446672-1-nphamcs@gmail.com
Signed-off-by: Nhat Pham <nphamcs@gmail.com>
Suggested-by: Yosry Ahmed <yosryahmed@google.com>
Acked-by: Chris Li <chrisl@kernel.org>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 Documentation/admin-guide/cgroup-v2.rst |    5 ++++-
 Documentation/admin-guide/mm/zswap.rst  |    4 ++++
 2 files changed, 8 insertions(+), 1 deletion(-)

--- a/Documentation/admin-guide/cgroup-v2.rst~zswap-memcontrol-implement-zswap-writeback-disabling-fix
+++ a/Documentation/admin-guide/cgroup-v2.rst
@@ -1686,7 +1686,10 @@ PAGE_SIZE multiple when read back.
 
 	When this is set to 0, all swapping attempts to swapping devices
 	are disabled. This included both zswap writebacks, and swapping due
-	to zswap store failure.
+	to zswap store failures. If the zswap store failures are recurring
+	(for e.g if the pages are incompressible), users can observe
+	reclaim inefficiency after disabling writeback (because the same
+	pages might be rejected again and again).
 
 	Note that this is subtly different from setting memory.swap.max to
 	0, as it still allows for pages to be written to the zswap pool.
--- a/Documentation/admin-guide/mm/zswap.rst~zswap-memcontrol-implement-zswap-writeback-disabling-fix
+++ a/Documentation/admin-guide/mm/zswap.rst
@@ -159,6 +159,10 @@ zswap itself) on a cgroup-basis as follo
 
 	echo 0 > /sys/fs/cgroup/<cgroup-name>/memory.zswap.writeback
 
+Note that if the store failures are recurring (for e.g if the pages are
+incompressible), users can observe reclaim inefficiency after disabling
+writeback (because the same pages might be rejected again and again).
+
 When there is a sizable amount of cold memory residing in the zswap pool, it
 can be advantageous to proactively write these cold pages to swap and reclaim
 the memory for other use cases. By default, the zswap shrinker is disabled.
_
