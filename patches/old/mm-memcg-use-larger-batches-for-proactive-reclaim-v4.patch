From: "T.J. Mercier" <tjmercier@google.com>
Subject: mm-memcg-use-larger-batches-for-proactive-reclaim-v4
Date: Tue, 6 Feb 2024 17:52:50 +0000

Add additional info to commit message and move definition of batch_size
per Michal Hocko.  No functional changes.

Link: https://lkml.kernel.org/r/20240206175251.3364296-1-tjmercier@google.com
Fixes: 0388536ac291 ("mm:vmscan: fix inaccurate reclaim during proactive reclaim")
Signed-off-by: T.J. Mercier <tjmercier@google.com>
Reviewed-by: Yosry Ahmed <yosryahmed@google.com>
Acked-by: Johannes Weiner <hannes@cmpxchg.org>
Acked-by: Shakeel Butt <shakeelb@google.com>
Reviewed-by: Michal Koutny <mkoutny@suse.com>
Cc: Efly Young <yangyifei03@kuaishou.com>
Cc: Michal Hocko <mhocko@suse.com>
Cc: Muchun Song <songmuchun@bytedance.com>
Cc: Roman Gushchin <roman.gushchin@linux.dev>
Cc: Yu Zhao <yuzhao@google.com>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 mm/memcontrol.c |    5 ++---
 1 file changed, 2 insertions(+), 3 deletions(-)

--- a/mm/memcontrol.c~mm-memcg-use-larger-batches-for-proactive-reclaim-v4
+++ a/mm/memcontrol.c
@@ -6981,6 +6981,8 @@ static ssize_t memory_reclaim(struct ker
 
 	reclaim_options	= MEMCG_RECLAIM_MAY_SWAP | MEMCG_RECLAIM_PROACTIVE;
 	while (nr_reclaimed < nr_to_reclaim) {
+		/* Will converge on zero, but reclaim enforces a minimum */
+		unsigned long batch_size = (nr_to_reclaim - nr_reclaimed) / 4;
 		unsigned long reclaimed;
 
 		if (signal_pending(current))
@@ -6994,9 +6996,6 @@ static ssize_t memory_reclaim(struct ker
 		if (!nr_retries)
 			lru_add_drain_all();
 
-		/* Will converge on zero, but reclaim enforces a minimum */
-		unsigned long batch_size = (nr_to_reclaim - nr_reclaimed) / 4;
-
 		reclaimed = try_to_free_mem_cgroup_pages(memcg,
 					batch_size, GFP_KERNEL, reclaim_options);
 
_
