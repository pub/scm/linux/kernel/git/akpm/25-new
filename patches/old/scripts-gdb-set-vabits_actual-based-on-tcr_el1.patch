From: Kuan-Ying Lee <kuan-ying.lee@canonical.com>
Subject: scripts/gdb: set vabits_actual based on TCR_EL1
Date: Wed, 19 Jun 2024 15:49:09 +0800

We encounter the following issue after commit 9cce9c6c2c3b ("arm64: mm: Handle
LVA support as a CPU feature").

(gdb) lx-slabinfo
Python Exception <class 'gdb.error'>: No symbol "vabits_actual" in current context.
Error occurred in Python: No symbol "vabits_actual" in current context.

We set vabits_actual based on TCR_EL1 value when
VA_BITS is bigger than 48.

Link: https://lkml.kernel.org/r/20240619074911.100434-5-kuan-ying.lee@canonical.com
Fixes: 9cce9c6c2c3b ("arm64: mm: Handle LVA support as a CPU feature")
Signed-off-by: Kuan-Ying Lee <kuan-ying.lee@canonical.com>
Cc: Jan Kiszka <jan.kiszka@siemens.com>
Cc: Kieran Bingham <kbingham@kernel.org>
Cc: Kirill A. Shutemov <kirill.shutemov@linux.intel.com>
Cc: Michael Ellerman <mpe@ellerman.id.au>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 scripts/gdb/linux/mm.py |    4 +++-
 1 file changed, 3 insertions(+), 1 deletion(-)

--- a/scripts/gdb/linux/mm.py~scripts-gdb-set-vabits_actual-based-on-tcr_el1
+++ a/scripts/gdb/linux/mm.py
@@ -48,7 +48,9 @@ class aarch64_page_ops():
         self.VA_BITS = constants.LX_CONFIG_ARM64_VA_BITS
         if self.VA_BITS > 48:
             self.VA_BITS_MIN = 48
-            self.vabits_actual = gdb.parse_and_eval('vabits_actual')
+            tcr_el1 = gdb.execute("info registers $TCR_EL1", to_string=True)
+            tcr_el1 = int(tcr_el1.split()[1], 16)
+            self.vabits_actual = 64 - ((tcr_el1 >> 16) & 63)
         else:
             self.VA_BITS_MIN = self.VA_BITS
             self.vabits_actual = self.VA_BITS
_
