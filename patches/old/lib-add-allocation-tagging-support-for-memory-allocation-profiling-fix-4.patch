From: Suren Baghdasaryan <surenb@google.com>
Subject: lib: fix alloc_tag_init() to prevent passing NULL to PTR_ERR()
Date: Tue, 16 Apr 2024 17:33:49 -0700

codetag_register_type() never returns NULL, yet IS_ERR_OR_NULL() is used
to check its return value.  This leads to a warning about possibility of
passing NULL to PTR_ERR().  Fix that by using IS_ERR() to exclude NULL.

Link: https://lkml.kernel.org/r/20240417003349.2520094-1-surenb@google.com
Fixes: 6e8a230a6b1a ("lib: add allocation tagging support for memory allocation profiling")
Signed-off-by: Suren Baghdasaryan <surenb@google.com>
Reported-by: kernel test robot <lkp@intel.com>
Reported-by: Dan Carpenter <dan.carpenter@linaro.org>
Closes: https://lore.kernel.org/r/202404051340.7Wo7oiJ5-lkp@intel.com/
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
---

 lib/alloc_tag.c |    2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

--- a/lib/alloc_tag.c~lib-add-allocation-tagging-support-for-memory-allocation-profiling-fix-4
+++ a/lib/alloc_tag.c
@@ -141,7 +141,7 @@ static int __init alloc_tag_init(void)
 	};
 
 	alloc_tag_cttype = codetag_register_type(&desc);
-	if (IS_ERR_OR_NULL(alloc_tag_cttype))
+	if (IS_ERR(alloc_tag_cttype))
 		return PTR_ERR(alloc_tag_cttype);
 
 	register_sysctl_init("vm", memory_allocation_profiling_sysctls);
_
